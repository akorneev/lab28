import sys, os

# command line parameters
argc = len(sys.argv)

if argc < 4 :
    print("Usage: cmsRun lab28dump.py [environment] file1 [file2 ...]")
    print("  [environment] - environment id")
    print("  [file1]       - file name")
    sys.exit(1)

envId = sys.argv[2]
fileList = sys.argv[3:]
runNumber = os.path.basename(fileList[0]).split("_")[1].split(".")[0]

print("file name =", fileList)
print("run number =", runNumber)
print("environment id =", envId)

# setup CMSSW
import FWCore.ParameterSet.Config as cms

process = cms.Process("lab28dump")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 1000

# debug
#process.MessageLogger.cerr.threshold = cms.untracked.string( 'INFO' )
#process.MessageLogger.cerr.INFO.limit = cms.untracked.int32(-1)
#process.MessageLogger.debugModules = cms.untracked.vstring('*') 

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.source = cms.Source("HcalTBSource",
    quiet = cms.untracked.bool(False),
    firstLuminosityBlockForEachRun = cms.untracked.VLuminosityBlockID([]),
    fileNames = cms.untracked.vstring(
        ['file:'+i for i in fileList]
    )
)

process.options = cms.untracked.PSet(
        wantSummary = cms.untracked.bool(False)
)

process.tbunpack = cms.EDProducer("HcalTBObjectUnpacker",
        IncludeUnmatchedHits = cms.untracked.bool(False),
        ConfigurationFile = cms.untracked.string('UserCode/H2TestBeamAnalyzer/configQADCTDC.txt'),
#        HcalSlowDataFED = cms.untracked.int32(3),
        HcalTriggerFED = cms.untracked.int32(1),
        
        # lab28:
        HcalTDCFED = cms.untracked.int32(8),
        HcalQADCFED = cms.untracked.int32(8),
        
        fedRawDataCollectionTag = cms.InputTag('source')
)

process.hcalDigis = cms.EDProducer("HcalRawToDigi",
#       UnpackHF = cms.untracked.bool(True),
        ### Flag to enable unpacking of TTP channels(default = false)
        ### UnpackTTP = cms.untracked.bool(True),
        FilterDataQuality = cms.bool(False),
        InputLabel = cms.InputTag('source'),
        HcalFirstFED = cms.untracked.int32(700),
        ComplainEmptyData = cms.untracked.bool(False),
#       UnpackCalib = cms.untracked.bool(True),
        firstSample = cms.int32(0),
        lastSample = cms.int32(9)
)

process.hcalAnalyzer = cms.EDAnalyzer('H2TestBeamAnalyzer',
        OutFileName = cms.untracked.string('dump-'+runNumber+'.root'),
        Verbosity = cms.untracked.int32(0),
        Gain = cms.untracked.double(1)
)

#process.TFileService = cms.Service("TFileService",
#        fileName = cms.string("analysis.root"),
#)

process.load('Configuration.Geometry.GeometryIdeal_cff')
#process.load('RecoLocalCalo.Configuration.hcalLocalReco_cff')
#process.load('RecoLocalCalo.HcalRecProducers.HcalSimpleReconstructor_hf_cfi')

process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.autoCond import autoCond
from CondCore.DBCommon.CondDBSetup_cfi import *
process.GlobalTag.globaltag = autoCond['startup'] 

# EMAP
process.es_ascii = cms.ESSource('HcalTextCalibrations',
        input = cms.VPSet(
               cms.PSet(
                object = cms.string('ElectronicsMap'),
                # lab28:
                file = cms.FileInPath('UserCode/H2TestBeamAnalyzer/EMAP-lab28.txt')
               )
        )
)
process.es_prefer = cms.ESPrefer('HcalTextCalibrations', 'es_ascii')



# set proper settings for environment
if envId == "lab28":
  process.tbunpack.HcalTDCFED = cms.untracked.int32(8)
  process.tbunpack.HcalQADCFED = cms.untracked.int32(8)
  process.hcalDigis.FEDs = cms.untracked.vint32(700,928)
  process.es_ascii.input[0].file = cms.FileInPath('UserCode/H2TestBeamAnalyzer/EMAP-lab28.txt')
  
elif envId == "p5hep17":
  process.tbunpack.HcalTDCFED = cms.untracked.int32(1114)
  process.tbunpack.HcalQADCFED = cms.untracked.int32(1114)
  process.hcalDigis.FEDs = cms.untracked.vint32(1114,1194,1196,934,938)
  process.es_ascii.input[0].file = cms.FileInPath('UserCode/H2TestBeamAnalyzer/EMAP-p5-HEP17.txt')
  
elif envId == "laser18":
  process.tbunpack.HcalTDCFED = cms.untracked.int32(1114)
  process.tbunpack.HcalQADCFED = cms.untracked.int32(1114)
  process.hcalDigis.FEDs = cms.untracked.vint32()
  
  for i in range(1100, 1117):
    process.hcalDigis.FEDs.append(i)
  
  process.es_ascii.input[0].file = cms.FileInPath('UserCode/H2TestBeamAnalyzer/Emap_HE_K_20180411.txt')
  
elif envId == "tb2017":
  process.tbunpack.HcalTDCFED = cms.untracked.int32(8)
  #process.tbunpack.HcalTDCFED = cms.untracked.int32(-1)
  #process.tbunpack.HcalQADCFED = cms.untracked.int32(8)
  process.tbunpack.HcalQADCFED = cms.untracked.int32(-1)
  process.hcalDigis.FEDs = cms.untracked.vint32(938)
  process.es_ascii.input[0].file = cms.FileInPath('UserCode/H2TestBeamAnalyzer/EMAP_AC05_01AUG2017_NOMINAL.txt')
  
  # no TDC in runs with 'Run key'='Pedestal HE-phase1'
  if int(runNumber) in [2989,3369,3424,3433]:
    process.tbunpack.HcalTDCFED = cms.untracked.int32(-1)
  
else:
  print("ERROR: unknown environment id")
  sys.exit(1)


# debug
#process.output = cms.OutputModule(
#        'PoolOutputModule',
#        fileName = cms.untracked.string('cmsrun-'+runNumber+'.root')
#)
#process.outpath = cms.EndPath(process.output)

# debug
#process.dump = cms.EDAnalyzer("HcalDigiDump")
#process.p = cms.Path(process.tbunpack*process.hcalDigis*process.dump*process.hcalAnalyzer)

process.p = cms.Path(process.tbunpack*process.hcalDigis*process.hcalAnalyzer)
