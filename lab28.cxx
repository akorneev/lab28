#include "TProfile2D.h"
#include "TH2.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TFitResult.h"
#include "THStack.h"
#include "TPaveStats.h"
#include "TLegend.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
using namespace std;

#include "src/format.h"

// P5 data processing flags
const bool isP5 = true;

// linear zone range in SiPM pixels fired
// (for apd/hpd counts to effective p.e. conversion calculation)
const double lin_min_x = 10;
const double lin_max_x = 1000;

// channels of interest
//const int N_channels = 4;
const int N_channels = 16;

// shunt factor
const double shunt11_5 = 10.9;

// data structures
//
struct ChannelId {
  TString id;
  double sipm_SPC; // Single Pixel Charge, fC
  TString tag;
};

struct ErrValue {
  double x;
  double err;
};

ErrValue operator / (const ErrValue& num, const ErrValue& den)
{
  ErrValue r;
  r.x = num.x / den.x;
  r.err = r.x * (fabs(num.err/num.x) + fabs(den.err/den.x));
  return r;
}

struct StatValue {
  double x;
  double rms;
  int n;
  
  double err() const { return (n != 0) ? rms/sqrt(n) : 0; }
};

struct ChannelSignal {
  TString id;
  TString tag;
  StatValue led;
  StatValue ped;
  
  double SPC; // single pixel charge
  
  // pedestal-subtracted time samples
  static const int nTS = 10;
  ErrValue ts[nTS];
  
  double signal() const { return (led.x - ped.x); }
  double signal_err() const { return sqrt(led.err()*led.err() + ped.err()*ped.err()); }
  
  TString idtag() const { return id + ":" + tag; }
};

struct Measurement {
  TString fname;
  vector<ChannelSignal> APD;
  vector<ChannelSignal> SiPM;
  vector<ChannelSignal> HPD;
  
  int totalChannels() const { return APD.size() + SiPM.size() + HPD.size(); }
  
  ChannelSignal getChannel(size_t i) const
  {
    if (i < APD.size()) return APD[i];
    i -= APD.size();
    
    if (i < SiPM.size()) return SiPM[i];
    i -= SiPM.size();
    
    return HPD[i];
  }
  
  void setChannel(size_t i, ChannelSignal signal)
  {
    if (i < APD.size()) { APD[i] = signal; return; }
    i -= APD.size();
    
    if (i < SiPM.size()) { SiPM[i] = signal; return; }
    i -= SiPM.size();
    
    HPD[i] = signal; return;
  }
  
  void print() const
  {
    cout << "File name: " << fname << "\n";
    
    for (int i = 0; i < totalChannels(); ++i) {
      const ChannelSignal& c = getChannel(i);
      cout << c.id
           << " | LED mean = " << c.led.x << " rms = " << c.led.rms << " n = " << c.led.n << " err() = " << c.led.err()
           << " | PED mean = " << c.ped.x << " rms = " << c.ped.rms << " n = " << c.ped.n << " err() = " << c.ped.err()
           << "\n";
    }
    
    cout << endl;
  }
};

struct RunId {
  int runid;
  double shunt;
  
  TString fname() const
  {
    return TString::Format("dump-%06d.root", runid);
  }
};

struct RunsSet {
  TString title;
  vector<ChannelId> channels;
  
  vector<RunId> runs;
  
  TString name() const
  {
    if (runs.size() == 0) return title;
    
    int min = runs[0].runid;
    int max = runs[0].runid;
    
    for (const RunId& r : runs) {
      const int id = r.runid;
      if (id < min) min = id;
      if (id > max) max = id;
    }
    
    return TString::Format("%d-%d-", min, max) + title;
  }
  
  TString cache() const
  {
    return name() + "-cache.txt";
  }
};

struct RunsPlots {
  TMultiGraph* plots[5];
  //TMultiGraph* raw;
  //TMultiGraph* sipm_pixels;
  //TMultiGraph* apd_pe;
  /*
  TString name(const int i) const
  {
    if (i == 0) return "RAW";
    if (i == 1) return "SIPM_PIXELS";
    if (i == 2) return "APD_PE";
    if (i == 3) return "CORR";
    return "UNKNOWN";
  }
  */
};

struct RunsResults {
  RunsSet runs;
  vector<Measurement> meas;
  RunsPlots plots;
};

struct PlotInfo {
  TString id;
  TString name;
  
  // ranges for zoom plot
  double Xmin, Xmax, Ymin, Ymax;
};

// QIE11 charge [fC] per pixel fired
//const double fC_to_pixel = 52.5; // uniform gain
//const double fC_to_pixel[N_channels] = {52.8, 50.4, 51.0, 53.7};
//const double fC_to_pixel = 47.0;

/*
const ChannelId selchan[N_channels] = {
  {2, 2, ""},
  {2, 3, ""},
  {2, 4, ""},
  {2, 5, ""},
  {2, 6, ""},
  
  {4, 1, ""},
  {4, 6, ""},
  
  {6, 3, ""},
  {6, 6, ""},
  
  {7, 1, ""},
  {7, 3, ""},
  {7, 4, ""},
  
  {8, 2, ""},
  {8, 3, ""},
  {8, 4, ""},
  {8, 6, ""}
  
  
  //{7, 1, ""},
  //{6, 3, ""},
  //{6, 6, ""},
  //{4, 1, ""}
};
*/

void exec1()
{
  if (!gPad) return;
  
  int event = gPad->GetEvent();
  int px = gPad->GetEventX();
  TObject* select = gPad->GetSelected();
  TString name = select ? select->IsA()->GetName() : "";
  
  /*
  cout << "exec1()"
       << " gpad name = " << gPad->GetName()
       << " gpad event = " << event
       << " gpad px = " << px
       << " select = " << select
       << " select name = " << name
       << endl;
  */
  
  // https://root.cern.ch/doc/master/Buttons_8h_source.html
  if (event != kButton1Up) return;
  
  TPad* pad = (TPad*) gPad->Clone();
  pad->DeleteExec("exec1");
  
  // copy primitives
  TIter next(gPad->GetListOfPrimitives());
  while (TObject* obj = next()) {
    TObject* clone = obj->Clone();
    pad->GetListOfPrimitives()->Add(clone, next.GetOption());
    //cout << clone->GetName() << " = " << next.GetOption() << " = " << obj->GetOption() << " = " << obj->GetDrawOption() << endl;
  }
  
  TCanvas* c = new TCanvas;
  c->cd();
  gROOT->SetSelectedPad(c);
  
  pad->SetPad(0,0,1,1);
  pad->Draw();
  pad->Modified();
  pad->Update();
  pad->cd();
}

 

void zoom_to_xcontents(TH1* h)
{
  const int f = h->FindFirstBinAbove(0);
  const int l = h->FindLastBinAbove(0);
  TAxis* a = h->GetXaxis();
  a->SetRange(f-10, l+10);
}

void zoom_to_xcontents(THStack* hs)
{
  TList* list = hs->GetHists();
  
  int lo = 100000;
  int hi = 0;
  
  for (int i = 0; i < list->GetSize(); ++i) {
    TH1* h = (TH1*) list->At(i);
    const int first = h->FindFirstBinAbove(0);
    const int last = h->FindLastBinAbove(0);
    //cout << "zoom_to_xcontents(THStack): i = " << i << " name = " << h->GetName() << " firstbin = " << first << " lastbin = " << last << endl;
    
    if (lo > first) lo = first;
    if (hi < last) hi = last;
  }
  
  TAxis* a = hs->GetXaxis();
  a->SetRange(lo-10, hi+10);
}

void zoom_to_ycontents(TH2* h, const bool isLogY = false)
{
  const int f = h->FindFirstBinAbove(0, 2);
  const int l = h->FindLastBinAbove(0, 2);
  TAxis* a = h->GetYaxis();
  a->SetRange(f-(isLogY ? 1 : 10), l+10);
}

void axis_center_labels(TAxis* a)
{
  const int ndiv = a->GetXmax() - a->GetXmin();
  a->SetNdivisions(ndiv);
  a->CenterLabels();
}

// HCAL cell ID
struct CellId
{
  int iphi; // 1 - 72
  int ieta; // 16 - 29, sign: "+" for HE+, "-" for "HE-"
  int depth;// 1 - 7
  
  TString id() const { return TString::Format("%d-%d-%d", iphi, ieta, depth); }
  
  friend bool operator == (const CellId& l, const CellId& r) {
    return (l.iphi == r.iphi) && (l.ieta == r.ieta) && (l.depth == r.depth);
  }
  
  friend bool operator != (const CellId& l, const CellId& r) { return !(l == r); }
};

struct GausFitResult {
  ErrValue mean;
  ErrValue sigma;
  double chi2norm;
};

class DumpFormat {
  TFile* f;
  TTree* Events11;
  TTree* Events8;
  
public:
  Int_t nevents;
  TQIE11Info q11;
  TQIE8Info q8;
  
  //int led_trigger;
  //double QADC5;
  
  DumpFormat(const TString fname)
  : f(0), Events11(0), Events8(0), nevents(0)
  {
    // open data file
    f = new TFile(fname);
    
    if (f->IsOpen() == kFALSE) {
      throw std::runtime_error(("ERROR: can't open file " + fname).Data());
    }
    
    // QIE11 readout
    Events11 = (TTree*)f->Get("QIE11Data/Events");
    //cout << "Events11=" << Events11 << endl;
    Events11->SetBranchAddress("numChs", &q11.numChs);
    Events11->SetBranchAddress("ieta", &q11.ieta);
    Events11->SetBranchAddress("iphi", &q11.iphi);
    Events11->SetBranchAddress("depth", &q11.depth);
    Events11->SetBranchAddress("pulse", &q11.pulse);
    Events11->SetBranchAddress("pulse_adc", &q11.pulse_adc);
    
    // QIE8 readout
    Events8 = (TTree*)f->Get("HBHEData/Events");
    //cout << "Events8=" << Events8 << endl;
    Events8->SetBranchAddress("numChs", &q8.numChs);
    Events8->SetBranchAddress("ieta", &q8.ieta);
    Events8->SetBranchAddress("iphi", &q8.iphi);
    Events8->SetBranchAddress("depth", &q8.depth);
    Events8->SetBranchAddress("pulse", &q8.pulse);
    Events8->SetBranchAddress("pulse_adc", &q8.pulse_adc);
    
    //TTree* treeTriggers = (TTree*) f->Get("Triggers/Events");
    //treeTriggers->SetBranchAddress("led", &led_trigger);
    
    //TTree* treeBC = (TTree*) f->Get("BeamCounters/Events");
    //treeBC->SetBranchAddress("QADC5", &QADC5);
    
    nevents = (Int_t)Events11->GetEntries();
    //cout << "nevents=" << nevents << endl;
  }
  
  ~DumpFormat() {
    delete f;
  }
  
  // load event data
  void ReadEvent(int i)
  {
    Events11->GetEntry(i);
    Events8->GetEntry(i);
    //treeTriggers->GetEntry(i);
    //treeBC->GetEntry(i);
  }
};

// read P5 laser 2018 data
Measurement read_data_laser18(const TString fname, const double shunt = 1.)
{
  cout << "===> Reading file " << fname << ", QIE11 shunt = " << shunt << endl;
  
  gStyle->SetOptStat("emruo");
  //gStyle->SetOptFit(1111);
  
  TH1::AddDirectory(kFALSE);
  TH1::SetDefaultSumw2(kTRUE);
  
  TString label;
  
  // qie11 book histograms
  TProfile* pulse11 = new TProfile("pulse11", "QIE11 pulse shape;time sample;charge, fC", 10, 0, 10);
  pulse11->SetMarkerStyle(kFullSquare);
  pulse11->SetMarkerSize(0.5);
  TH1I* adc11 = new TH1I("adc11", "adc11;ADC counts;number of samples", 255, 0, 255);
  
  TH1I* pedestal11 = new TH1I("pedestal11", "pedestal charge;charge, fC;events/10fC", 300, 0, 3000);
  TH1I* signal11 = new TH1I("signal11", "signal charge;charge, fC;events/10fC", 20000, 0, 200000);
  TH1I* total11 = new TH1I("total11", "total charge;charge, fC;events/10fC", 20000, 0, 200000);
  
  // per-channel histograms
  TProfile2D* allpulse11  = new TProfile2D("allpulse11", "pulse shape;channel;time sample;charge, fC", 1, 0, 1, 10, 0, 10); //, "S");
  axis_center_labels(allpulse11->GetYaxis());
  allpulse11->SetOption("COL");
  
  const int nqedges = 1001;
  double qedges[nqedges];
  double loedge = 0.;
  double step = 10.;
  for (int i = 0; i < nqedges; ++i) {
    cout << loedge << " ";
    qedges[i] = loedge;
    loedge += step;
    
    if (i % 10 == 9) {
      cout << endl;
      step += 10. * shunt / 3.;
    }
  }
  cout << endl;
  
  
  //return Measurement();
  
  TH2I* alltotal11 = new TH2I("alltotal11", "total charge distribution;channel;charge, fC;events", 1, 0, 1, nqedges-1, qedges);
  alltotal11->SetOption("COLZ");
  
  /*
  TH2I* alltscharge11 = new TH2I("alltotal11", "total charge distribution;channel;charge, fC;events", 1, 0, 1, nqedges-1, qedges);
  alltotal11->SetOption("COLZ");
  */
  
  
  
  TProfile* alltotal11pf = new TProfile("alltotal11pf", "total charge mean;channel;charge, fC", 1, 0, 1);
  alltotal11pf->SetMarkerStyle(kFullSquare);
  alltotal11pf->SetMarkerSize(0.5);
  
  // apd
  TH1D* apd[2]; // [led]
  apd[1] = new TH1D("APD signal", "APD signal;adc counts", 4100, 0, 4100);
  apd[1]->SetLineColor(kRed);
  
  apd[0] = new TH1D("APD pedestal", "APD pedestal;adc counts", 4100, 0, 4100);
  apd[0]->SetLineColor(kBlue);
  
  // qie8 book histograms
  TH2F* spectr8[2][2]; // [RM][LED]
  TProfile* pulseshape8[2][18][2]; // [RM][channel][LED]
  TProfile* signalmap8[2][2]; // [RM][led]
  
  for (int rm = 0; rm < 2; rm++) {
    for (int led = 0; led < 2; led++) {
      label.Form("qie8 spectra RM%d LED=%d", rm+1, led);
      spectr8[rm][led] = new TH2F(label, label + ";channel;total charge, fC", 18, 0, 18, 10000, 0, 35000);
      axis_center_labels(spectr8[rm][led]->GetXaxis());
      
      label.Form("qie8 signal RM%d led=%d", rm+1, led);
      signalmap8[rm][led] = new TProfile(label, label + ";channel", 18, 0, 18, "S");
    
      for (int ch=0; ch < 18; ch++) {
        label.Form("qie8 pulse shape RM%d ch%d led=%d", rm+1, ch, led);
        pulseshape8[rm][ch][led] = new TProfile(label, label + ";time sample;avg. amplitude, fC", 10, 0, 10);
        axis_center_labels(pulseshape8[rm][ch][led]->GetXaxis());
        
        if (led == 1) pulseshape8[rm][ch][led]->SetLineColor(kRed);
      }
    }
  }
  
  
  DumpFormat d(fname);
  cout << "Total events " << d.nevents << endl;
  
  // no laser pulse in first ~100 events
  for (Int_t i = 100; i < d.nevents; i++) {
    // print progress
    if (i % 100 == 0)
      cout << "Event #" << i << endl;
    
    d.ReadEvent(i);
    
    // TODO: only P5 ????
    // skip event with QIE8 data corruption (idle stream events)
    if (d.q8.isIdleStream()) continue;
    
    // QIE11 event corruption check
    if (d.q11.isBad()) continue;
    
    
   for (int j=0; j < d.q11.numChs; j++) {
     const CellId id = {d.q11.iphi[j], d.q11.ieta[j], d.q11.depth[j]};
     const TString channel = id.id();
     const CellId id38184 = {38, 18, 4};
     //if (id != id38184) continue;
     
     double sumall = 0;
     
     for (int k = 0; k < 8; k++) {
        const double p = shunt*d.q11.pulse[j][k];
        
        allpulse11->Fill(channel, k, p, 1.);
        adc11->Fill(d.q11.pulse_adc[j][k]);
        pulse11->Fill(k, p);
        
        sumall += p;
     }
     
     const double sumped = 8./2. * (shunt*d.q11.pulse[j][0] + shunt*d.q11.pulse[j][1]);
     
     
     const double signal = sumall-sumped;
     
     pedestal11->Fill(sumped);
     signal11->Fill(signal);
     total11->Fill(sumall);
     
     alltotal11->Fill(channel, sumall, 1.);
     alltotal11pf->Fill(channel, sumall);


     // dump pulse
     //const bool dumpPulse = (sumall > 1e4 || isProblem);
     //const bool dumpPulse = (sumped > 1e3);
     //const bool dumpPulse = true;
     const bool dumpPulse = ((i+1) % 1000 == 0) && (id == id38184);
     
     if (dumpPulse) {
     cout << "event#" << i << " | sum=" << sumall << " fC | ";
     d.q11.print(j);
     }
   }
   
   //break;
   continue;
    
    // QIE8
    cout << "  QIE8.numChs = " << d.q8.numChs << endl;
    
    continue;
    
    
    for (int j=0; j < d.q8.numChs; j++) {
      const int rm = d.q8.depth[j] - 1;
      const int ch = d.q8.ieta[j];
      //cout << "  j = " << j << " rm = " << rm << " ch = " << ch << endl;
      
      // populate spectra plots
      // pulse shape plots
      float sumall = 0;
      
      for (int k = 0; k < 10; k++) {
        const float p = d.q8.pulse[j][k];
        pulseshape8[rm][ch][1]->Fill(k, p);
        sumall += p;
      }
      
      /*
      if (sumall < 35) {
        cout << "event#" << i << " HPD-" << rm << "-" << ch << "-led=" << led_trigger << " pulse = ";
        for (int k = 0; k < 10; k++) cout << d.q8.pulse[j][k] << " ";
        cout << " | adc = ";
        for (int k = 0; k < 10; k++) cout << (int)d.q8.pulse_adc[j][k] << " ";
        cout << endl;
      }
      */
      
      spectr8[rm][1]->Fill(ch, sumall);
    }
    
  }
  
  cout << "End of event loop" << endl;
  
  /*
  allpulse11->LabelsDeflate("X");
  allpulse11->LabelsOption("a v", "X");
  //alltotal11->LabelsDeflate("X");
  //alltotal11->LabelsOption("a v", "X");
  alltotal11pf->LabelsDeflate("X");
  alltotal11pf->LabelsOption("a v", "X");
  */
  
  cout << 1 << endl;
  
  // 
  TFile f11("histo-"+fname, "RECREATE");
  f11.SetBit(TFile::kDevNull); // disable fsync to speedup I/O
  f11.cd();
  cout << 2 << endl;
  adc11->Write();
  pulse11->Write();
  pedestal11->Write();
  signal11->Write();
  total11->Write();
  allpulse11->Write();
  alltotal11->Write();
  alltotal11pf->Write();
  cout << 3 << endl;
  f11.Close();
  cout << 3 << endl;
  
  return Measurement();
  
  
    TCanvas* c1 = new TCanvas;
    c1->SetLogy();
    c1->SetGrid();
    signal11->Draw();
    zoom_to_xcontents(signal11);
    
    
    TCanvas* c2 = new TCanvas;
    c2->SetLogy();
    c2->SetGrid();
    adc11->Draw();
    zoom_to_xcontents(adc11);
    
  
  return Measurement();
  /*
  for (int i = 0; i < 4; ++i) {
    spec11[i]->LabelsDeflate("X");
    spec11[i]->LabelsOption("a v", "X");
    
    //TCanvas* c1 = new TCanvas(label, label);
    TCanvas* c1 = new TCanvas;
    c1->SetLogz();
    c1->SetLogy();
    c1->SetGridx();
    spec11[i]->Draw();
    zoom_to_ycontents(spec11[i], true);
  }
  
  
  return Measurement();
  
  //new TCanvas;
  
  // fit pedestals
  map<string, GausFitResult> ped;
  
  for (int i = 1; i <= spec11[3]->GetNbinsX(); ++i) {
    
    const string name = spec11[3]->GetXaxis()->GetBinLabel(i);
    TH1D* pr = spec11[3]->ProjectionY("", i, i, "e");
    TFitResultPtr fit = pr->Fit("gaus", "SNQ");
    cout << "ped ch " << name
         << " fit status=" << (int)fit
         << " mean=" << fit->Parameter(1)
         << " mean_err=" << fit->ParError(1)
         << " sigma=" << fit->Parameter(2)
         << " sigma_err=" << fit->ParError(2)
         << " chi2=" << fit->Chi2()
         << " ndof=" << fit->Ndf()
         << endl;
         
    const GausFitResult r = {
      {fit->Parameter(1), fit->ParError(1)},
      {fit->Parameter(2), fit->ParError(2)},
      fit->Chi2() / fit->Ndf()
    };
    
    ped[name] = r;
  }
  
  
  return Measurement();
  */
  
  
  /*
  // === draw pulse shape
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / pulse shape", fname.Data(), rm);
    TCanvas* c = new TCanvas(label, label);
    c->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c->cd(1 + (f-1) + 8*(6-fch))->AddExec("exec1", "exec1()");
        
        const int ch = 10*f + fch;
        avgPS[rm][ch][1]->SetLineColor(kRed);
        avgPS[rm][ch][1]->Draw();
        avgPS[rm][ch][0]->Draw("same");
      }
    }
  }
  
  // === draw spectra vs channel plot
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge spectrum", fname.Data(), rm);
    TCanvas* c1 = new TCanvas(label, label);
    c1->Divide(1, 2);
    TPad* p1 = (TPad*) c1->cd(1);
    p1->SetLogz();
    p1->SetLogy();
    p1->SetGridx();
    spectr11[rm][1]->Draw("colz");
    zoom_to_ycontents(spectr11[rm][1]);
    
    TPad* p2 = (TPad*) c1->cd(2);
    p2->SetLogz();
    p2->SetGridx();
    spectr11[rm][0]->Draw("colz");
    zoom_to_ycontents(spectr11[rm][0]);
  }
  
  
  // === draw histograms of LED signal distributions
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge histograms LED=1", fname.Data(), rm);
    TCanvas* c4 = new TCanvas(label, label);
    c4->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c4->cd(1 + (f-1) + 8*(6-fch));
        const int ch = 10*f + fch;
        const int bin = 1 + ch;
        TH1D* p = spectr11[rm][1]->ProjectionY(label, bin, bin);
        label.Form("RM%d ch%d LED=1;total charge, fC;#events", rm, ch);
        p->SetTitle(label);
        p->Draw("HIST");
        zoom_to_xcontents(p);
      }
    }
  }
  
  
  TH1D* spc1d_10 = new TH1D("spc1d_10", "Single pixel charge (p1-p0);Charge, fC;Channels", 50, 30, 50);
  TH1D* spc1d_21 = new TH1D("spc1d_21", "Single pixel charge (p2-p1);Charge, fC;Channels", 50, 30, 50);
  TH1D* spc1d_32 = new TH1D("spc1d_32", "Single pixel charge (p3-p2);Charge, fC;Channels", 50, 30, 50);
  double SPCdata[5][100][2] = {0};    // [RM][ch][led]
  
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge histograms LED=0", fname.Data(), rm);
    TCanvas* c = new TCanvas(label, label);
    //c->AddExec("exec1", "exec1()");
    c->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c->cd(1 + (f-1) + 8*(6-fch))->AddExec("exec1", "exec1()");
        const int ch = 10*f + fch;
        const int bin = 1 + ch;
        TH1D* p = spectr11[rm][0]->ProjectionY(label, bin, bin);
        label.Form("RM%d ch%d LED=0;total charge, fC;#events", rm, ch);
        p->SetTitle(label);
        p->Draw("HIST");
        zoom_to_xcontents(p);
        
        SPCdata[rm][ch][0] = 0;
        
        if (p->GetEntries() == 0) continue;
        
        cout << "rm" << rm << "-ch" << ch << " peaks:" << endl;
        
        vector<double> peak;
        // initial estimate of peak position and single pixel charge
        double peakpos = p->GetBinCenter(p->GetMaximumBin());
        const double gain = 40;
        
        for (int i = 0; i < 4; i++) {
          cout << "  PEAK" << i << " estimate = " << peakpos << endl;
          TFitResultPtr fitres = p->Fit("gaus", "IQS+", "", peakpos - gain/2, peakpos + gain/2);
          cout << "  fit status = " << fitres << endl;
          if (fitres < 0) break;
          peakpos = fitres->Parameter(1);
          cout << "  gaus fit params: mean = " << peakpos
               << "  sigma = " << fitres->Parameter(2)
               << "  chi2 = " << fitres->Chi2()
               << "  ndof = " << fitres->NFreeParameters()
               << "  ntot = " << fitres->NPar()
               << endl;
          peak.push_back(peakpos);
          
          // next peak position
          peakpos += (i < 1) ? gain : (peak[i] - peak[i-1]);
        }
        
        const double SPC10 = (peak.size() > 1) ? (peak[1] - peak[0]) : 0;
        const double SPC21 = (peak.size() > 2) ? (peak[2] - peak[1]) : 0;
        const double SPC32 = (peak.size() > 3) ? (peak[3] - peak[2]) : 0;
        SPCdata[rm][ch][0] = SPC10;
        
        spc1d_10->Fill(SPC10);
        spc1d_21->Fill(SPC21);
        spc1d_32->Fill(SPC32);
        
        cout << "  Peaks detected = " << peak.size()
             << " SPC10 = " << SPC10
             << " SPC21 = " << SPC21
             << " SPC32 = " << SPC32
             << endl;
        cout << endl;
      }
    }
  }
  
  // === draw single pixel charge distribution
  label.Form("%s / QIE11-SiPM / single pixel charge (p1-p0) LED=0", fname.Data());
  TCanvas* c = new TCanvas(label, label);
  spc1d_10->Draw();
  
  label.Form("%s / QIE11-SiPM / single pixel charge (p2-p1) LED=0", fname.Data());
  c = new TCanvas(label, label);
  spc1d_21->Draw();
  
  label.Form("%s / QIE11-SiPM / single pixel charge (p3-p2) LED=0", fname.Data());
  c = new TCanvas(label, label);
  spc1d_32->Draw();
  
  // === draw signal maps mean and rms
  for (int rm = 1; rm <= 4; ++rm) {
    TH2D* meansignalmap[2];
    TH2D* rmssignalmap[2];
    
    for (int led=0; led<2; led++) {
      label.Form("qie11 mean RM%d led=%d", rm, led);
      meansignalmap[led] = signalmap11[rm][led]->ProjectionXY(label, "E");
      meansignalmap[led]->SetTitle(label + ";fiber;channel");
      
      label.Form("qie11 rms RM%d led=%d", rm, led);
      rmssignalmap[led] = signalmap11[rm][led]->ProjectionXY(label, "E C=E");
      rmssignalmap[led]->SetTitle(label + ";fiber;channel");
    }
    
    label.Form("%s / QIE11-SiPM RM%d / total charge map", fname.Data(), rm);
    TCanvas* c2 = new TCanvas(label, label);
    c2->Divide(2,2);
    c2->cd(1)->SetLogz();
    meansignalmap[1]->Draw("colz text");
    axis_center_labels(meansignalmap[1]->GetXaxis());
    axis_center_labels(meansignalmap[1]->GetYaxis());
    meansignalmap[1]->SetStats(0);
    c2->cd(2)->SetLogz();
    meansignalmap[0]->Draw("colz text");
    axis_center_labels(meansignalmap[0]->GetXaxis());
    axis_center_labels(meansignalmap[0]->GetYaxis());
    meansignalmap[0]->SetStats(0);
    c2->cd(3)->SetLogz();
    rmssignalmap[1]->Draw("colz text");
    axis_center_labels(rmssignalmap[1]->GetXaxis());
    axis_center_labels(rmssignalmap[1]->GetYaxis());
    rmssignalmap[1]->SetStats(0);
    c2->cd(4)->SetLogz();
    rmssignalmap[0]->Draw("colz text");
    axis_center_labels(rmssignalmap[0]->GetXaxis());
    axis_center_labels(rmssignalmap[0]->GetYaxis());
    rmssignalmap[0]->SetStats(0);
  }
  */
  
  
  // === draw reference apd
  label.Form("%s / QADC-APD / amplitude histograms", fname.Data());
  TCanvas* c3 = new TCanvas(label, label);
  
  THStack* hs = new THStack(label, label + ";adc counts;#events");
  hs->Add(apd[1], "sames");
  hs->Add(apd[0], "sames");
  hs->Draw("nostack");
  zoom_to_xcontents(hs);
  
  // special magic to display stats box
  // https://root.cern.ch/phpBB3/viewtopic.php?p=1094#p1094
  c3->Update();
  TPaveStats* st0 = (TPaveStats*) apd[0]->GetListOfFunctions()->FindObject("stats");
  TPaveStats* st1 = (TPaveStats*) apd[1]->GetListOfFunctions()->FindObject("stats");
  const double vsize = st0->GetY2NDC() - st0->GetY1NDC();
  const double gap = 0.01;
  st0->SetY1NDC(st1->GetY1NDC() - vsize - gap);
  st0->SetY2NDC(st1->GetY1NDC() - gap);
  c3->Modified();
  
  // gaus fit of APD data
  //TFitResultPtr apdfit = apd[1]->Fit("gaus", "QMS", "same");
  //apdfit->Print("V");
  //TFitResultPtr pedapdfit = apd[0]->Fit("gaus", "QMS", "same");
  //pedapdfit->Print("V");
  
  // === QIE8 draw pulse shape
  for (int rm = 0; rm < 2; rm++) {
    TString label;
    label.Form("%s / QIE8-HPD RM%d / pulse shape", fname.Data(), rm+1);
    TCanvas* c = new TCanvas(label, label);
    c->DivideSquare(18);
    
    for (int ch = 0; ch < 18; ch++) {
      c->cd(ch+1)->AddExec("exec1", "exec1()");
      
      label.Form("QIE8 pulse shape RM%d ch%d", rm+1, ch);
      THStack* hs = new THStack(label, label + ";time sample;avg. amplitude, fC");
      hs->Add(pulseshape8[rm][ch][0]);
      hs->Add(pulseshape8[rm][ch][1]);
      hs->Draw();
      axis_center_labels(hs->GetXaxis());
    }
  }
  
  // === QIE8 draw spectr vs channel plot
  for (int rm = 0; rm < 2; rm++) {
    TString label;
    label.Form("%s / QIE8-HPD RM%d / spectrum", fname.Data(), rm+1);
    TCanvas* c = new TCanvas(label, label);
    c->Divide(1, 2);
    c->cd(1);
    c->GetPad(1)->SetLogz();
    spectr8[rm][1]->Draw("colz");
    zoom_to_ycontents(spectr8[rm][1]);
    
    c->cd(2);
    c->GetPad(2)->SetLogz();
    spectr8[rm][0]->Draw("colz");
    zoom_to_ycontents(spectr8[rm][0]);
  }
  
  // === QIE8 draw signal maps
  /*
  TH1D* meansignalmap8[2][2];
  TH1D* rmssignalmap8[2][2];
  
  for (int rm = 0; rm < 2; rm++) {
    for (int led = 0; led < 2; led++) {
      label.Form("qie8 rm%d mean led=%d", rm+1, led);
      meansignalmap8[rm][led] = signalmap8[rm][led]->ProjectionX(label, "E");
      meansignalmap8[rm][led]->SetTitle(label + ";channel");
      
      label.Form("qie8 rm%d rms led=%d", rm+1, led);
      rmssignalmap8[rm][led] = signalmap8[rm][led]->ProjectionX(label, "E C=E");
      rmssignalmap8[rm][led]->SetTitle(label + ";channel");
    }
  }
  */
  
  // === prepare output data
  Measurement m;
  m.fname = fname;
  
  {
    ChannelSignal c;
    c.id = "APD";
    //c.led.x = apdfit->Parameter(1);
    //c.led.rms = apdfit->Parameter(2);
    //c.ped.x = pedapdfit->Parameter(1);
    //c.ped.rms = pedapdfit->Parameter(2);
    c.led.x = apd[1]->GetMean();
    c.led.rms = apd[1]->GetRMS();
    c.led.n = apd[1]->GetEntries();
    c.ped.x = apd[0]->GetMean();
    c.ped.rms = apd[0]->GetRMS();
    c.ped.n = apd[0]->GetEntries();
    c.SPC = 0;
    m.APD.push_back(c);
  }
  
  /*
  // QIE11
  for (int rm = 1; rm <= 4; ++rm) {
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        
        const int ch = 10*f + fch;
        ChannelSignal c;
        c.id.Form("SiPM-rm%d-ch%d", rm, ch);
        c.led.x = signalmap11[rm][1]->GetBinContent(f, fch);
        c.led.rms = signalmap11[rm][1]->GetBinError(f, fch);
        c.led.n = signalmap11[rm][1]->GetBinEntries(signalmap11[rm][1]->GetBin(f, fch));
        
        c.ped.x = signalmap11[rm][0]->GetBinContent(f, fch);
        c.ped.rms = signalmap11[rm][0]->GetBinError(f, fch);
        c.ped.n = signalmap11[rm][0]->GetBinEntries(signalmap11[rm][0]->GetBin(f, fch));
        
        c.SPC = SPCdata[rm][ch][0];
        
        for (size_t i = 0; i < ChannelSignal::nTS; ++i) {
          c.ts[i].x = avgPS[rm][ch][1]->GetBinContent(i+1) - avgPS[rm][ch][0]->GetBinContent(i+1);
          c.ts[i].err = sqrt(pow(avgPS[rm][ch][1]->GetBinError(i+1), 2) + pow(avgPS[rm][ch][0]->GetBinError(i+1), 2));
        }
        
        m.SiPM.push_back(c);
      }
    }
  }
  */
  
  // QIE8
  for (int rm=0; rm<2; rm++) {
    for (int ch=0; ch<18; ch++) {
      
      ChannelSignal c;
      c.id.Form("HPD-rm%d-ch%d", rm+1, ch);
      c.led.x = signalmap8[rm][1]->GetBinContent(1+ch);
      c.led.rms = signalmap8[rm][1]->GetBinError(1+ch);
      c.led.n = signalmap8[rm][1]->GetBinEntries(1+ch);
      
      c.ped.x = signalmap8[rm][0]->GetBinContent(1+ch);
      c.ped.rms = signalmap8[rm][0]->GetBinError(1+ch);
      c.ped.n = signalmap8[rm][0]->GetBinEntries(1+ch);
      
      c.SPC = 0;
      
      for (size_t i = 0; i < ChannelSignal::nTS; ++i) {
        c.ts[i].x = pulseshape8[rm][ch][1]->GetBinContent(i+1) - pulseshape8[rm][ch][0]->GetBinContent(i+1);
        c.ts[i].err = sqrt(pow(pulseshape8[rm][ch][1]->GetBinError(i+1), 2) + pow(pulseshape8[rm][ch][0]->GetBinError(i+1), 2));
      }
      
      m.HPD.push_back(c);
    }
  }
  
  // print summary
  m.print();
  
  return m;
}




// read HTB2017 data
Measurement read_data_tb2017(const TString fname, const double shunt = 1.)
{
  cout << "===> Reading file " << fname << ", QIE11 shunt = " << shunt << endl;
  
  gStyle->SetOptStat("emruo");
  //gStyle->SetOptFit(1111);
  
  TH1::AddDirectory(kFALSE);
  TH1::SetDefaultSumw2(kTRUE);
  
  TString label;
  
  // qie11 book histograms
  TProfile* avgPS[5][100][2];    // [RM][ch][led]
  TH2F* spectr11[5][2];          // [RM][led]
  TProfile2D* signalmap11[5][2]; // [RM][led]
  
  for (int rm = 1; rm <= 4; ++rm) {
    for(int f = 1; f <= 8; f++) {
      for(int fch=1; fch<=6; fch++){
        for(int led=0;led<2;led++) {
          const int ch = 10*f + fch;
          label.Form("qie11 pulse shape RM%d ch%d led=%d", rm, ch, led);
          avgPS[rm][ch][led]= new TProfile(label, label + ";time sample;avg. amplitude, fC", 10, 0, 10);
          axis_center_labels(avgPS[rm][ch][led]->GetXaxis());
        }
      }
    }
    
    for (int led=0;led<2;led++){
      label.Form("qie11 total charge spectrum RM%d LED=%d", rm, led);
      
      // TODO: ??? more bins for P5 only?
      
      //const int nbins =   15000*(1+9*led);
      //const double max = 150000*(0.5+9.5*led);
      const int nbins =   15000*(1+9*1);
      const double max = 150000*(0.5+9.5*1);
      
      spectr11[rm][led] = new TH2F(label, label + ";channel;total charge, fC", 90, 0, 90, nbins, 0, max);
      spectr11[rm][led]->GetXaxis()->SetRangeUser(10, 90);
      
      label.Form("qie11 signal RM%d led=%d", rm, led);
      signalmap11[rm][led] = new TProfile2D(label, label + ";fiber;channel", 8, 1, 9, 6, 1, 7, "S");
    }
  }
  
  // apd
  TH1D* apd[2]; // [led]
  apd[1] = new TH1D("APD signal", "APD signal;adc counts", 4100, 0, 4100);
  apd[1]->SetLineColor(kRed);
  
  apd[0] = new TH1D("APD pedestal", "APD pedestal;adc counts", 4100, 0, 4100);
  apd[0]->SetLineColor(kBlue);
  
  // qie8 book histograms
  TH2F* spectr8[2][2]; // [RM][LED]
  TProfile* pulseshape8[2][18][2]; // [RM][channel][LED]
  TProfile* signalmap8[2][2]; // [RM][led]
  
  for (int rm = 0; rm < 2; rm++) {
    for (int led = 0; led < 2; led++) {
      label.Form("qie8 spectra RM%d LED=%d", rm+1, led);
      spectr8[rm][led] = new TH2F(label, label + ";channel;total charge, fC", 18, 0, 18, 10000, 0, 35000);
      axis_center_labels(spectr8[rm][led]->GetXaxis());
      
      label.Form("qie8 signal RM%d led=%d", rm+1, led);
      signalmap8[rm][led] = new TProfile(label, label + ";channel", 18, 0, 18, "S");
    
      for (int ch=0; ch < 18; ch++) {
        label.Form("qie8 pulse shape RM%d ch%d led=%d", rm+1, ch, led);
        pulseshape8[rm][ch][led] = new TProfile(label, label + ";time sample;avg. amplitude, fC", 10, 0, 10);
        axis_center_labels(pulseshape8[rm][ch][led]->GetXaxis());
        
        if (led == 1) pulseshape8[rm][ch][led]->SetLineColor(kRed);
      }
    }
  }
  
  // open data file
  TFile fileinput(fname);
  
  if (fileinput.IsOpen() == kFALSE) {
    cout << "ERROR: can't open file " << fname << endl;
    return Measurement();
  }
  
  float pulse11[300][10];
  unsigned char pulse_adc11[300][10];
  int depth11[300];
  int ieta11[300];
  int iphi11[300];
  int numChs11;
  
  int numChs8;
  int ieta8[300];
  int depth8[300];
  float pulse8[300][10];
  unsigned char pulse_adc8[300][10];
  
  //int led_trigger;
  //double QADC5;
  
  TTree* Events11 = (TTree*)fileinput.Get("QIE11Data/Events");
  Events11->SetBranchAddress("numChs", &numChs11);
  Events11->SetBranchAddress("pulse", &pulse11);
  Events11->SetBranchAddress("pulse_adc", &pulse_adc11);
  Events11->SetBranchAddress("ieta", &ieta11);
  Events11->SetBranchAddress("iphi", &iphi11);
  Events11->SetBranchAddress("depth", &depth11);
  
  TTree* Events8 = (TTree*)fileinput.Get("HBHEData/Events"); // QIE8 readout
  
  Events8->SetBranchAddress("numChs", &numChs8);
  Events8->SetBranchAddress("ieta", &ieta8);
  Events8->SetBranchAddress("depth", &depth8);
  Events8->SetBranchAddress("pulse", &pulse8);
  Events8->SetBranchAddress("pulse_adc", &pulse_adc8);
  
  //TTree* treeTriggers = (TTree*) fileinput.Get("Triggers/Events");
  //treeTriggers->SetBranchAddress("led", &led_trigger);
  
  //TTree* treeBC = (TTree*) fileinput.Get("BeamCounters/Events");
  //treeBC->SetBranchAddress("QADC5", &QADC5);
  
  // read events
  Int_t nentries = (Int_t)Events11->GetEntries();
  //cout << "Total events " << nentries << " " << flush;
  cout << "Total events " << nentries << " " << endl;
  
  
  //TH2F* spectr11[2]; // [trigger type]
  TH2I* spec11[4];
  {
  //const int nbins =   15000*(1+9*led);
  //const double max = 150000*(0.5+9.5*led);
  const int nbins =   150000;
  const double max = 1500000;
  
  label.Form("QIE11 charge spectrum");
  
  spec11[0] = new TH2I(label, label + " pedestal;channel;total charge, fC", 100, 0, 100, nbins, 0, max);
  spec11[1] = new TH2I(label, label + " total;channel;total charge, fC", 100, 0, 100, nbins, 0, max);
  spec11[2] = new TH2I(label, label + " signal;channel;total charge, fC", 100, 0, 100, nbins, 0, max);
  //spec11->SetCanExtend(TH1::kXaxis);
  ////spec11->ResetBit(TH1::kCanRebin);
  spec11[3] = new TH2I(label, label + " pedTS;channel;total charge, fC", 100, 0, 100, 500, 0, 2000);
  
  spec11[0]->SetOption("COLZ");
  spec11[1]->SetOption("COLZ");
  spec11[2]->SetOption("COLZ");
  spec11[3]->SetOption("COLZ");
  }
  
  
  for (Int_t i = 0; i < nentries; i++) {
   //cout << "Event #" << i << endl;
   
   // print progress
   if ((i > 0) && (i % 1000 == 0)) {
     cout << "Event #" << i << endl;
     //cout << "." << flush;
   }
   
   Events11->GetEntry(i);
   Events8->GetEntry(i);
   //treeTriggers->GetEntry(i);
   //treeBC->GetEntry(i);
   
   
    // QIE8 event corruption check
    // http://cmsonline.cern.ch/cms-elog/968085
    bool isIdleStream = false;
    
    for (int j = 0; j < numChs8; j++) {
      int n40 = 0;
      int n94 = 0;
      
      for (int k = 0; k < 10; k++) {
        const unsigned char adc = pulse_adc8[j][k];
        if (adc == 40) n40++;
        if (adc == 94) n94++;
      }
      
      isIdleStream = (n40 > 3) || (n94 > 3);
      
      if (isIdleStream) {
        cout << "INFO: QIE8 corrupted event#" << i << " | adc = ";
        for (int k = 0; k < 10; k++) cout << (int)pulse_adc8[j][k] << " ";
        cout << endl;
        break;
      }
    }
    
    // TODO: only P5 ????
    
    // skip idle stream events
    if (isIdleStream) continue;
    
   
    // QIE11 event corruption check
    // http://cmsonline.cern.ch/cms-elog/999277
    bool isBad11 = false;
    int nBad11 = 0;
    
    for (int j = 0; j < numChs11; j++) {
      int n124 = 0;
      int n188 = 0;
      
      for (int k = 0; k < 10; k++) {
        const unsigned char adc = pulse_adc11[j][k];
        if (adc == 124) n124++;
        if (adc == 188) n188++;
      }
      
      isBad11 = (n124 > 3) || (n188 > 3);
      
      if ((n124 > 0) || (n188 > 0)) nBad11++;
      
      if (isBad11) {
        cout << "INFO: QIE11 corrupted event#" << i << " | adc = ";
        for (int k = 0; k < 10; k++) cout << (int)pulse_adc11[j][k] << " ";
        cout << endl;
        break;
      }
    }
    
    // skip corrupted event
    if (!isBad11 && nBad11 == numChs11)
      cout << "INFO: QIE11 corrupted event#" << i << " | nBad11 == numChs11" << endl;
    
    
    if (isBad11 || nBad11 == numChs11) continue;
    
   /*
   cout << "led_trigger = " << led_trigger
        << " QADC5 = " << QADC5
        << endl;
   */
   
   // QIE11
   //cout << "QIE11 numChs = " << numChs11 << endl;
   
   bool isProblem = false;
   
   for (int j=0; j < numChs11; j++) {
     const CellId id = {iphi11[j], ieta11[j], depth11[j]};
     
     /*
     cout << "ch#" << j
          << " iphi=" << iphi11[j]
          << " ieta=" << ieta11[j]
          << " depth=" << depth11[j]
          //<< endl;
          << " | ";
     */
     
     double sumall = 0;
     
     // skip 1st time sample, work-around for P5
     //const int tstart = isP5 ? 1 : 0;
     //const int tstart = 0;
     const int tstart = 1;
     
     for (int k = tstart; k < 10; k++) {
     //for (int k=0; k < 10; k++) {
     //for (int k=2; k < 6; k++) {
        const double p = shunt*pulse11[j][k];
        
        //cout << p << " ";
        
        //avgPS[rm][ch][led_trigger]->Fill(k, p);
        sumall += p;
        //if (sumall < p) sumall = p;
     }
     
     const double sumped = 9./2. * (shunt*pulse11[j][1] + shunt*pulse11[j][2]);
     
     // dump pulse
     //const bool dumpPulse = (sumall > 1e4 || isProblem);
     const bool dumpPulse = (sumped > 1e3);
     
     if (dumpPulse) {
     cout << "event#" << i
          << " ch#" << j
          << " iphi=" << iphi11[j]
          << " ieta=" << ieta11[j]
          << " depth=" << depth11[j]
          //<< endl;
          << " | ";
     cout << "ADC: ";
     for (int k = 0; k < 10; k++) cout << (int)pulse_adc11[j][k] << " ";
     cout << " | pulse: ";
     for (int k = 0; k < 10; k++) cout << shunt*pulse11[j][k] << " ";
     cout << " | sum=" << sumall << " fC"<< endl;
     //cout << endl;
     //isProblem = true;
     }
     
     spec11[0]->Fill(id.id(), sumped, 1.);
     spec11[1]->Fill(id.id(), sumall, 1.);
     spec11[2]->Fill(id.id(), sumall-sumped, 1.);
     spec11[3]->Fill(id.id(), shunt*pulse11[j][1], 1.);
     spec11[3]->Fill(id.id(), shunt*pulse11[j][2], 1.);
     
     //spectr11[rm][led_trigger]->Fill(ch, sumall);
     //signalmap11[rm][led_trigger]->Fill(ieta11[j], iphi11[j], sumall);
   }
   
   //if (i > 5) break;
   continue;
    
    // QIE8
    //cout << "  numChs8 = " << numChs8 << endl;
    
    for (int j=0; j < numChs8; j++) {
      const int rm = depth8[j] - 1;
      const int ch = ieta8[j];
      //cout << "  j = " << j << " rm = " << rm << " ch = " << ch << endl;
      
      // populate spectra plots
      // pulse shape plots
      float sumall = 0;
      
      for (int k = 0; k < 10; k++) {
        const float p = pulse8[j][k];
        pulseshape8[rm][ch][1]->Fill(k, p);
        sumall += p;
      }
      
      /*
      if (sumall < 35) {
        cout << "event#" << i << " HPD-" << rm << "-" << ch << "-led=" << led_trigger << " pulse = ";
        for (int k = 0; k < 10; k++) cout << pulse8[j][k] << " ";
        cout << " | adc = ";
        for (int k = 0; k < 10; k++) cout << (int)pulse_adc8[j][k] << " ";
        cout << endl;
      }
      */
      
      spectr8[rm][1]->Fill(ch, sumall);
    }
    
  }
  
  cout << endl;
  
  for (int i = 0; i < 4; ++i) {
    spec11[i]->LabelsDeflate("X");
    spec11[i]->LabelsOption("a v", "X");
    
    /*
    //TCanvas* c1 = new TCanvas(label, label);
    TCanvas* c1 = new TCanvas;
    c1->SetLogz();
    c1->SetLogy();
    c1->SetGridx();
    spec11[i]->Draw("colz");
    zoom_to_ycontents(spec11[i], true);
    */
  }
  
  TFile f("histo-"+fname, "RECREATE");
  spec11[0]->Write("0pedestal");
  spec11[1]->Write("1total");
  spec11[2]->Write("2signal");
  
  return Measurement();
  
  //new TCanvas;
  
  // fit pedestals
  map<string, GausFitResult> ped;
  
  for (int i = 1; i <= spec11[3]->GetNbinsX(); ++i) {
    
    const string name = spec11[3]->GetXaxis()->GetBinLabel(i);
    TH1D* pr = spec11[3]->ProjectionY("", i, i, "e");
    TFitResultPtr fit = pr->Fit("gaus", "SNQ");
    cout << "ped ch " << name
         << " fit status=" << (int)fit
         << " mean=" << fit->Parameter(1)
         << " mean_err=" << fit->ParError(1)
         << " sigma=" << fit->Parameter(2)
         << " sigma_err=" << fit->ParError(2)
         << " chi2=" << fit->Chi2()
         << " ndof=" << fit->Ndf()
         << endl;
         
    const GausFitResult r = {
      {fit->Parameter(1), fit->ParError(1)},
      {fit->Parameter(2), fit->ParError(2)},
      fit->Chi2() / fit->Ndf()
    };
    
    ped[name] = r;
  }
  
  
  return Measurement();
  
  // === draw pulse shape
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / pulse shape", fname.Data(), rm);
    TCanvas* c = new TCanvas(label, label);
    c->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c->cd(1 + (f-1) + 8*(6-fch))->AddExec("exec1", "exec1()");
        
        const int ch = 10*f + fch;
        avgPS[rm][ch][1]->SetLineColor(kRed);
        avgPS[rm][ch][1]->Draw();
        avgPS[rm][ch][0]->Draw("same");
      }
    }
  }
  
  // === draw spectra vs channel plot
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge spectrum", fname.Data(), rm);
    TCanvas* c1 = new TCanvas(label, label);
    c1->Divide(1, 2);
    TPad* p1 = (TPad*) c1->cd(1);
    p1->SetLogz();
    p1->SetLogy();
    p1->SetGridx();
    spectr11[rm][1]->Draw("colz");
    zoom_to_ycontents(spectr11[rm][1]);
    
    TPad* p2 = (TPad*) c1->cd(2);
    p2->SetLogz();
    p2->SetGridx();
    spectr11[rm][0]->Draw("colz");
    zoom_to_ycontents(spectr11[rm][0]);
  }
  
  
  // === draw histograms of LED signal distributions
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge histograms LED=1", fname.Data(), rm);
    TCanvas* c4 = new TCanvas(label, label);
    c4->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c4->cd(1 + (f-1) + 8*(6-fch));
        const int ch = 10*f + fch;
        const int bin = 1 + ch;
        TH1D* p = spectr11[rm][1]->ProjectionY(label, bin, bin);
        label.Form("RM%d ch%d LED=1;total charge, fC;#events", rm, ch);
        p->SetTitle(label);
        p->Draw("HIST");
        zoom_to_xcontents(p);
      }
    }
  }
  
  
  TH1D* spc1d_10 = new TH1D("spc1d_10", "Single pixel charge (p1-p0);Charge, fC;Channels", 50, 30, 50);
  TH1D* spc1d_21 = new TH1D("spc1d_21", "Single pixel charge (p2-p1);Charge, fC;Channels", 50, 30, 50);
  TH1D* spc1d_32 = new TH1D("spc1d_32", "Single pixel charge (p3-p2);Charge, fC;Channels", 50, 30, 50);
  double SPCdata[5][100][2] = {0};    // [RM][ch][led]
  
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge histograms LED=0", fname.Data(), rm);
    TCanvas* c = new TCanvas(label, label);
    //c->AddExec("exec1", "exec1()");
    c->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c->cd(1 + (f-1) + 8*(6-fch))->AddExec("exec1", "exec1()");
        const int ch = 10*f + fch;
        const int bin = 1 + ch;
        TH1D* p = spectr11[rm][0]->ProjectionY(label, bin, bin);
        label.Form("RM%d ch%d LED=0;total charge, fC;#events", rm, ch);
        p->SetTitle(label);
        p->Draw("HIST");
        zoom_to_xcontents(p);
        
        SPCdata[rm][ch][0] = 0;
        
        if (p->GetEntries() == 0) continue;
        
        cout << "rm" << rm << "-ch" << ch << " peaks:" << endl;
        
        vector<double> peak;
        // initial estimate of peak position and single pixel charge
        double peakpos = p->GetBinCenter(p->GetMaximumBin());
        const double gain = 40;
        
        for (int i = 0; i < 4; i++) {
          cout << "  PEAK" << i << " estimate = " << peakpos << endl;
          TFitResultPtr fitres = p->Fit("gaus", "IQS+", "", peakpos - gain/2, peakpos + gain/2);
          cout << "  fit status = " << fitres << endl;
          if (fitres < 0) break;
          peakpos = fitres->Parameter(1);
          cout << "  gaus fit params: mean = " << peakpos
               << "  sigma = " << fitres->Parameter(2)
               << "  chi2 = " << fitres->Chi2()
               << "  ndof = " << fitres->NFreeParameters()
               << "  ntot = " << fitres->NPar()
               << endl;
          peak.push_back(peakpos);
          
          // next peak position
          peakpos += (i < 1) ? gain : (peak[i] - peak[i-1]);
        }
        
        const double SPC10 = (peak.size() > 1) ? (peak[1] - peak[0]) : 0;
        const double SPC21 = (peak.size() > 2) ? (peak[2] - peak[1]) : 0;
        const double SPC32 = (peak.size() > 3) ? (peak[3] - peak[2]) : 0;
        SPCdata[rm][ch][0] = SPC10;
        
        spc1d_10->Fill(SPC10);
        spc1d_21->Fill(SPC21);
        spc1d_32->Fill(SPC32);
        
        cout << "  Peaks detected = " << peak.size()
             << " SPC10 = " << SPC10
             << " SPC21 = " << SPC21
             << " SPC32 = " << SPC32
             << endl;
        cout << endl;
      }
    }
  }
  
  // === draw single pixel charge distribution
  label.Form("%s / QIE11-SiPM / single pixel charge (p1-p0) LED=0", fname.Data());
  TCanvas* c = new TCanvas(label, label);
  spc1d_10->Draw();
  
  label.Form("%s / QIE11-SiPM / single pixel charge (p2-p1) LED=0", fname.Data());
  c = new TCanvas(label, label);
  spc1d_21->Draw();
  
  label.Form("%s / QIE11-SiPM / single pixel charge (p3-p2) LED=0", fname.Data());
  c = new TCanvas(label, label);
  spc1d_32->Draw();
  
  // === draw signal maps mean and rms
  for (int rm = 1; rm <= 4; ++rm) {
    TH2D* meansignalmap[2];
    TH2D* rmssignalmap[2];
    
    for (int led=0; led<2; led++) {
      label.Form("qie11 mean RM%d led=%d", rm, led);
      meansignalmap[led] = signalmap11[rm][led]->ProjectionXY(label, "E");
      meansignalmap[led]->SetTitle(label + ";fiber;channel");
      
      label.Form("qie11 rms RM%d led=%d", rm, led);
      rmssignalmap[led] = signalmap11[rm][led]->ProjectionXY(label, "E C=E");
      rmssignalmap[led]->SetTitle(label + ";fiber;channel");
    }
    
    label.Form("%s / QIE11-SiPM RM%d / total charge map", fname.Data(), rm);
    TCanvas* c2 = new TCanvas(label, label);
    c2->Divide(2,2);
    c2->cd(1)->SetLogz();
    meansignalmap[1]->Draw("colz text");
    axis_center_labels(meansignalmap[1]->GetXaxis());
    axis_center_labels(meansignalmap[1]->GetYaxis());
    meansignalmap[1]->SetStats(0);
    c2->cd(2)->SetLogz();
    meansignalmap[0]->Draw("colz text");
    axis_center_labels(meansignalmap[0]->GetXaxis());
    axis_center_labels(meansignalmap[0]->GetYaxis());
    meansignalmap[0]->SetStats(0);
    c2->cd(3)->SetLogz();
    rmssignalmap[1]->Draw("colz text");
    axis_center_labels(rmssignalmap[1]->GetXaxis());
    axis_center_labels(rmssignalmap[1]->GetYaxis());
    rmssignalmap[1]->SetStats(0);
    c2->cd(4)->SetLogz();
    rmssignalmap[0]->Draw("colz text");
    axis_center_labels(rmssignalmap[0]->GetXaxis());
    axis_center_labels(rmssignalmap[0]->GetYaxis());
    rmssignalmap[0]->SetStats(0);
  }
  
  // === draw reference apd
  label.Form("%s / QADC-APD / amplitude histograms", fname.Data());
  TCanvas* c3 = new TCanvas(label, label);
  
  THStack* hs = new THStack(label, label + ";adc counts;#events");
  hs->Add(apd[1], "sames");
  hs->Add(apd[0], "sames");
  hs->Draw("nostack");
  zoom_to_xcontents(hs);
  
  // special magic to display stats box
  // https://root.cern.ch/phpBB3/viewtopic.php?p=1094#p1094
  c3->Update();
  TPaveStats* st0 = (TPaveStats*) apd[0]->GetListOfFunctions()->FindObject("stats");
  TPaveStats* st1 = (TPaveStats*) apd[1]->GetListOfFunctions()->FindObject("stats");
  const double vsize = st0->GetY2NDC() - st0->GetY1NDC();
  const double gap = 0.01;
  st0->SetY1NDC(st1->GetY1NDC() - vsize - gap);
  st0->SetY2NDC(st1->GetY1NDC() - gap);
  c3->Modified();
  
  // gaus fit of APD data
  //TFitResultPtr apdfit = apd[1]->Fit("gaus", "QMS", "same");
  //apdfit->Print("V");
  //TFitResultPtr pedapdfit = apd[0]->Fit("gaus", "QMS", "same");
  //pedapdfit->Print("V");
  
  // === QIE8 draw pulse shape
  for (int rm = 0; rm < 2; rm++) {
    TString label;
    label.Form("%s / QIE8-HPD RM%d / pulse shape", fname.Data(), rm+1);
    TCanvas* c = new TCanvas(label, label);
    c->DivideSquare(18);
    
    for (int ch = 0; ch < 18; ch++) {
      c->cd(ch+1)->AddExec("exec1", "exec1()");
      
      label.Form("QIE8 pulse shape RM%d ch%d", rm+1, ch);
      THStack* hs = new THStack(label, label + ";time sample;avg. amplitude, fC");
      hs->Add(pulseshape8[rm][ch][0]);
      hs->Add(pulseshape8[rm][ch][1]);
      hs->Draw();
      axis_center_labels(hs->GetXaxis());
    }
  }
  
  // === QIE8 draw spectr vs channel plot
  for (int rm = 0; rm < 2; rm++) {
    TString label;
    label.Form("%s / QIE8-HPD RM%d / spectrum", fname.Data(), rm+1);
    TCanvas* c = new TCanvas(label, label);
    c->Divide(1, 2);
    c->cd(1);
    c->GetPad(1)->SetLogz();
    spectr8[rm][1]->Draw("colz");
    zoom_to_ycontents(spectr8[rm][1]);
    
    c->cd(2);
    c->GetPad(2)->SetLogz();
    spectr8[rm][0]->Draw("colz");
    zoom_to_ycontents(spectr8[rm][0]);
  }
  
  // === QIE8 draw signal maps
  /*
  TH1D* meansignalmap8[2][2];
  TH1D* rmssignalmap8[2][2];
  
  for (int rm = 0; rm < 2; rm++) {
    for (int led = 0; led < 2; led++) {
      label.Form("qie8 rm%d mean led=%d", rm+1, led);
      meansignalmap8[rm][led] = signalmap8[rm][led]->ProjectionX(label, "E");
      meansignalmap8[rm][led]->SetTitle(label + ";channel");
      
      label.Form("qie8 rm%d rms led=%d", rm+1, led);
      rmssignalmap8[rm][led] = signalmap8[rm][led]->ProjectionX(label, "E C=E");
      rmssignalmap8[rm][led]->SetTitle(label + ";channel");
    }
  }
  */
  
  // === prepare output data
  Measurement m;
  m.fname = fname;
  
  {
    ChannelSignal c;
    c.id = "APD";
    //c.led.x = apdfit->Parameter(1);
    //c.led.rms = apdfit->Parameter(2);
    //c.ped.x = pedapdfit->Parameter(1);
    //c.ped.rms = pedapdfit->Parameter(2);
    c.led.x = apd[1]->GetMean();
    c.led.rms = apd[1]->GetRMS();
    c.led.n = apd[1]->GetEntries();
    c.ped.x = apd[0]->GetMean();
    c.ped.rms = apd[0]->GetRMS();
    c.ped.n = apd[0]->GetEntries();
    c.SPC = 0;
    m.APD.push_back(c);
  }
  
  // QIE11
  for (int rm = 1; rm <= 4; ++rm) {
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        
        const int ch = 10*f + fch;
        ChannelSignal c;
        c.id.Form("SiPM-rm%d-ch%d", rm, ch);
        c.led.x = signalmap11[rm][1]->GetBinContent(f, fch);
        c.led.rms = signalmap11[rm][1]->GetBinError(f, fch);
        c.led.n = signalmap11[rm][1]->GetBinEntries(signalmap11[rm][1]->GetBin(f, fch));
        
        c.ped.x = signalmap11[rm][0]->GetBinContent(f, fch);
        c.ped.rms = signalmap11[rm][0]->GetBinError(f, fch);
        c.ped.n = signalmap11[rm][0]->GetBinEntries(signalmap11[rm][0]->GetBin(f, fch));
        
        c.SPC = SPCdata[rm][ch][0];
        
        for (size_t i = 0; i < ChannelSignal::nTS; ++i) {
          c.ts[i].x = avgPS[rm][ch][1]->GetBinContent(i+1) - avgPS[rm][ch][0]->GetBinContent(i+1);
          c.ts[i].err = sqrt(pow(avgPS[rm][ch][1]->GetBinError(i+1), 2) + pow(avgPS[rm][ch][0]->GetBinError(i+1), 2));
        }
        
        m.SiPM.push_back(c);
      }
    }
  }
  
  // QIE8
  for (int rm=0; rm<2; rm++) {
    for (int ch=0; ch<18; ch++) {
      
      ChannelSignal c;
      c.id.Form("HPD-rm%d-ch%d", rm+1, ch);
      c.led.x = signalmap8[rm][1]->GetBinContent(1+ch);
      c.led.rms = signalmap8[rm][1]->GetBinError(1+ch);
      c.led.n = signalmap8[rm][1]->GetBinEntries(1+ch);
      
      c.ped.x = signalmap8[rm][0]->GetBinContent(1+ch);
      c.ped.rms = signalmap8[rm][0]->GetBinError(1+ch);
      c.ped.n = signalmap8[rm][0]->GetBinEntries(1+ch);
      
      c.SPC = 0;
      
      for (size_t i = 0; i < ChannelSignal::nTS; ++i) {
        c.ts[i].x = pulseshape8[rm][ch][1]->GetBinContent(i+1) - pulseshape8[rm][ch][0]->GetBinContent(i+1);
        c.ts[i].err = sqrt(pow(pulseshape8[rm][ch][1]->GetBinError(i+1), 2) + pow(pulseshape8[rm][ch][0]->GetBinError(i+1), 2));
      }
      
      m.HPD.push_back(c);
    }
  }
  
  // print summary
  m.print();
  
  return m;
}







// EMAP is defined to have:
//   depth is RM#
//   ieta, iphi is fiber, fiber channel

// QIE8-HPD readout
// QIE11-SiPM readout
// QADC-APD readout
Measurement read_data(const TString fname, const double shunt = 1.)
{
  cout << "===> Reading file " << fname << ", QIE11 shunt = " << shunt << endl;
  
  // TODO: only P5
  
  const bool enforcePed = (fname == "dump-288086.root");
  if (enforcePed) cout << "PEDESTAL run" << endl;
  
  gStyle->SetOptStat("emruo");
  //gStyle->SetOptFit(1111);
  
  TH1::AddDirectory(kFALSE);
  TH1::SetDefaultSumw2(kTRUE);
  
  TString label;
  
  // qie11 book histograms
  TProfile* avgPS[5][100][2];    // [RM][ch][led]
  TH2F* spectr11[5][2];          // [RM][led]
  TProfile2D* signalmap11[5][2]; // [RM][led]
  
  for (int rm = 1; rm <= 4; ++rm) {
    for(int f = 1; f <= 8; f++) {
      for(int fch=1; fch<=6; fch++){
        for(int led=0;led<2;led++) {
          const int ch = 10*f + fch;
          label.Form("qie11 pulse shape RM%d ch%d led=%d", rm, ch, led);
          avgPS[rm][ch][led]= new TProfile(label, label + ";time sample;avg. amplitude, fC", 10, 0, 10);
          axis_center_labels(avgPS[rm][ch][led]->GetXaxis());
        }
      }
    }
    
    for (int led=0;led<2;led++){
      label.Form("qie11 total charge spectrum RM%d LED=%d", rm, led);
      
      // TODO: ??? more bins for P5 only?
      
      //const int nbins =   15000*(1+9*led);
      //const double max = 150000*(0.5+9.5*led);
      const int nbins =   15000*(1+9*1);
      const double max = 150000*(0.5+9.5*1);
      
      spectr11[rm][led] = new TH2F(label, label + ";channel;total charge, fC", 90, 0, 90, nbins, 0, max);
      spectr11[rm][led]->GetXaxis()->SetRangeUser(10, 90);
      
      label.Form("qie11 signal RM%d led=%d", rm, led);
      signalmap11[rm][led] = new TProfile2D(label, label + ";fiber;channel", 8, 1, 9, 6, 1, 7, "S");
    }
  }
  
  // apd
  TH1D* apd[2]; // [led]
  apd[1] = new TH1D("APD signal", "APD signal;adc counts", 4100, 0, 4100);
  apd[1]->SetLineColor(kRed);
  
  apd[0] = new TH1D("APD pedestal", "APD pedestal;adc counts", 4100, 0, 4100);
  apd[0]->SetLineColor(kBlue);
  
  // qie8 book histograms
  TH2F* spectr8[2][2]; // [RM][LED]
  TProfile* pulseshape8[2][18][2]; // [RM][channel][LED]
  TProfile* signalmap8[2][2]; // [RM][led]
  
  for (int rm = 0; rm < 2; rm++) {
    for (int led = 0; led < 2; led++) {
      label.Form("qie8 spectra RM%d LED=%d", rm+1, led);
      spectr8[rm][led] = new TH2F(label, label + ";channel;total charge, fC", 18, 0, 18, 10000, 0, 35000);
      axis_center_labels(spectr8[rm][led]->GetXaxis());
      
      label.Form("qie8 signal RM%d led=%d", rm+1, led);
      signalmap8[rm][led] = new TProfile(label, label + ";channel", 18, 0, 18, "S");
    
      for (int ch=0; ch < 18; ch++) {
        label.Form("qie8 pulse shape RM%d ch%d led=%d", rm+1, ch, led);
        pulseshape8[rm][ch][led] = new TProfile(label, label + ";time sample;avg. amplitude, fC", 10, 0, 10);
        axis_center_labels(pulseshape8[rm][ch][led]->GetXaxis());
        
        if (led == 1) pulseshape8[rm][ch][led]->SetLineColor(kRed);
      }
    }
  }
  
  // open data file
  TFile fileinput(fname);
  
  if (fileinput.IsOpen() == kFALSE) {
    cout << "ERROR: can't open file " << fname << endl;
    return Measurement();
  }
  
  float pulse11[300][10];
  int depth11[300];
  int ieta11[300];
  int iphi11[300];
  int numChs11;
  
  int numChs8;
  int ieta8[300];
  int depth8[300];
  float pulse8[300][10];
  unsigned char pulse_adc8[300][10];
  
  int led_trigger;
  double QADC5;
  
  TTree* Events11 = (TTree*)fileinput.Get("QIE11Data/Events");
  Events11->SetBranchAddress("numChs", &numChs11);
  Events11->SetBranchAddress("pulse", &pulse11);
  Events11->SetBranchAddress("ieta", &ieta11);
  Events11->SetBranchAddress("iphi", &iphi11);
  Events11->SetBranchAddress("depth", &depth11);
  
  TTree* Events8 = (TTree*)fileinput.Get("HBHEData/Events"); // QIE8 readout
  
  Events8->SetBranchAddress("numChs", &numChs8);
  Events8->SetBranchAddress("ieta", &ieta8);
  Events8->SetBranchAddress("depth", &depth8);
  Events8->SetBranchAddress("pulse", &pulse8);
  Events8->SetBranchAddress("pulse_adc", &pulse_adc8);
  
  TTree* treeTriggers = (TTree*) fileinput.Get("Triggers/Events");
  treeTriggers->SetBranchAddress("led", &led_trigger);
  
  TTree* treeBC = (TTree*) fileinput.Get("BeamCounters/Events");
  treeBC->SetBranchAddress("QADC5", &QADC5);
  
  // read events
  Int_t nentries = (Int_t)Events11->GetEntries();
  cout << "Total events " << nentries << " " << flush;
  
  for (Int_t i = 0; i < nentries; i++) {
   //cout << "Event #" << i << endl;
   
   // print progress
   if ( (i + 1) % 1000 == 0) {
     //cout << "processing event " << (i+1) << endl;
     cout << "." << flush;
   }
   
   Events11->GetEntry(i);
   Events8->GetEntry(i);
   treeTriggers->GetEntry(i);
   treeBC->GetEntry(i);
   
   
    // QIE8 idle stream event check
    bool isIdleStream = false;
    
    for (int j = 0; j < numChs8; j++) {
      int n40 = 0;
      int n94 = 0;
      
      for (int k = 0; k < 10; k++) {
        const unsigned char adc = pulse_adc8[j][k];
        if (adc == 40) n40++;
        if (adc == 94) n94++;
      }
      
      isIdleStream = (n40 > 3) || (n94 > 3);
      
      if (isIdleStream) {
        cout << "INFO: skip idle stream event#" << i << " | adc = ";
        for (int k = 0; k < 10; k++) cout << (int)pulse_adc8[j][k] << " ";
        cout << endl;
        break;
      }
    }
    
    // TODO: only P5 ????
    
    // skip idle stream events
    if (isIdleStream) continue;
    
   
   /*
   cout << "led_trigger = " << led_trigger
        << " QADC5 = " << QADC5
        << endl;
   */
   
   if (isP5) {
     led_trigger = 1;
     if (enforcePed) led_trigger = 0;
   }
   
   // QADC-APD
   apd[led_trigger]->Fill(QADC5);
   
   // QIE11
   //cout << "QIE11 numChs = " << numChs11 << endl;
   
   for (int j=0; j < numChs11; j++) {
     const int rm = depth11[j];
     const int ch = 10*ieta11[j] + iphi11[j];
     
     /*
     cout << "channel #" << j
          << " ieta = " << ieta11[j]
          << " iphi = " << iphi11[j]
          << " depth = " << depth11[j]
          << " -> "
          << " rm = " << rm
          << " ch = " << ch
          << endl;
     */
     
     float sumall = 0;
     
     // skip 1st time sample, work-around for P5
     const int tstart = isP5 ? 1 : 0;
     
     for (int k = tstart; k < 10; k++) {
     //for (int k=0; k < 10; k++) {
     //for (int k=2; k < 6; k++) {
        const double p = shunt*pulse11[j][k];
        avgPS[rm][ch][led_trigger]->Fill(k, p);
        sumall += p;
        //if (sumall < p) sumall = p;
     }
     
     spectr11[rm][led_trigger]->Fill(ch, sumall);
     signalmap11[rm][led_trigger]->Fill(ieta11[j], iphi11[j], sumall);
   }
    
    // QIE8
    //cout << "  numChs8 = " << numChs8 << endl;
    
    for (int j=0; j < numChs8; j++) {
      const int rm = depth8[j] - 1;
      const int ch = ieta8[j];
      //cout << "  j = " << j << " rm = " << rm << " ch = " << ch << endl;
      
      // populate spectra plots
      // pulse shape plots
      float sumall = 0;
      
      for (int k = 0; k < 10; k++) {
        const float p = pulse8[j][k];
        pulseshape8[rm][ch][led_trigger]->Fill(k, p);
        sumall += p;
      }
      
      /*
      if (sumall < 35) {
        cout << "event#" << i << " HPD-" << rm << "-" << ch << "-led=" << led_trigger << " pulse = ";
        for (int k = 0; k < 10; k++) cout << pulse8[j][k] << " ";
        cout << " | adc = ";
        for (int k = 0; k < 10; k++) cout << (int)pulse_adc8[j][k] << " ";
        cout << endl;
      }
      */
      
      spectr8[rm][led_trigger]->Fill(ch, sumall);
      signalmap8[rm][led_trigger]->Fill(ch, sumall);
    }
    
  }
  
  cout << endl;
  
  // === draw pulse shape
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / pulse shape", fname.Data(), rm);
    TCanvas* c = new TCanvas(label, label);
    c->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c->cd(1 + (f-1) + 8*(6-fch))->AddExec("exec1", "exec1()");
        
        const int ch = 10*f + fch;
        avgPS[rm][ch][1]->SetLineColor(kRed);
        avgPS[rm][ch][1]->Draw();
        avgPS[rm][ch][0]->Draw("same");
      }
    }
  }
  
  // === draw spectra vs channel plot
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge spectrum", fname.Data(), rm);
    TCanvas* c1 = new TCanvas(label, label);
    c1->Divide(1, 2);
    TPad* p1 = (TPad*) c1->cd(1);
    p1->SetLogz();
    p1->SetLogy();
    p1->SetGridx();
    spectr11[rm][1]->Draw("colz");
    zoom_to_ycontents(spectr11[rm][1]);
    
    TPad* p2 = (TPad*) c1->cd(2);
    p2->SetLogz();
    p2->SetGridx();
    spectr11[rm][0]->Draw("colz");
    zoom_to_ycontents(spectr11[rm][0]);
  }
  
  
  // === draw histograms of LED signal distributions
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge histograms LED=1", fname.Data(), rm);
    TCanvas* c4 = new TCanvas(label, label);
    c4->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c4->cd(1 + (f-1) + 8*(6-fch));
        const int ch = 10*f + fch;
        const int bin = 1 + ch;
        TH1D* p = spectr11[rm][1]->ProjectionY(label, bin, bin);
        label.Form("RM%d ch%d LED=1;total charge, fC;#events", rm, ch);
        p->SetTitle(label);
        p->Draw("HIST");
        zoom_to_xcontents(p);
      }
    }
  }
  
  
  TH1D* spc1d_10 = new TH1D("spc1d_10", "Single pixel charge (p1-p0);Charge, fC;Channels", 50, 30, 50);
  TH1D* spc1d_21 = new TH1D("spc1d_21", "Single pixel charge (p2-p1);Charge, fC;Channels", 50, 30, 50);
  TH1D* spc1d_32 = new TH1D("spc1d_32", "Single pixel charge (p3-p2);Charge, fC;Channels", 50, 30, 50);
  double SPCdata[5][100][2] = {0};    // [RM][ch][led]
  
  for (int rm = 1; rm <= 4; ++rm) {
    label.Form("%s / QIE11-SiPM RM%d / total charge histograms LED=0", fname.Data(), rm);
    TCanvas* c = new TCanvas(label, label);
    //c->AddExec("exec1", "exec1()");
    c->Divide(8, 6);
    
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        c->cd(1 + (f-1) + 8*(6-fch))->AddExec("exec1", "exec1()");
        const int ch = 10*f + fch;
        const int bin = 1 + ch;
        TH1D* p = spectr11[rm][0]->ProjectionY(label, bin, bin);
        label.Form("RM%d ch%d LED=0;total charge, fC;#events", rm, ch);
        p->SetTitle(label);
        p->Draw("HIST");
        zoom_to_xcontents(p);
        
        SPCdata[rm][ch][0] = 0;
        
        if (p->GetEntries() == 0) continue;
        
        cout << "rm" << rm << "-ch" << ch << " peaks:" << endl;
        
        vector<double> peak;
        // initial estimate of peak position and single pixel charge
        double peakpos = p->GetBinCenter(p->GetMaximumBin());
        const double gain = 40;
        
        for (int i = 0; i < 4; i++) {
          cout << "  PEAK" << i << " estimate = " << peakpos << endl;
          TFitResultPtr fitres = p->Fit("gaus", "IQS+", "", peakpos - gain/2, peakpos + gain/2);
          cout << "  fit status = " << fitres << endl;
          if (fitres < 0) break;
          peakpos = fitres->Parameter(1);
          cout << "  gaus fit params: mean = " << peakpos
               << "  sigma = " << fitres->Parameter(2)
               << "  chi2 = " << fitres->Chi2()
               << "  ndof = " << fitres->NFreeParameters()
               << "  ntot = " << fitres->NPar()
               << endl;
          peak.push_back(peakpos);
          
          // next peak position
          peakpos += (i < 1) ? gain : (peak[i] - peak[i-1]);
        }
        
        const double SPC10 = (peak.size() > 1) ? (peak[1] - peak[0]) : 0;
        const double SPC21 = (peak.size() > 2) ? (peak[2] - peak[1]) : 0;
        const double SPC32 = (peak.size() > 3) ? (peak[3] - peak[2]) : 0;
        SPCdata[rm][ch][0] = SPC10;
        
        spc1d_10->Fill(SPC10);
        spc1d_21->Fill(SPC21);
        spc1d_32->Fill(SPC32);
        
        cout << "  Peaks detected = " << peak.size()
             << " SPC10 = " << SPC10
             << " SPC21 = " << SPC21
             << " SPC32 = " << SPC32
             << endl;
        cout << endl;
      }
    }
  }
  
  // === draw single pixel charge distribution
  label.Form("%s / QIE11-SiPM / single pixel charge (p1-p0) LED=0", fname.Data());
  TCanvas* c = new TCanvas(label, label);
  spc1d_10->Draw();
  
  label.Form("%s / QIE11-SiPM / single pixel charge (p2-p1) LED=0", fname.Data());
  c = new TCanvas(label, label);
  spc1d_21->Draw();
  
  label.Form("%s / QIE11-SiPM / single pixel charge (p3-p2) LED=0", fname.Data());
  c = new TCanvas(label, label);
  spc1d_32->Draw();
  
  // === draw signal maps mean and rms
  for (int rm = 1; rm <= 4; ++rm) {
    TH2D* meansignalmap[2];
    TH2D* rmssignalmap[2];
    
    for (int led=0; led<2; led++) {
      label.Form("qie11 mean RM%d led=%d", rm, led);
      meansignalmap[led] = signalmap11[rm][led]->ProjectionXY(label, "E");
      meansignalmap[led]->SetTitle(label + ";fiber;channel");
      
      label.Form("qie11 rms RM%d led=%d", rm, led);
      rmssignalmap[led] = signalmap11[rm][led]->ProjectionXY(label, "E C=E");
      rmssignalmap[led]->SetTitle(label + ";fiber;channel");
    }
    
    label.Form("%s / QIE11-SiPM RM%d / total charge map", fname.Data(), rm);
    TCanvas* c2 = new TCanvas(label, label);
    c2->Divide(2,2);
    c2->cd(1)->SetLogz();
    meansignalmap[1]->Draw("colz text");
    axis_center_labels(meansignalmap[1]->GetXaxis());
    axis_center_labels(meansignalmap[1]->GetYaxis());
    meansignalmap[1]->SetStats(0);
    c2->cd(2)->SetLogz();
    meansignalmap[0]->Draw("colz text");
    axis_center_labels(meansignalmap[0]->GetXaxis());
    axis_center_labels(meansignalmap[0]->GetYaxis());
    meansignalmap[0]->SetStats(0);
    c2->cd(3)->SetLogz();
    rmssignalmap[1]->Draw("colz text");
    axis_center_labels(rmssignalmap[1]->GetXaxis());
    axis_center_labels(rmssignalmap[1]->GetYaxis());
    rmssignalmap[1]->SetStats(0);
    c2->cd(4)->SetLogz();
    rmssignalmap[0]->Draw("colz text");
    axis_center_labels(rmssignalmap[0]->GetXaxis());
    axis_center_labels(rmssignalmap[0]->GetYaxis());
    rmssignalmap[0]->SetStats(0);
  }
  
  // === draw reference apd
  label.Form("%s / QADC-APD / amplitude histograms", fname.Data());
  TCanvas* c3 = new TCanvas(label, label);
  
  THStack* hs = new THStack(label, label + ";adc counts;#events");
  hs->Add(apd[1], "sames");
  hs->Add(apd[0], "sames");
  hs->Draw("nostack");
  zoom_to_xcontents(hs);
  
  // special magic to display stats box
  // https://root.cern.ch/phpBB3/viewtopic.php?p=1094#p1094
  c3->Update();
  TPaveStats* st0 = (TPaveStats*) apd[0]->GetListOfFunctions()->FindObject("stats");
  TPaveStats* st1 = (TPaveStats*) apd[1]->GetListOfFunctions()->FindObject("stats");
  const double vsize = st0->GetY2NDC() - st0->GetY1NDC();
  const double gap = 0.01;
  st0->SetY1NDC(st1->GetY1NDC() - vsize - gap);
  st0->SetY2NDC(st1->GetY1NDC() - gap);
  c3->Modified();
  
  // gaus fit of APD data
  //TFitResultPtr apdfit = apd[1]->Fit("gaus", "QMS", "same");
  //apdfit->Print("V");
  //TFitResultPtr pedapdfit = apd[0]->Fit("gaus", "QMS", "same");
  //pedapdfit->Print("V");
  
  // === QIE8 draw pulse shape
  for (int rm = 0; rm < 2; rm++) {
    TString label;
    label.Form("%s / QIE8-HPD RM%d / pulse shape", fname.Data(), rm+1);
    TCanvas* c = new TCanvas(label, label);
    c->DivideSquare(18);
    
    for (int ch = 0; ch < 18; ch++) {
      c->cd(ch+1)->AddExec("exec1", "exec1()");
      
      label.Form("QIE8 pulse shape RM%d ch%d", rm+1, ch);
      THStack* hs = new THStack(label, label + ";time sample;avg. amplitude, fC");
      hs->Add(pulseshape8[rm][ch][0]);
      hs->Add(pulseshape8[rm][ch][1]);
      hs->Draw();
      axis_center_labels(hs->GetXaxis());
    }
  }
  
  // === QIE8 draw spectr vs channel plot
  for (int rm = 0; rm < 2; rm++) {
    TString label;
    label.Form("%s / QIE8-HPD RM%d / spectrum", fname.Data(), rm+1);
    TCanvas* c = new TCanvas(label, label);
    c->Divide(1, 2);
    c->cd(1);
    c->GetPad(1)->SetLogz();
    spectr8[rm][1]->Draw("colz");
    zoom_to_ycontents(spectr8[rm][1]);
    
    c->cd(2);
    c->GetPad(2)->SetLogz();
    spectr8[rm][0]->Draw("colz");
    zoom_to_ycontents(spectr8[rm][0]);
  }
  
  // === QIE8 draw signal maps
  /*
  TH1D* meansignalmap8[2][2];
  TH1D* rmssignalmap8[2][2];
  
  for (int rm = 0; rm < 2; rm++) {
    for (int led = 0; led < 2; led++) {
      label.Form("qie8 rm%d mean led=%d", rm+1, led);
      meansignalmap8[rm][led] = signalmap8[rm][led]->ProjectionX(label, "E");
      meansignalmap8[rm][led]->SetTitle(label + ";channel");
      
      label.Form("qie8 rm%d rms led=%d", rm+1, led);
      rmssignalmap8[rm][led] = signalmap8[rm][led]->ProjectionX(label, "E C=E");
      rmssignalmap8[rm][led]->SetTitle(label + ";channel");
    }
  }
  */
  
  // === prepare output data
  Measurement m;
  m.fname = fname;
  
  {
    ChannelSignal c;
    c.id = "APD";
    //c.led.x = apdfit->Parameter(1);
    //c.led.rms = apdfit->Parameter(2);
    //c.ped.x = pedapdfit->Parameter(1);
    //c.ped.rms = pedapdfit->Parameter(2);
    c.led.x = apd[1]->GetMean();
    c.led.rms = apd[1]->GetRMS();
    c.led.n = apd[1]->GetEntries();
    c.ped.x = apd[0]->GetMean();
    c.ped.rms = apd[0]->GetRMS();
    c.ped.n = apd[0]->GetEntries();
    c.SPC = 0;
    m.APD.push_back(c);
  }
  
  // QIE11
  for (int rm = 1; rm <= 4; ++rm) {
    for (int f=1; f<=8; f++) {
      for(int fch=1; fch<=6; fch++) {
        
        const int ch = 10*f + fch;
        ChannelSignal c;
        c.id.Form("SiPM-rm%d-ch%d", rm, ch);
        c.led.x = signalmap11[rm][1]->GetBinContent(f, fch);
        c.led.rms = signalmap11[rm][1]->GetBinError(f, fch);
        c.led.n = signalmap11[rm][1]->GetBinEntries(signalmap11[rm][1]->GetBin(f, fch));
        
        c.ped.x = signalmap11[rm][0]->GetBinContent(f, fch);
        c.ped.rms = signalmap11[rm][0]->GetBinError(f, fch);
        c.ped.n = signalmap11[rm][0]->GetBinEntries(signalmap11[rm][0]->GetBin(f, fch));
        
        c.SPC = SPCdata[rm][ch][0];
        
        for (size_t i = 0; i < ChannelSignal::nTS; ++i) {
          c.ts[i].x = avgPS[rm][ch][1]->GetBinContent(i+1) - avgPS[rm][ch][0]->GetBinContent(i+1);
          c.ts[i].err = sqrt(pow(avgPS[rm][ch][1]->GetBinError(i+1), 2) + pow(avgPS[rm][ch][0]->GetBinError(i+1), 2));
        }
        
        m.SiPM.push_back(c);
      }
    }
  }
  
  // QIE8
  for (int rm=0; rm<2; rm++) {
    for (int ch=0; ch<18; ch++) {
      
      ChannelSignal c;
      c.id.Form("HPD-rm%d-ch%d", rm+1, ch);
      c.led.x = signalmap8[rm][1]->GetBinContent(1+ch);
      c.led.rms = signalmap8[rm][1]->GetBinError(1+ch);
      c.led.n = signalmap8[rm][1]->GetBinEntries(1+ch);
      
      c.ped.x = signalmap8[rm][0]->GetBinContent(1+ch);
      c.ped.rms = signalmap8[rm][0]->GetBinError(1+ch);
      c.ped.n = signalmap8[rm][0]->GetBinEntries(1+ch);
      
      c.SPC = 0;
      
      for (size_t i = 0; i < ChannelSignal::nTS; ++i) {
        c.ts[i].x = pulseshape8[rm][ch][1]->GetBinContent(i+1) - pulseshape8[rm][ch][0]->GetBinContent(i+1);
        c.ts[i].err = sqrt(pow(pulseshape8[rm][ch][1]->GetBinError(i+1), 2) + pow(pulseshape8[rm][ch][0]->GetBinError(i+1), 2));
      }
      
      m.HPD.push_back(c);
    }
  }
  
  // print summary
  m.print();
  
  return m;
}


int find_channel_by_id(const TString& id, const vector<ChannelId>& channels)
{
  for (size_t i = 0; i < channels.size(); ++i) {
    if (id == channels[i].id) return i;
  }
  
  return -1;
}

void select_channels(vector<ChannelSignal>& signal, const vector<ChannelId>& channels)
{
  // selected channels
  vector<ChannelSignal> sel;
  
  for (ChannelSignal sig : signal) {
    const int i = find_channel_by_id(sig.id, channels);
    if (i < 0) continue;
    sig.tag = channels[i].tag;
    sel.push_back(sig);
  }
  
  signal = sel;
}

void select_channels(vector<Measurement>& meas, const vector<ChannelId>& channels)
{
  for (Measurement& m : meas) {
    select_channels(m.SiPM, channels); // SiPM
    select_channels(m.HPD, channels);  // HPD
  }
}

vector<ChannelId> select_channels(const vector<ChannelId>& channels, const vector<string>& ids)
{
  vector<ChannelId> sel;
  
  for (const string& id : ids) {
    const int i = find_channel_by_id(id, channels);
    if (i < 0) continue;
    
    sel.push_back(channels[i]);
  }
  
  return sel;
}

void set_pedestal_run(vector<Measurement>& meas, const TString& ped_fname)
{
  cout << "set_pedestal_run() ped_fname = " << ped_fname << endl;
  
  int pedidx = -1;
  
  for (size_t i = 0; i < meas.size(); ++i) {
    if (meas[i].fname == ped_fname) {
      pedidx = i;
      break;
    }
  }
  
  if (pedidx < 0) {
    cout << "ERROR: can't find pedestal file" << endl;
    return;
  }
  
  Measurement& pedrun = meas[pedidx];
  
  for (size_t i = 0; i < meas.size(); ++i) {
    // skip pedestal run
    if (meas[i].fname == ped_fname) continue;
    
    // propagate pedestal values
    
    Measurement& run = meas[i];
    
    for (int j = 0; j < run.totalChannels(); ++j) {
      ChannelSignal s = run.getChannel(j);
      const ChannelSignal& p = pedrun.getChannel(j);
      
      // re-compute TS
      if (s.ped.n != 0) {
        cout << "INFO: set_pedestal_run(): ch=" << s.id << " s.ped.n=" << s.ped.n << endl;
        cout << "ERROR: set_pedestal_run(): can't re-compute TS data" << endl;
        continue;
      }
      
      for (size_t k = 0; k < ChannelSignal::nTS; ++k) {
        s.ts[k].x += p.ts[k].x;
        s.ts[k].err = sqrt(pow(s.ts[k].err, 2) + pow(p.ts[k].err, 2));
      }
      
      // propagate pedestal and SPC
      s.ped     = p.ped;
      s.SPC     = p.SPC;
      
      run.setChannel(j, s);
    }
  }
  
  // delete pedestal run entry
  meas.erase(meas.begin() + pedidx);
}

void correct_qie11_shunt(vector<Measurement>& meas, const vector<RunId>& runs)
{
  cout << "correct_qie11_shunt()" << endl;
  
  int i1 = 0;  // index of shunt=1 point
  int i2 = 0;  // index of shunt>1 point
  
  for (size_t i = 1; i < runs.size(); ++i) {
    if (runs[i].shunt != runs[i-1].shunt) {
      if (runs[i].shunt < runs[i-1].shunt) {
        i1 = i;
        i2 = i-1;
      }
      else {
        i1 = i-1;
        i2 = i;
      }
      
      break;
    }
  }
  
  if (i1 == i2) {
    cout << "INFO: no shunt transition detected" << endl;
    return;
  }
  
  const double s1 = runs[i1].shunt;
  const double s2 = runs[i2].shunt;
  
  cout << "shunt transition: "
       << "#" << i1 << ": run=" << runs[i1].runid << " shunt=" << runs[i1].shunt
       << " ---> "
       << "#" << i2 << ": run=" << runs[i2].runid << " shunt=" << runs[i2].shunt
       << endl;
  
  const bool hasRef = (meas[0].HPD.size() > 0);
  if (!hasRef) {
    cout << "WARNING: reference light level data is unknown (no HPD data)" << endl;
  }
  
  const double l1 = hasRef ? meas[i1].HPD[0].signal() : 1.;
  const double l2 = hasRef ? meas[i2].HPD[0].signal() : 1.;
  
  // loop over channels of shunt=1/shunt>1 measurements to extract effective shunt value
  for (size_t i = 0; i < meas[0].SiPM.size(); ++i) {
    const ChannelSignal& c1 = meas[i1].SiPM[i];
    const ChannelSignal& c2 = meas[i2].SiPM[i];
    
    const double s2new = s2 * c1.signal() / c2.signal() * l2 / l1;
    const double corr = s2new / s2;
    cout << "channel " << c2.id << " shunt=" << runs[i2].shunt << " -> " << s2new << ", corr = " << corr << endl;
    
    // loop over light intensity poinst to apply the correction:
    for (size_t j = 0; j < meas.size(); ++j) {
      // skip shunt==1 points
      if (runs[j].shunt == s1) continue;
      
      ChannelSignal& c = meas[j].SiPM[i];
      c.led.x *= corr;
      c.led.rms *= corr;
      c.ped.x *= corr;
      c.ped.rms *= corr;
    }
  }
}

void save_meas_data(const vector<Measurement>& meas, const char* fname)
{
  cout << "save_meas_data(): meas "
       << " napd = " << meas[0].APD.size()
       << " nsipm = " << meas[0].SiPM.size()
       << " nhpd = " << meas[0].HPD.size()
       << ", fname " << fname
       << endl;
  
  // print results
  ofstream f(fname);
  f.precision(15);
  
  // header
  f << "File;";
  for (int j = 0; j < meas[0].totalChannels(); j++) {
    const TString id = meas[0].getChannel(j).id;
    
    f << id << " led;"
      << id << " led_rms;"
      << id << " led_n;"
      << id << " ped;"
      << id << " ped_rms;"
      << id << " ped_n;"
      << id << " signal;"
      << id << " signal_err;"
      << id << " SPC;";
      
      for (size_t k = 0; k < ChannelSignal::nTS; ++k) {
        f << id << " ts" << k << ";"
          << id << " ts" << k << "err;";
      }
  }
  
  f << endl;
  
  // data
  for (size_t i = 0; i < meas.size(); i++) {
    f << meas[i].fname << ";";
    
    for (int j = 0; j < meas[i].totalChannels(); j++) {
      const ChannelSignal& cs = meas[i].getChannel(j);
      
      f << cs.led.x << ";"
        << cs.led.rms << ";"
        << cs.led.n << ";"
        << cs.ped.x << ";"
        << cs.ped.rms << ";"
        << cs.ped.n << ";"
        << cs.signal() << ";"
        << cs.signal_err() << ";"
        << cs.SPC << ";";
        
        for (size_t k = 0; k < ChannelSignal::nTS; ++k) {
          f << cs.ts[k].x << ";"
            << cs.ts[k].err << ";";
        }
    }
    
    f << endl;
  }
}


vector<string> split(const string& s, const string& delim)
{
  //cout << "split() s=" << s << " delim=" << delim << endl;
  vector<string> v;
  size_t from = 0;
  
  while (from < s.size()) {
    size_t to = s.find_first_of(delim, from);
    if (to == string::npos) to = s.size();
    const size_t count = (to - from);
    //cout << "from=" << from << " to=" << to << " count=" << count << endl;
    
    v.push_back(s.substr(from, count));
    from = to + 1;
  }
  
  return v;
}

// convert string to value
template<typename T>
T cast(const string& s)
{
  istringstream iss(s);
  T x;
  iss >> x;
  return x;
}

bool load_meas_data(vector<Measurement>& meas, const char* fname)
{
  cout << "load_meas_data() fname=" << fname << endl;
  meas.clear();
  
  ifstream f(fname);
  if (!f) return false;
  
  // read header
  string line;
  getline(f, line);
  const vector<string> header = split(line, ";");
  vector<string> id;
  
  const int nDATA = 9 + 2*ChannelSignal::nTS;
  
  for (size_t i = 1; i < header.size(); i += nDATA) {
    id.push_back(split(header[i], " ")[0]);
  }
  
  cout << "n_channels = " << id.size() << endl;
  
  while (getline(f, line)) {
    const vector<string> field = split(line, ";");
    
    // skip broken lines
    if (1 + nDATA*id.size() != field.size()) continue;
    
    Measurement m;
    m.fname = field[0];
    
    for (size_t i = 0; i < id.size(); i++) {
      ChannelSignal s;
      s.id = id[i];
      s.led.x   = cast<double>(field[1 + nDATA*i + 0]);
      s.led.rms = cast<double>(field[1 + nDATA*i + 1]);
      s.led.n   =    cast<int>(field[1 + nDATA*i + 2]);
      s.ped.x   = cast<double>(field[1 + nDATA*i + 3]);
      s.ped.rms = cast<double>(field[1 + nDATA*i + 4]);
      s.ped.n   =    cast<int>(field[1 + nDATA*i + 5]);
      s.SPC     = cast<double>(field[1 + nDATA*i + 8]);
      
      
      for (size_t k = 0; k < ChannelSignal::nTS; ++k) {
        s.ts[k].x   = cast<double>(field[1 + nDATA*i + 9 + 2*k]);
        s.ts[k].err = cast<double>(field[1 + nDATA*i + 9 + 2*k+1]);
      }
      
      const string type = split(id[i], "-")[0];
      if (type == "APD") m.APD.push_back(s);
      if (type == "SiPM") m.SiPM.push_back(s);
      if (type == "HPD") m.HPD.push_back(s);
    }
    
    meas.push_back(m);
  }
  
  cout << "n_measurements = " << meas.size() << endl;
  
  return (meas.size() != 0);
}

void print_multi_graph(TMultiGraph* mg);

RunsPlots make_plots(const vector<Measurement>& meas, const RunsSet& rset)
{
  cout << "make_plots(): rset = " << rset.title << ", #runs = " << rset.runs.size()
       << ", #meas = " << meas.size() << ", #channels = " <<  meas[0].totalChannels()  << endl;
  
  RunsPlots rp;
  const TString setname = rset.name();
  
  // 1) ===> prepare SiPM vs ref plot
  TMultiGraph* mg = new TMultiGraph;
  mg->SetTitle(setname + " / SiPM vs Ref;Ref counts;SiPM charge, fC");
  
  // prepare colors list per each tag
  map<TString, int> tagcolor;
  
  // collect list of tags
  for (size_t j = 0; j < meas[0].SiPM.size(); j++) {
    const TString& tag = meas[0].SiPM[j].tag;
    tagcolor[tag] = 1;
  }
  
  // assign colors
  TLegend* tagleg = new TLegend(0.55, 0.78, 0.65, 0.88);
  int idxcol = 0;
  for (auto& p : tagcolor) {
  //for (auto p = tagcolor.begin(); p != tagcolor.end(); ++p) {
    // skip bad colors / white: 0, 10
    if (idxcol == 0) idxcol++;
    if (idxcol == 10) idxcol++;
    p.second = idxcol;
    idxcol++;
    
    TBox* entry = new TBox;
    entry->SetFillColor(p.second);
    tagleg->AddEntry(entry, p.first, "F");
  }
  
  const int refHPD = 0;
  cout << "INFO: reference HPD: index = " << refHPD << "  name = " << meas[0].HPD[refHPD].id << endl;
  
  for (size_t j = 0; j < meas[0].SiPM.size(); j++) {
    const int n = meas.size();
    double x[n];
    double y[n];
    double ex[n];
    double ey[n];
    
    for (size_t i = 0; i < meas.size(); i++) {
      //x[i] = meas[i].APD.signal();
      x[i] = meas[i].HPD[refHPD].signal();
      y[i] = meas[i].SiPM[j].signal();
      //ex[i] = meas[i].APD.signal_err();
      ex[i] = meas[i].HPD[refHPD].signal_err();
      ey[i] = meas[i].SiPM[j].signal_err();
      
      // TODO: ??? make optional
      
      // average HPD
      x[i] = 0;
      ex[i] = 0;
      const size_t nHPD = meas[i].HPD.size();
      for (size_t k = 0; k < nHPD; ++k) {
        x[i] += meas[i].HPD[k].signal();
        ex[i] += meas[i].HPD[k].signal_err();
      }
      x[i] /= nHPD;
      ex[i] /= nHPD;
    }
    
    TGraphErrors* g = new TGraphErrors(n, x, y, ex, ey);
    g->SetTitle(meas[0].SiPM[j].id);
    g->SetMarkerStyle(20+j);
    g->SetFillColor(0);
    const int color = tagcolor[meas[0].SiPM[j].tag];
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg->Add(g);
  }
  
  const int legn = mg->GetListOfGraphs()->GetSize();
  const int legny = min(legn, 10);
  const int legnx = (legn+9) / legny;
  cout << "chleg: n = " << legn << " nx = " << legnx << " ny= " << legny << endl;
  
  TCanvas* c1 = new TCanvas("c1", "1-raw");
  c1->SetGrid();
  mg->DrawClone("APL");
  rp.plots[0] = (TMultiGraph*) mg->Clone();
  TLegend* chleg = c1->BuildLegend(0.15, 0.88 - 0.02*legny, 0.15 + 0.05*legnx, 0.88);
  chleg->SetNColumns(legnx);
  tagleg->DrawClone();
  c1->Print(setname + "-1-sipm-vs-ref.pdf");
  
  print_multi_graph(mg);
  
  
  
  
  // 2) ===> convert SiPM fC to pixels
  
  // find the lowest light run:
  double low = 1e9;
  int lowIdx = 0;
  
  for (size_t i = 0; i < meas.size(); i++) {
    const double ref = meas[i].HPD[refHPD].signal();
    if (ref < low) { low = ref; lowIdx = i; }
  }
  
  cout << "Lowest light run = #" << lowIdx
       << " fname = " << rset.runs[lowIdx].fname()
       << " shunt = " << rset.runs[lowIdx].shunt
       << endl;
  
  const TList* list = mg->GetListOfGraphs();
  for (int i = 0; i < list->GetSize(); ++i) {
    TGraphErrors* g = (TGraphErrors*) list->At(i);
    const int n = g->GetN();
    double* y = g->GetY();
    double* ey = g->GetEY();
    
    //const double SPC = rset.channels[i].sipm_SPC;
    const double SPC = meas[lowIdx].SiPM[i].SPC;
    //const double SPC = 1;
    
    cout << "channel = " << g->GetTitle()
         << " |"
         << " approximate SPC = " << rset.channels[i].sipm_SPC << " fC"
         << " | calculated SPC " << meas[lowIdx].SiPM[i].SPC << " fC"
         << " | used SPC " << SPC << " fC"
         << endl;
    
    for (int j = 0; j < n; ++j) {
      y[j] /= SPC;
      ey[j] /= SPC;
    }
  }
  
  TCanvas* c2 = new TCanvas("c2", "2-sipm_pixels");
  c2->SetGrid();
  mg->SetTitle(setname + " / SiPM vs Ref;Ref counts;SiPM pixels");
  mg->DrawClone("APL");
  rp.plots[1] = (TMultiGraph*) mg->Clone();
  chleg->DrawClone();
  tagleg->DrawClone();
  c2->Print(setname + "-2-sipm-pixels-vs-ref.pdf");
  
  
  
  // 2a) ===> SiPM/HPD vs Ref
  TMultiGraph* mg2a = (TMultiGraph*) mg->Clone();
  mg2a->SetTitle(setname + " / HPD to SiPM;HPD, fC;HPD/SiPM, fC/pixels");
  TIter next2a(mg2a->GetListOfGraphs());
  while (TGraphErrors* g = (TGraphErrors*)next2a()) {
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    for (int j = 0; j < n; ++j) {
      const bool fail = (y[j] == 0. || x[j] == 0.);
      const double f = fail ? 0 : x[j] / y[j];
      const double ef = fail ? 0 : f * (fabs(ex[j]/x[j]) + fabs(ey[j]/y[j]));
      y[j] = f;
      ey[j] = ef;
    }
  }
  
  TCanvas* c2a = new TCanvas("c2a", "2a-ref_to_sipm");
  c2a->SetGridy();
  mg2a->DrawClone("APL");
  chleg->DrawClone();
  tagleg->DrawClone();
  c2a->Print(setname + "-2a-ref_to_sipm.pdf");
  
  
  // 2b) ===> SiPM/HPD vs SiPM
  TMultiGraph* mg2b = (TMultiGraph*) mg->Clone();
  mg2b->SetTitle(setname + " / HPD to SiPM;SiPM, pixels;HPD/SiPM, fC/pixels");
  TIter next2b(mg2b->GetListOfGraphs());
  while (TGraphErrors* g = (TGraphErrors*)next2b()) {
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    for (int j = 0; j < n; ++j) {
      const bool fail = (y[j] == 0. || x[j] == 0.);
      const double f = fail ? 0 : x[j] / y[j];
      const double ef = fail ? 0 : f * (fabs(ex[j]/x[j]) + fabs(ey[j]/y[j]));
      x[j] = y[j];
      y[j] = f;
      ex[j] = ey[j];
      ey[j] = ef;
    }
  }
  
  TCanvas* c2b = new TCanvas("c2b", "2b-ref_to_sipm");
  c2b->SetGridy();
  mg2b->DrawClone("APL");
  chleg->DrawClone();
  tagleg->DrawClone();
  c2b->Print(setname + "-2b-ref_to_sipm.pdf");
  
  
  
  // 3) ===> fit first points to calc ref effective p.e. scale
  cout << "SiPM pixels linear range min = " << lin_min_x << " max = " << lin_max_x << endl;
  vector<TF1*> apd_count_to_pe(list->GetSize(), 0);
  //TF1* f = new TF1("f", "[0]*x + [1]", 0, 1000);
  TF1* f = new TF1("f", "[0]*x", 0, 1);
  for (int i = 0; i < list->GetSize(); ++i) {
    TGraphErrors* g = (TGraphErrors*) list->At(i);
    
    // calculate fit range
    TGraph q(g->GetN(), g->GetY(), g->GetX());
    const double fit_min_x = q.Eval(lin_min_x);
    const double fit_max_x = q.Eval(lin_max_x);
    
    f->SetRange(fit_min_x, fit_max_x);
    
    TFitResultPtr fitres = g->Fit(f, "Q M S N", "", fit_min_x, fit_max_x);
    
    cout << "channel = " << g->GetTitle()
         << " |"
         << " range = " << fit_min_x << " " << fit_max_x
         << " eval(minx) = " << f->Eval(fit_min_x)
         << " eval(maxx) = " << f->Eval(fit_max_x)
         << " |"
         << " fit status = " << (int) fitres;
    
    if (fitres >= 0) {
    cout << " fit slope  = " << fitres->Parameter(0)
         //<< " intercept = " << fitres->Parameter(1)
         << " status = " << (int) fitres
         << " chi2/ndf = " << fitres->Chi2() << " / " << fitres->Ndf()
         << " = " << fitres->Chi2() / fitres->Ndf()
         << endl;
    //fitres->Print();
    }
    
    f->SetLineWidth(1);
    f->SetLineColor(g->GetLineColor());
    
    apd_count_to_pe[i] = (TF1*) f->Clone();
  }
  
  /*
  TCanvas* c3 = new TCanvas("c3", "3-fit");
  //TMultiGraph* mgfit = (TMultiGraph*)
  mg->DrawClone("APL");
  for (size_t i = 0; i < apd_count_to_pe.size(); ++i)
    apd_count_to_pe[i]->DrawCopy("L SAME");
  
  chleg->DrawClone();
  tagleg->DrawClone();
  c3->Print(setname + "-3-ref-vs-sipm-pixels-fit.pdf");
  
  // zoom to see fit quality
  //mgfit->GetXaxis()->SetLimits(fit_mix_x, fit_max_x);
  //mgfit->SetMinimum(0);
  //mgfit->SetMaximum(1000);
  //mgfit->Draw("P");
  //c3->Print(setname + "-3-ref-vs-sipm-pixels-fit-zoom.pdf");
  */
  
  // 4) ===> apply apd scale
  for (int i = 0; i < list->GetSize(); ++i) {
    TGraphErrors* g = (TGraphErrors*) list->At(i);
    const int n = g->GetN();
    double* x = g->GetX();
    double* ex = g->GetEX();
    for (int j = 0; j < n; ++j) {
      //cout << "apply scale: x = " << x[j] << " ex = " << ex[j] << " ==> ";
      const double px = x[j];
      x[j] = apd_count_to_pe[i]->Eval(px);
      ex[j] *= apd_count_to_pe[i]->Derivative(px);
      //cout << "x = " << x[j] << " ex = " << ex[j] << endl;
    }
  }
  
  // unity line APD 1 p.e. = SiPM 1 pixel
  TF1* funi = new TF1("funi", "x", -1e6, 1e6);
  funi->SetLineColor(kBlack);
  funi->SetLineStyle(2);
  funi->SetLineWidth(1);
  funi->SetTitle("unity");
  
  TCanvas* c4 = new TCanvas("c4", "4-ref_pe");
  c4->SetGrid();
  mg->SetTitle(setname + " / SiPM vs Ref;Ref effective p.e.;SiPM pixels");
  TMultiGraph* mgscale = (TMultiGraph*) mg->DrawClone("APL");
  rp.plots[2] = (TMultiGraph*) mg->Clone();
  funi->DrawCopy("L SAME");
  chleg->DrawClone();
  tagleg->DrawClone();
  c4->Print(setname + "-4-ref-vs-sipm-pixels-refscale.pdf");
  
  // zoom
  //mgscale->GetXaxis()->SetLimits(0, 1500);
  //mgscale->SetMinimum(0);
  //mgscale->SetMaximum(1500);
  //mgscale->Draw("P");
  //c4->Print(setname + "-4-ref-vs-sipm-pixels-refscale-zoom.pdf");
  
  // 5) ===> correction factor vs Ref
  TMultiGraph* mgapd = (TMultiGraph*) mg->Clone();
  mgapd->SetTitle(setname + " / Correction factor;Ref effective p.e.;Correction factor");
  TIter next3(mgapd->GetListOfGraphs());
  while (TGraphErrors* g = (TGraphErrors*)next3()) {
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    for (int j = 0; j < n; ++j) {
      const bool fail = (y[j] == 0. || x[j] == 0.);
      const double f = fail ? 0 : x[j] / y[j];
      const double ef = fail ? 0 : f * (fabs(ex[j]/x[j]) + fabs(ey[j]/y[j]));
      y[j] = f;
      ey[j] = ef;
    }
  }
  
  //print_multi_graph(mgapd);
  
  TF1* fconst = new TF1("fconst", "1", -1e6, 1e6);
  fconst->SetLineColor(kBlack);
  fconst->SetLineStyle(2);
  fconst->SetLineWidth(1);
  fconst->SetTitle("unity");
  
  /*
  TCanvas* c5 = new TCanvas("c5", "5-corr_ref");
  TMultiGraph* scale1 = (TMultiGraph*) mgapd->DrawClone("APL");
  rp.plots[3] = mgapd;
  fconst->DrawCopy("L SAME");
  chleg->DrawClone();
  tagleg->DrawClone();
  scale1->SetMinimum(0.5);
  scale1->SetMaximum(2.0);
  c5->Print(setname + "-5-corr-vs-ref.pdf");
  */
  
  // 6) ===> correction factor vs SiPM
  TMultiGraph* mgsipm = (TMultiGraph*) mg->Clone();
  mgsipm->SetTitle(setname + " / Correction factor;SiPM pixels;Correction factor");
  TIter next4(mgsipm->GetListOfGraphs());
  while (TGraphErrors* g = (TGraphErrors*)next4()) {
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    for (int j = 0; j < n; ++j) {
      const bool fail = (y[j] == 0. || x[j] == 0.);
      const double f = fail ? 0 : x[j] / y[j];
      const double ef = fail ? 0 : f * (fabs(ex[j]/x[j]) + fabs(ey[j]/y[j]));
      x[j] = y[j];
      y[j] = f;
      ex[j] = ey[j];
      ey[j] = ef;
    }
  }
  
  TCanvas* c6 = new TCanvas("c6", "6-corr_sipm");
  c6->SetGrid();
  TMultiGraph* scale2 = (TMultiGraph*) mgsipm->DrawClone("APL");
  rp.plots[4] = mgsipm;
  
  gStyle->SetOptFit(1);
  TF1* corrf = new TF1("corrf", "pol2", 10, 30000);
  corrf->FixParameter(0, 1.);
  corrf->SetLineColor(kBlue);
  scale2->Fit(corrf, "W");
  scale2->Fit(corrf, "");
  
  fconst->DrawCopy("L SAME");
  chleg->DrawClone();
  tagleg->DrawClone();
  //scale2->SetMinimum(0.5);
  scale2->SetMaximum(2.0);
  c6->Print(setname + "-6-corr-vs-sipm.pdf");
  
  // 7) ===> prepare correction factor mean and spread (RMS/mean) plots
  TGraphErrors* grms = new TGraphErrors;
  grms->SetTitle(setname + " / Correction factor spread (RMS/MEAN);SiPM pixels;RMS/MEAN, %");
  grms->SetMarkerStyle(kFullSquare);
  grms->SetFillColor(0);
  
  TGraphErrors* gmean = new TGraphErrors;
  gmean->SetTitle(setname + " / Correction factor MEAN;SiPM pixels;Correction factor");
  gmean->SetMarkerStyle(kFullSquare);
  gmean->SetFillColor(0);
  
  double maxX = 0;
  {
    TList* list = mgsipm->GetListOfGraphs();
    for (int i = 0; i < list->GetSize(); ++i ) {
      TGraphErrors* g = (TGraphErrors*) list->At(i);
      const int xn = g->GetN();
      const double* xdata = g->GetX();
      
      for (int j = 0; j < xn; ++j) {
        if (maxX < xdata[j]) maxX = xdata[j];
      }
    }
  }
  
  cout << "RMS/MEAN maxX = " << maxX << endl;
  
  
  for (double x = 0; x < maxX; x+=500) {
    int n = 0;
    double S = 0;
    double S2 = 0;
    
    TList* list = mgsipm->GetListOfGraphs();
    for (int i = 0; i < list->GetSize(); ++i ) {
      TGraphErrors* g = (TGraphErrors*) list->At(i);
      const int xn = g->GetN();
      const double* xdata = g->GetX();
      int nlow = 0;
      int nhigh = 0;
      
      for (int j = 0; j < xn; ++j) {
        if (xdata[j] <= x) nlow++;
        if (xdata[j] >= x) nhigh++;
      }
      
      // x point is out of range
      if (nlow == 0 || nhigh == 0) continue;
      
      const double v = g->Eval(x);
      n++;
      S += v;
      S2 += v*v;
    }
    
    if (n == 0) continue;
    
    const double mean = S/n;
    const double rms = sqrt(S2/n - mean*mean);
    const double spread = 100*rms/mean;
    const int xi = grms->GetN();
    grms->SetPoint(xi, x, spread);
    //grms->SetPointError(xi, 0, spread/sqrt(n));
    
    gmean->SetPoint(xi, x, mean);
    gmean->SetPointError(xi, 0, rms);
  }
  
  TCanvas* c7 = new TCanvas("c7", "7-corr_sipm-rms");
  c7->SetGrid();
  grms->Draw("APL");
  c7->Print(setname + "-7-corr-vs-sipm-rms.pdf");
  
  TCanvas* c8 = new TCanvas("c8", "8-corr_sipm-mean");
  c8->SetGrid();
  gmean->Draw("APL");
  c8->Print(setname + "-8-corr-vs-sipm-mean.pdf");
  
  return rp;
}

RunsPlots make_plots_di_i_dv(const vector<Measurement>& meas, const RunsSet& rset, const vector<double>& volt)
{
  cout << "make_plots_di_i_dv(): rset = " << rset.title << ", #runs = " << rset.runs.size()
       << ", #meas = " << meas.size() << ", #channels = " <<  meas[0].totalChannels()
       << ", #volt[] = " << volt.size()
       << endl;
  
  //RunsPlots rp;
  const TString setname = rset.name();
  
  // 1) ===> prepare photo current vs Vb plot
  TMultiGraph* mg = new TMultiGraph;
  TMultiGraph* mg0 = new TMultiGraph;
  TMultiGraph* mg1 = new TMultiGraph;
  mg->SetTitle(setname + " / Photo current;ngCCM voltage, V;SiPM current, uA");
  mg0->SetTitle(setname + " / Dark current;ngCCM voltage, V;SiPM current, uA");
  mg1->SetTitle(setname + " / LED current;ngCCM voltage, V;SiPM current, uA");
  
  // prepare colors list per each tag
  /*
  map<TString, int> tagcolor;
  int idxcol = 0;
  
  for (size_t j = 0; j < meas[0].SiPM.size(); j++) {
    const TString& tag = meas[0].SiPM[j].tag;
    if (tagcolor[tag] != 0) continue;
    
    // skip bad colors / white: 0, 10
    if (idxcol == 0) idxcol++;
    if (idxcol == 10) idxcol++;
    tagcolor[tag] = idxcol;
    idxcol++;
  }
  
  TLegend* tagleg = new TLegend(0.35, 0.78, 0.45, 0.88);
  for (const auto& p : tagcolor) {
    TBox* entry = new TBox;
    entry->SetFillColor(p.second);
    tagleg->AddEntry(entry, p.first, "F");
  }
  */
  
  // 
  // 1 fC in (10 TS of 25 ns) in uA conversion constant
  const double K = 1e-15 / (10 * 25 * 1e-9) / 1e-6;
  
  for (size_t j = 0; j < meas[0].SiPM.size(); j++) {
    const int n = meas.size();
    double x[n];
    double y[n];
    double ex[n];
    double ey[n];
    
    for (size_t i = 0; i < meas.size(); i++) {
      x[i] = volt[i];
      y[i] = K* meas[i].SiPM[j].signal();
      ex[i] = 0;
      ey[i] = K * meas[i].SiPM[j].signal_err();
    }
    
    TGraphErrors* g = new TGraphErrors(n, x, y, ex, ey);
    g->SetTitle(meas[0].SiPM[j].id);
    g->SetMarkerStyle(20+j);
    g->SetFillColor(0);
    //const int color = tagcolor[meas[0].SiPM[j].tag];
    const int color = 1;
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg->Add(g);
  }
  
  TCanvas* c1 = new TCanvas("c1", "1-photo");
  c1->SetGrid();
  c1->SetLogy();
  mg->SetMinimum(1e-3);
  mg->DrawClone("APL");
  //rp.plots[0] = (TMultiGraph*) mg->Clone();
  c1->BuildLegend(0.15, 0.88 - 0.01*mg->GetListOfGraphs()->GetSize()/4, 0.35, 0.88)->SetNColumns(4);
  //tagleg->Draw();
  c1->Print(setname + "-1-Iphoto-vs-Vb.pdf");
  
  
  // 2) ===> calculate dI/IdV
  const TList* list = mg->GetListOfGraphs();
  for (int i = 0; i < list->GetSize(); ++i) {
    TGraphErrors* g = (TGraphErrors*) list->At(i);
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    
    /*
    cout << "channel = " << g->GetTitle()
         << " |"
         << " SPC = " << SPC << " fC"
         << endl;
    */
    
    for (int j = 0; j < n-1; ++j) {
      const double dI = y[j+1] - y[j];
      const double dV = x[j+1] - x[j];
      const double I = 0.5 * (y[j+1] + y[j]);
      const double V = 0.5 * (x[j+1] + x[j]);
      
      x[j] = V;
      y[j] = dI / I / dV;
      ex[j] = 0;
      ey[j] = 0;
    }
    
    g->Set(n-1);
  }
  
  TCanvas* c2 = new TCanvas("c2", "2-di_i_dv");
  c2->SetGrid();
  mg->SetMinimum(-0.5);
  mg->SetMaximum(6);
  mg->SetTitle(setname + " / dI/IdV;ngCCM voltage, V; dI/IdV");
  mg->DrawClone("APL");
  //rp.plots[1] = (TMultiGraph*) mg->Clone();
  c2->BuildLegend(0.65, 0.88 - 0.01*mg->GetListOfGraphs()->GetSize()/4, 0.85, 0.88)->SetNColumns(4);
  c2->Print(setname + "-2-diidv-vs-Vb.pdf");
  
  /*
  
  // 3) ===> fit first points to calc APD effective p.e. scale
  cout << "SiPM pixels linear range min = " << lin_min_x << " max = " << lin_max_x << endl;
  vector<TF1*> apd_count_to_pe(list->GetSize(), 0);
  //TF1* f = new TF1("f", "[0]*x + [1]", 0, 1000);
  TF1* f = new TF1("f", "[0]*x", 0, 1);
  for (int i = 0; i < list->GetSize(); ++i) {
    TGraphErrors* g = (TGraphErrors*) list->At(i);
    
    // calculate fit range
    TGraph q(g->GetN(), g->GetY(), g->GetX());
    const double fit_min_x = q.Eval(lin_min_x);
    const double fit_max_x = q.Eval(lin_max_x);
    
    f->SetRange(fit_min_x, fit_max_x);
    
    TFitResultPtr fitres = g->Fit(f, "Q M S N", "", fit_min_x, fit_max_x);
    const double slope = fitres->Parameter(0);
    const double fitq = fitres->Chi2() / fitres->Ndf();
    cout << "channel = " << g->GetTitle()
         << " |"
         << " range = " << fit_min_x << " " << fit_max_x
         << " eval(minx) = " << f->Eval(fit_min_x)
         << " eval(maxx) = " << f->Eval(fit_max_x)
         << " |"
         << " fit slope  = " << slope
         //<< " intercept = " << fitres->Parameter(1)
         << " status = " << (int) fitres
         << " chi2/ndf = " << fitres->Chi2() << " / " << fitres->Ndf()
         << " = " << fitq
         << endl;
    //fitres->Print();
    f->SetLineWidth(1);
    f->SetLineColor(g->GetLineColor());
    
    apd_count_to_pe[i] = (TF1*) f->Clone();
  }
  
  TCanvas* c3 = new TCanvas("c3", "3-fit");
  //TMultiGraph* mgfit = (TMultiGraph*)
  mg->DrawClone("APL");
  for (size_t i = 0; i < apd_count_to_pe.size(); ++i)
    apd_count_to_pe[i]->DrawCopy("L SAME");
  
  c3->BuildLegend(0.15, 0.7, 0.33, 0.88);
  c3->Print(setname + "-3-apd-vs-sipm-pixels-fit.pdf");
  
  // zoom to see fit quality
  //mgfit->GetXaxis()->SetLimits(fit_mix_x, fit_max_x);
  //mgfit->SetMinimum(0);
  //mgfit->SetMaximum(1000);
  //mgfit->Draw("P");
  //c3->Print(setname + "-3-apd-vs-sipm-pixels-fit-zoom.pdf");
  
  // 4) ===> apply apd scale
  for (int i = 0; i < list->GetSize(); ++i) {
    TGraphErrors* g = (TGraphErrors*) list->At(i);
    const int n = g->GetN();
    double* x = g->GetX();
    double* ex = g->GetEX();
    for (int j = 0; j < n; ++j) {
      //cout << "apply scale: x = " << x[j] << " ex = " << ex[j] << " ==> ";
      const double px = x[j];
      x[j] = apd_count_to_pe[i]->Eval(px);
      ex[j] *= apd_count_to_pe[i]->Derivative(px);
      //cout << "x = " << x[j] << " ex = " << ex[j] << endl;
    }
  }
  
  // unity line APD 1 p.e. = SiPM 1 pixel
  TF1* funi = new TF1("funi", "x", -1e6, 1e6);
  funi->SetLineColor(kBlack);
  funi->SetLineStyle(2);
  funi->SetLineWidth(1);
  funi->SetTitle("unity");
  
  TCanvas* c4 = new TCanvas("c4", "4-apd_pe");
  mg->SetTitle(setname + " / SiPM vs APD;APD effective p.e.;SiPM pixels");
  TMultiGraph* mgscale = (TMultiGraph*) mg->DrawClone("APL");
  rp.plots[2] = (TMultiGraph*) mg->Clone();
  funi->DrawCopy("L SAME");
  c4->BuildLegend(0.15, 0.7, 0.33, 0.88);
  c4->Print(setname + "-4-apd-vs-sipm-pixels-apdscale.pdf");
  
  // zoom
  //mgscale->GetXaxis()->SetLimits(0, 1500);
  //mgscale->SetMinimum(0);
  //mgscale->SetMaximum(1500);
  //mgscale->Draw("P");
  //c4->Print(setname + "-4-apd-vs-sipm-pixels-apdscale-zoom.pdf");
  
  // 5) ===> correction factor vs APD
  TMultiGraph* mgapd = (TMultiGraph*) mg->Clone();
  mgapd->SetTitle(setname + " / Correction factor;APD effective p.e.;Correction factor");
  TIter next3(mgapd->GetListOfGraphs());
  while (TGraphErrors* g = (TGraphErrors*)next3()) {
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    for (int j = 0; j < n; ++j) {
      const bool fail = (y[j] == 0. || x[j] == 0.);
      const double f = fail ? 0 : x[j] / y[j];
      const double ef = fail ? 0 : f * (fabs(ex[j]/x[j]) + fabs(ey[j]/y[j]));
      y[j] = f;
      ey[j] = ef;
    }
  }
  
  //print_multi_graph(mgapd);
  
  TF1* fconst = new TF1("fconst", "1", -1e6, 1e6);
  fconst->SetLineColor(kBlack);
  fconst->SetLineStyle(2);
  fconst->SetLineWidth(1);
  fconst->SetTitle("unity");
  
  TCanvas* c5 = new TCanvas("c5", "5-corr_apd");
  TMultiGraph* scale1 = (TMultiGraph*) mgapd->DrawClone("APL");
  rp.plots[3] = mgapd;
  fconst->DrawCopy("L SAME");
  c5->BuildLegend(0.15, 0.7, 0.33, 0.88);
  scale1->SetMinimum(0.5);
  scale1->SetMaximum(2.0);
  c5->Print(setname + "-5-corr-vs-apd.pdf");
  
  // 6) ===> correction factor vs SiPM
  TMultiGraph* mgsipm = (TMultiGraph*) mg->Clone();
  mgsipm->SetTitle(setname + " / Correction factor;SiPM pixels;Correction factor");
  TIter next4(mgsipm->GetListOfGraphs());
  while (TGraphErrors* g = (TGraphErrors*)next4()) {
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    double* ex = g->GetEX();
    double* ey = g->GetEY();
    for (int j = 0; j < n; ++j) {
      const bool fail = (y[j] == 0. || x[j] == 0.);
      const double f = fail ? 0 : x[j] / y[j];
      const double ef = fail ? 0 : f * (fabs(ex[j]/x[j]) + fabs(ey[j]/y[j]));
      x[j] = y[j];
      y[j] = f;
      ex[j] = ey[j];
      ey[j] = ef;
    }
  }
  
  TCanvas* c6 = new TCanvas("c6", "6-corr_sipm");
  TMultiGraph* scale2 = (TMultiGraph*) mgsipm->DrawClone("APL");
  rp.plots[4] = mgsipm;
  fconst->DrawCopy("L SAME");
  c6->BuildLegend(0.15, 0.7, 0.33, 0.88);
  scale2->SetMinimum(0.5);
  scale2->SetMaximum(2.0);
  c6->Print(setname + "-6-corr-vs-sipm.pdf");
  
  return rp;
  
  */
  
  return RunsPlots();
}

void make_plots_ts_ratio(const vector<Measurement>& meas, const RunsSet& rset)
{
  cout << "make_plots_ts_ratio(): rset = " << rset.title << ", #runs = " << rset.runs.size()
       << ", #meas = " << meas.size() << ", #channels = " <<  meas[0].totalChannels()
       << endl;
  
  const TString setname = rset.name();
  
  gStyle->SetLineScalePS(1);
  
  // QIE11
  // ratio ts4 / ts3
  TMultiGraph* mg43 = new TMultiGraph;
  mg43->SetTitle(setname + " / ratio TS4/TS3;Total charge, fC;Ratio");
  
  for (size_t j = 0; j < meas[0].SiPM.size(); j++) {
    const int n = meas.size();
    double x[n];
    double y[n];
    double ex[n];
    double ey[n];
    
    for (size_t i = 0; i < meas.size(); i++) {
      const ChannelSignal& c = meas[i].SiPM[j];
      const ErrValue ratio = c.ts[4] / c.ts[3];
      x[i] = c.signal();
      y[i] = ratio.x;
      ex[i] = c.signal_err();
      ey[i] = ratio.err;
    }
    
    TGraphErrors* g = new TGraphErrors(n, x, y, ex, ey);
    g->SetTitle(meas[0].SiPM[j].id);
    g->SetMarkerStyle(20+j);
    g->SetFillColor(0);
    const int color = kBlack;
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg43->Add(g);
  }
  
  TCanvas* c1 = new TCanvas(setname+"-1-ratio43", setname+"-1-ratio43");
  c1->SetGrid();
  c1->SetLogx();
  mg43->SetMinimum(0);
  mg43->DrawClone("APL");
  c1->BuildLegend(0.15, 0.88 - 0.02*mg43->GetListOfGraphs()->GetSize()/4, 0.35, 0.88)->SetNColumns(4);
  c1->Print(".pdf");
  
  //print_multi_graph(mg43);
  
  // ratio ts5 / ts3
  TMultiGraph* mg53 = new TMultiGraph;
  mg53->SetTitle(setname + " / ratio TS5/TS3;Total charge, fC;Ratio");
  
  for (size_t j = 0; j < meas[0].SiPM.size(); j++) {
    const int n = meas.size();
    double x[n];
    double y[n];
    double ex[n];
    double ey[n];
    
    for (size_t i = 0; i < meas.size(); i++) {
      const ChannelSignal& c = meas[i].SiPM[j];
      const ErrValue ratio = c.ts[5] / c.ts[3];
      x[i] = c.signal();
      y[i] = ratio.x;
      ex[i] = c.signal_err();
      ey[i] = ratio.err;
    }
    
    TGraphErrors* g = new TGraphErrors(n, x, y, ex, ey);
    g->SetTitle(meas[0].SiPM[j].id);
    g->SetMarkerStyle(20+j);
    g->SetFillColor(0);
    const int color = kBlack;
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg53->Add(g);
  }
  
  TCanvas* c2 = new TCanvas(setname+"-2-ratio53", setname+"-2-ratio53");
  c2->SetGrid();
  c2->SetLogx();
  mg53->SetMinimum(0);
  mg53->DrawClone("APL");
  c2->BuildLegend(0.15, 0.88 - 0.02*mg53->GetListOfGraphs()->GetSize()/4, 0.35, 0.88)->SetNColumns(4);
  c2->Print(".pdf");
  
  
  // QIE8
  // ratio ts5 / ts4
  TMultiGraph* mg54 = new TMultiGraph;
  mg54->SetTitle(setname + " / HPD-QIE8 ratio TS5/TS4;Total charge, fC;Ratio");
  
  for (size_t j = 0; j < meas[0].HPD.size(); j++) {
    const int n = meas.size();
    double x[n];
    double y[n];
    double ex[n];
    double ey[n];
    
    for (size_t i = 0; i < meas.size(); i++) {
      const ChannelSignal& c = meas[i].HPD[j];
      const ErrValue ratio = c.ts[5] / c.ts[4];
      x[i] = c.signal();
      y[i] = ratio.x;
      ex[i] = c.signal_err();
      ey[i] = ratio.err;
      
      if (meas[i].fname == "dump-288188.root") {
        cout << meas[i].fname << " " << c.id << " TS4=" << c.ts[4].x << " TS5=" << c.ts[5].x << " TS6=" << c.ts[6].x << endl;
      }
    }
    
    TGraphErrors* g = new TGraphErrors(n, x, y, ex, ey);
    g->SetTitle(meas[0].HPD[j].id);
    g->SetMarkerStyle(20+j);
    g->SetFillColor(0);
    const int color = kBlack;
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg54->Add(g);
  }
  
  TCanvas* c3 = new TCanvas(setname+"-3-qie8ratio54", setname+"-3-qie8ratio54");
  c3->SetGrid();
  c3->SetLogx();
  mg54->SetMinimum(0);
  mg54->DrawClone("APL");
  c3->BuildLegend(0.15, 0.88 - 0.03*mg54->GetListOfGraphs()->GetSize()/4, 0.35, 0.88)->SetNColumns(4);
  c3->Print(".pdf");
  
  //print_multi_graph(mg54);

  
  // ratio ts6 / ts4
  TMultiGraph* mg64 = new TMultiGraph;
  mg64->SetTitle(setname + " / HPD-QIE8 ratio TS6/TS4;Total charge, fC;Ratio");
  
  for (size_t j = 0; j < meas[0].HPD.size(); j++) {
    const int n = meas.size();
    double x[n];
    double y[n];
    double ex[n];
    double ey[n];
    
    for (size_t i = 0; i < meas.size(); i++) {
      const ChannelSignal& c = meas[i].HPD[j];
      const ErrValue ratio = c.ts[6] / c.ts[4];
      x[i] = c.signal();
      y[i] = ratio.x;
      ex[i] = c.signal_err();
      ey[i] = ratio.err;
    }
    
    TGraphErrors* g = new TGraphErrors(n, x, y, ex, ey);
    g->SetTitle(meas[0].HPD[j].id);
    g->SetMarkerStyle(20+j);
    g->SetFillColor(0);
    const int color = kBlack;
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg64->Add(g);
  }
  
  TCanvas* c4 = new TCanvas(setname+"-4-qie8ratio64", setname+"-4-qie8ratio64");
  c4->SetGrid();
  c4->SetLogx();
  mg64->SetMinimum(0);
  mg64->DrawClone("APL");
  c4->BuildLegend(0.15, 0.88 - 0.03*mg64->GetListOfGraphs()->GetSize()/4, 0.35, 0.88)->SetNColumns(4);
  c4->Print(".pdf");
}

/*
SiPM LED and PED values defined as sum of all (ten) time samples of the signal shape, read from QIE11
APD values are read from QDC channel #5

apd-vs-sipm.pdf:

  plot SiPM charge (mean LED - mean PED) vs APD qadc counts (gaus fit mean of LED - gaus fit mean of PED)

apd-vs-sipm-pixels.pdf:

  SiPM charge is converted to number of pixels fired
  the conversion factor is extracted manually from SiPM LED distributions of run 2517 (low light) - 2517-led-histo.png
  the value of conversion factor is 52.5 fC / pixel (same for all channels)

apd-vs-sipm-pixels-fit3point(-zoom).pdf:

  first 3 points (low light) of each SiPM channel are expected to be linear versus APD counts and are fit with function "k*x", the slope gives APD qadc counts to p.e. conversion for each channel

apd-vs-sipm-pixels-apdscale(-zoom).pdf

   APD counts for each SiPM channel converted to photo electrons based on slope from fit
*/

#include "datasets.h"

const RunsSet all_sets[] = {
//  runs_initial_test,

/*
  runs_1fib_nomix,
  runs_4fib_nomix,
  runs_1fib_1mm_mix,
  runs_4fib_1mm_mix,
  runs_1fib_16mm_mix,
  runs_4fib_16mm_mix,
*/

//  runs_lin_1fib
//  runs_lin_4fib
//  runs_lin_1fib_filt10x

//  runs_lin_1fib_filtNx
};

vector<Measurement> read_set(const RunsSet& rset)
{
  vector<Measurement> meas;
  
  // disable creations of canvas windows
  const Bool_t isbatch = gROOT->IsBatch();
  gROOT->SetBatch(kTRUE);
  
  // read runs
  for (const RunId& r : rset.runs) {
    meas.push_back(read_data(r.fname(), r.shunt));
  }
  
  gROOT->SetBatch(isbatch);
  
  // TODO: ??? only P5
  set_pedestal_run(meas, "dump-288086.root");
  
  save_meas_data(meas, rset.cache());
  
  return meas;
}

vector<Measurement> read_set(const int start, const int stop, const double shunt = 1.0)
{
  RunsSet rset = {"data", {}, {}};
  
  for (int i = start; i <= stop; ++i)
    rset.runs.push_back({i, shunt});
  
  return read_set(rset);
}

vector<Measurement> read_set_with_cache(const RunsSet& rset)
{
  vector<Measurement> meas;
  
  // try to load cache
  load_meas_data(meas, rset.cache());
  
  const int ncache = meas.size();
  
  // disable creations of canvas windows
  const Bool_t isbatch = gROOT->IsBatch();
  gROOT->SetBatch(kTRUE);
  
  // read runs
  for (const RunId& r : rset.runs) {
    
    bool incache = false;
    for (const Measurement& m : meas) {
      if (m.fname == r.fname()) {
        incache = true;
        break;
      }
    }
    
    // skip the cached data
    if (incache) {
      cout << "INFO: cached data, skip reading file " << r.fname() << endl;
      continue;
    }
    
    meas.push_back(read_data(r.fname(), r.shunt));
    
    save_meas_data(meas, rset.cache());
  }
  
  gROOT->SetBatch(isbatch);
  
  // TODO: ??? only P5
  set_pedestal_run(meas, "dump-288086.root");
  
  return meas;
}


RunsResults analysis_linear(const RunsSet& rset)
{
  vector<Measurement> meas = read_set_with_cache(rset);
  
  //select_channels(meas, select_channels(rset.channels, {"SiPM-rm3-ch13", "SiPM-rm3-ch53", "HPD-rm1-ch0"}));
  select_channels(meas, rset.channels);
  correct_qie11_shunt(meas, rset.runs);
  //add_hpd_average(meas);
  
  const RunsPlots plots = make_plots(meas, rset);
  return {rset, meas, plots};
}

RunsResults analysis_di_i_dv(const RunsSet& rset, const vector<double>& volt)
{
  vector<Measurement> meas = read_set_with_cache(rset);
  
  //select_channels(meas, rset.channels);
  //correct_qie11_shunt(meas, rset.runs);
  //add_hpd_average(meas);
  
  const RunsPlots plots = make_plots_di_i_dv(meas, rset, volt);
  return {rset, meas, plots};
}

void analysis_ts_ratio(const RunsSet& rset)
{
  vector<Measurement> meas = read_set_with_cache(rset);
  
  select_channels(meas, rset.channels);
  correct_qie11_shunt(meas, rset.runs);
  //add_hpd_average(meas);
  
  make_plots_ts_ratio(meas, rset);
}


void print_multi_graph(TMultiGraph* mg)
{
  cout << "TMultiGraph title = " << mg->GetTitle() << endl;
  
  TGraph* g;
  TIter next(mg->GetListOfGraphs());
  while ((g = (TGraph*) next())) {
    cout << " TGraph title = " << g->GetTitle() << endl;
    
    const int n = g->GetN();
    const double* x = g->GetX();
    const double* y = g->GetY();
    
    cout << "  x = ";
    for (int i = 0; i < n; ++i) cout << x[i] << " ";
    cout << endl;
    
    cout << "  y = ";
    for (int i = 0; i < n; ++i) cout << y[i] << " ";
    cout << endl;
  }
}

/*
void process_all()
{
  vector<RunsResults> results;
  
  const Bool_t isbatch = gROOT->IsBatch();
  gROOT->SetBatch(kTRUE);
  
  for (const RunsSet& rs : all_sets) {
    results.push_back(analysis_linear(rs));
  }
  
  gROOT->SetBatch(isbatch);
  
  gStyle->SetGridColor(kGray);
  gStyle->SetGridStyle(7);
  gStyle->SetGridWidth(1);
  
  TCanvas* c = new TCanvas;
  c->SetGrid();
  c->Print("all.pdf[");
  
  // unity line APD 1 p.e. = SiPM 1 pixel
  TF1* funi = new TF1("funi", "x", 0, 150000);
  funi->SetLineColor(kBlack);
  funi->SetLineStyle(2);
  funi->SetLineWidth(1);
  funi->SetTitle("unity");
  
  const int Nplots = 6;
  const PlotInfo plotid[Nplots] = {
    {"RAW", "SiPM vs APD;APD qadc counts;SiPM charge, fC", 0, 10, 0, 15000},
    {"SIPM_PIXELS", "SiPM vs APD;APD qadc counts;SiPM pixels", 0, 250, 0, 10000},
    {"LY", "Light yield efficiency;APD qadc counts;Light yield efficiency, %", 0, 50, 50, 110},
    {"APD_PE", "SiPM vs APD;APD effective p.e.;SiPM pixels", 0, 30000, 0, 30000},
    {"CORR_APD", "Correction factor;APD effective p.e.;Correction factor", 0, 30000, 0.5, 2.},
    {"CORR_SIPM", "Correction factor;SiPM pixels;Correction factor", 0, 30000, 0.5, 2.},
  };
  
  // indices of baseline datasets for LY calculation
  const int lybaseline[6] = {0, 1, 0, 1, 0, 1};
  
  for (int k = 0; k < Nplots; ++k) {
    TMultiGraph* mgall = new TMultiGraph;
    mgall->SetTitle(plotid[k].id + " / ALL / " + plotid[k].name);
    
    TMultiGraph mg[N_channels];
    for (int i = 0; i < N_channels; ++i)
      mg[i].SetTitle(plotid[k].id + " / " + selchan[i].name() + " / " + plotid[k].name);
    
    const int korig = (k > 1) ? k - 1 : k;
    
    for (int j = 0; j < results.size(); ++j) {
      const RunsResults& rr = results[j];
      TList* list = rr.plots.plots[korig]->GetListOfGraphs();
      const TString rtitle = rr.runs.title;
      
      // skip 1.6 mm sets
      //if (j > 3) continue;
      
      for (int i = 0; i < list->GetEntries(); ++i) {
        TGraph* g = (TGraph*) list->At(i)->Clone();
        
        if (k == 2) {
          TGraph* gref = (TGraph*) results[lybaseline[j]].plots.plots[korig]->GetListOfGraphs()->At(i)->Clone();
          
          // normalise to baseline (nomix)
          const int n = g->GetN();
          double* x = g->GetX();
          double* y = g->GetY();
          for (int idx = 0; idx < n; ++idx)
            y[idx] = 100. * y[idx] / gref->Eval(x[idx]);
        }
        
        //g->SetMarkerStyle(20+j);
        //g->SetFillColor(0);
        g->SetMarkerColor(1+j);
        g->SetLineColor(1+j);
        g->SetTitle(rtitle + " / " + g->GetTitle());
        mgall->Add(g);
        
        g = (TGraph*) g->Clone();
        g->SetTitle(rtitle);
        mg[i].Add(g);
      }
    }
    
    // common plot
    print_multi_graph(mgall);
    
    c->Clear();
    TMultiGraph* mgscale = (TMultiGraph*) mgall->DrawClone("APL");
    if (k == 3) funi->DrawCopy("L SAME");
    
    c->BuildLegend(0.1, 0.4, 0.28, 0.88);
    
    if (k == 4 || k == 5) {
      mgscale->GetHistogram()->SetMinimum(0.9);
      mgscale->GetHistogram()->SetMaximum(2.);
      mgscale->Draw("P");
    }
    
    c->Print("all.pdf", "Title:" + plotid[k].id + " / ALL");
    
    // zoom plot
    mgscale->GetHistogram()->GetXaxis()->SetLimits(plotid[k].Xmin, plotid[k].Xmax);
    mgscale->GetHistogram()->SetMinimum(plotid[k].Ymin);
    mgscale->GetHistogram()->SetMaximum(plotid[k].Ymax);
    mgscale->Draw("P");
    c->Print("all.pdf", "Title:" + plotid[k].id + " / ALL / ZOOM");
    
    // separate channels plots
    for (int i = 0; i < N_channels; ++i) {
      print_multi_graph(&(mg[i]));
      
      c->Clear();
      mgscale = (TMultiGraph*) mg[i].DrawClone("APL");
      if (k == 3) funi->DrawCopy("L SAME");
      c->BuildLegend(0.15, 0.6, 0.3, 0.88);
      
      if (k == 4 || k == 5) {
        mgscale->GetHistogram()->SetMinimum(0.9);
        mgscale->GetHistogram()->SetMaximum(2.);
        mgscale->Draw("P");
      }
    
      c->Print("all.pdf", "Title:" + selchan[i].name());
      
      // zoom plot
      mgscale->GetHistogram()->GetXaxis()->SetLimits(plotid[k].Xmin, plotid[k].Xmax);
      mgscale->GetHistogram()->SetMinimum(plotid[k].Ymin);
      mgscale->GetHistogram()->SetMaximum(plotid[k].Ymax);
      mgscale->Draw("P");
      c->Print("all.pdf", "Title:" + selchan[i].name() + " / ZOOM");
    }
  }
  
  c->Clear();
  c->Print("all.pdf");
  
  c->Print("all.pdf]");
}
*/

void lab28()
{
  // read single file
  //read_data("dump-002672.root", 1.);
  
  // read single runset
  //read_set(runs20161012_sipm_hpd_apd);
  
  // analyse single runset
  //analysis_linear(runs20161012_sipm_hpd_apd);
  //analysis_linear(runs20161128_sipm_hpd);
  //analysis_linear(runs20161129_sipm1_hpd);
  
  //analysis_linear(runs20161207_shunt1);
  //analysis_linear(runs20161207_shunt5);
  //analysis_linear(runs20161207_shunt11);
  
  //analysis_linear(runs20170602_noy11);
  
  // P5 data
  //analysis_linear(runs20170220_p5laser);
  analysis_ts_ratio(runs20170220_p5laser);
  
  //analysis_ts_ratio(runs20161207_shunt1);
  //analysis_ts_ratio(runs20161207_shunt5);
  //analysis_ts_ratio(runs20161207_shunt11);
  //analysis_ts_ratio(runs20170602_noy11);
  
  //analysis_linear(runs20170627);
  //analysis_ts_ratio(runs20170627);
  
  //analysis_di_i_dv(runs20161026_bvscan, runs20161026_voltages);
  
  // read all sets
  //process_all();
}
