#include "TMath.h"
#include "TH2.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TPaveText.h"

#include <fstream>
#include <iostream>
using namespace std;


// usage:
// $ root -b -q -l ../mipfit.cxx

void zoom_to_ycontents(TH2* h, const bool isLogY = false)
{
  const int f = h->FindFirstBinAbove(0, 2);
  const int l = h->FindLastBinAbove(0, 2);
  TAxis* a = h->GetYaxis();
  a->SetRange(f-(isLogY ? 2 : 10), l+10);
}

int getbinnum(TAxis* a, const TString name)
{
  for (int i = 1; i <= a->GetNbins(); ++i) {
    const TString label = a->GetBinLabel(i);
    if (label == name) return i;
  }
  
  return -1;
}

// convolution function: landau (x) gauss
// origin: https://root.cern.ch/root/html/tutorials/fit/langaus.C.html
Double_t langaufun(Double_t* x, Double_t* par)
{
   //Fit parameters:
   //par[0]=Width (scale) parameter of Landau density
   //par[1]=Most Probable (MP, location) parameter of Landau density
   //par[2]=Total area (integral -inf to inf, normalization constant)
   //par[3]=Width (sigma) of convoluted Gaussian function
   //
   //In the Landau distribution (represented by the CERNLIB approximation), 
   //the maximum is located at x=-0.22278298 with the location parameter=0.
   //This shift is corrected within this function, so that the actual
   //maximum is identical to the MP parameter.

      // Numeric constants
      Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
      Double_t mpshift  = -0.22278298;       // Landau maximum location

      // Control constants
      Double_t np = 100.0;      // number of convolution steps
      Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

      // Variables
      Double_t xx;
      Double_t mpc;
      Double_t fland;
      Double_t sum = 0.0;
      Double_t xlow,xupp;
      Double_t step;
      Double_t i;


      // MP shift correction
      mpc = par[1] - mpshift * par[0]; 

      // Range of convolution integral
      xlow = x[0] - sc * par[3];
      xupp = x[0] + sc * par[3];

      step = (xupp-xlow) / np;

      // Convolution integral of Landau and Gaussian by sum
      for(i=1.0; i<=np/2; i++) {
         xx = xlow + (i-.5) * step;
         fland = TMath::Landau(xx,mpc,par[0]) / par[0];
         sum += fland * TMath::Gaus(x[0],xx,par[3]);

         xx = xupp - (i-.5) * step;
         fland = TMath::Landau(xx,mpc,par[0]) / par[0];
         sum += fland * TMath::Gaus(x[0],xx,par[3]);
      }

      return (par[2] * step * sum * invsq2pi / par[3]);
}


struct FileChannelList {
  TString fname;
  vector<TString> channels;
  
  TString run() const
  {
    return "run" + fname(fname.First(".")-4, 4);
  }
};

typedef vector<FileChannelList> ChannelList;

const ChannelList miplist = {
  /*
  // SiPM --->
  // Round A: Muon eta-phi scan to verify mapping -- Phi 5
  {"histo-dump-003030.root", {"5-16-2", "5-16-3"}},
  {"histo-dump-003032.root", {"5-17-2", "5-17-3", "5-17-4", "5-17-5"}},
  {"histo-dump-003040.root", {"5-18-2", "5-18-3", "5-18-4", "5-18-5", "5-18-6"}},
  {"histo-dump-003047.root", {"5-19-2", "5-19-3", "5-19-4", "5-19-5", "5-19-6"}},
  {"histo-dump-003055.root", {"5-20-2", "5-20-3", "5-20-4", "5-20-5", "5-20-6"}},
  {"histo-dump-003057.root", {"5-21-2", "5-21-3", "5-21-4", "5-21-5", "5-21-6"}},
  
  // eta22 megatile covers physically phi=5 and phi=6 and in readout have phi6-eta22
  {"histo-dump-003058.root", {"6-22-2", "6-22-3", "6-22-4", "6-22-5", "6-22-6"}},
  
  {"histo-dump-003059.root", {"5-23-2", "5-23-3", "5-23-4", "5-23-5", "5-23-6", "5-23-7"}},
  {"histo-dump-003060.root", {"5-25-2", "5-25-3", "5-25-4", "5-25-5", "5-25-6", "5-25-7"}},
  
  {"histo-dump-003228.root", {"5-18-2", "5-18-3", "5-18-4", "5-18-5", "5-18-6"}},
  {"histo-dump-003226.root", {"5-19-2", "5-19-3", "5-19-4", "5-19-5", "5-19-6"}},
  {"histo-dump-003224.root", {"5-20-2", "5-20-3", "5-20-4", "5-20-5", "5-20-6"}},
  
  // Round B: Muons for MIP calibration
  {"histo-dump-003234.root", {"6-18-2", "6-18-3", "6-18-4", "6-18-5", "6-18-6"}},
  {"histo-dump-003232.root", {"6-19-2", "6-19-3", "6-19-4", "6-19-5", "6-19-6"}},
  {"histo-dump-003230.root", {"6-20-2", "6-20-3", "6-20-4", "6-20-5", "6-20-6"}},
  
  // HPD --->
  {"histo-dump-003222.root", {}},
  {"histo-dump-003220.root", {}},
  {"histo-dump-003217.root", {}},
  
  {"histo-dump-003380.root", {}},
  {"histo-dump-003382.root", {}},
  {"histo-dump-003384.root", {}},
  */
  
  // NDF runs
  {"histo-dump-003427.root", {"5-19-2", "5-19-3", "5-19-4", "5-19-5", "5-19-6"}},
  
};

void mipfit()
{
  gStyle->SetOptStat("emruo");
  //gStyle->SetOptFit(111);
  gStyle->SetLineScalePS(1);
  gErrorIgnoreLevel = kWarning;
  
  ofstream fout("mipfit.csv");
  fout << "Run,"
       << "phi-eta-depth,"
       << "nevents,"
       << "langaus_chi2norm,"
       << "langaus_mpv,"
       << "langaus_mpv_error,"
       << "langaus_noise_sigma,"
       << "langaus_noise_sigma_error,"
       << endl;
  
  TCanvas* c = new TCanvas;
  c->Print("mipfit.pdf[");
  
    // Once again, here are the Landau * Gaussian parameters:
    //   par[0]=Width (scale) parameter of Landau density
    //   par[1]=Most Probable (MP, location) parameter of Landau density
    //   par[2]=Total area (integral -inf to inf, normalization constant)
    //   par[3]=Width (sigma) of convoluted Gaussian function
    //
    // Variables for langaufit call:
    //   fitrange[2]     lo and hi boundaries of fit range
    //   startvalues[4]  reasonable start values for the fit

    TF1* langaus = new TF1("langaus", langaufun, 0, 1000, 4);
    langaus->SetParameters(200, 1000, 50000, 200);
    langaus->SetParNames("Width", "MPV", "Area", "GSigma");
    langaus->SetNpx(500);
    langaus->SetLineColor(kOrange);
  
  
  for (const FileChannelList i : miplist) {
    cout << "fname = " << i.fname << endl;
    
    TFile* f = new TFile(i.fname);
    TH2I* h = (TH2I*) f->Get("2signal");
    h->SetTitle(i.run());
    
    c->SetLogx(kFALSE);
    c->SetLogy();
    c->SetLogz();
    c->SetGridx();
    h->Draw();
    zoom_to_ycontents(h, true);
    c->Print("mipfit.pdf", "Title:"+i.run());
    
    h->GetYaxis()->UnZoom();
    h->RebinY(4);
    zoom_to_ycontents(h, true);
    
    //c->SetLogz(kFALSE);
    c->SetLogx(kTRUE);
    c->SetLogy(kFALSE);
    //c->SetGridx(kFALSE);
    
    for (const TString j : i.channels) {
      cout << "channel = " << j << endl;
      
      int idx = getbinnum(h->GetXaxis(), j);
      
      //cout << "label = " << h->GetXaxis()->GetBinLabel(idx)
      //     << endl;
      
      if (idx < 0) continue;
      
      TH1D* pr = h->ProjectionY("", idx, idx, "e");
      pr->SetTitle(i.run() + " " + j);
      
      // gaus fit
      //TFitResultPtr fit = pr->Fit("gaus", "SNQ");
      //TFitResultPtr fit = pr->Fit("gaus", "S0Q");
      TFitResultPtr fit = pr->Fit("gaus", "SQM");
      fit->Print();
      /*
      cout << "channel " << j
           << " fit status=" << (int)fit
           << " mean=" << fit->Parameter(1)
           << " mean_err=" << fit->ParError(1)
           << " sigma=" << fit->Parameter(2)
           << " sigma_err=" << fit->ParError(2)
           << " chi2=" << fit->Chi2()
           << " ndof=" << fit->Ndf()
           << endl;
      */
      
      TF1* gaus = pr->GetFunction("gaus");
      gaus->SetNpx(500);
      gaus->SetLineColor(kRed);
      //pr->Draw();
      
      // landau fit
      TFitResultPtr fit2 = pr->Fit("landau", "SQM+");
      fit2->Print();
      TF1* landau = pr->GetFunction("landau");
      landau->SetNpx(500);
      landau->SetLineColor(kBlue);
      
      
      // convolution landau (x) gaus
      langaus->SetParameters(200, 1000, 50000, 200);
      TFitResultPtr fit3 = pr->Fit(langaus, "SQM+");
      fit3->Print();
      
      TPaveText* pt = new TPaveText(0.75, 0.4, 0.95, 0.7, "NDC");
      pt->SetFillColor(0);
      pt->SetTextAlign(12);
      pt->SetBorderSize(1);
      pt->AddText("#color[2]{gaus}");
      pt->AddText(Form("#chi^{2}_{} / NDF = %.1f / %d", fit->Chi2(), fit->Ndf()));
      pt->AddText(Form("%s = %.1f #pm %.2f", fit->ParName(1).c_str(), fit->Parameter(1), fit->ParError(1)));
      pt->AddText(Form("%s = %.1f #pm %.2f", fit->ParName(2).c_str(), fit->Parameter(2), fit->ParError(2)));
      pt->AddLine(.0, .333, 1., .333);
      pt->AddText("#color[4]{landau}");
      pt->AddText(Form("#chi^{2} / NDF = %.1f / %d", fit2->Chi2(), fit2->Ndf()));
      pt->AddText(Form("%s = %.1f #pm %.2f", fit2->ParName(1).c_str(), fit2->Parameter(1), fit2->ParError(1)));
      pt->AddText(Form("%s = %.1f #pm %.2f", fit2->ParName(2).c_str(), fit2->Parameter(2), fit2->ParError(2)));
      pt->AddLine(.0, .666, 1., .666);
      pt->AddText("#color[800]{langaus}");
      pt->AddText(Form("#chi^{2} / NDF = %.1f / %d", fit3->Chi2(), fit3->Ndf()));
      pt->AddText(Form("%s = %.1f #pm %.2f", fit3->ParName(1).c_str(), fit3->Parameter(1), fit3->ParError(1)));
      pt->AddText(Form("%s = %.1f #pm %.2f", fit3->ParName(3).c_str(), fit3->Parameter(3), fit3->ParError(3)));
      
      
      c->Clear();
      c->SetLogx(kTRUE);
      pr->Draw();
      pt->Draw();
      c->Print("mipfit.pdf", "Title:" + i.run() + " " + j);
      
      c->Clear();
      c->SetLogx(kFALSE);
      //pr->SetAxisRange(pr->GetXaxis()->GetXmin(), pr->GetMean() + pr->GetStdDev(), "X");
      pr->SetAxisRange(pr->GetXaxis()->GetXmin(), 3000., "X");
      pr->Draw();
      pt->Draw();
      c->Print("mipfit.pdf", "Title:" + i.run() + " " + j + " (linearX)");
      
      fout << i.run() << ","
           << j << ","
           << pr->GetEntries() << ","
           << (fit3->Chi2() / fit3->Ndf()) << ","
           << fit3->Parameter(1) << ","
           << fit3->ParError(1) << ","
           << fit3->Parameter(3) << ","
           << fit3->ParError(3) << ","
           << endl;
    }
    
    delete f;
  }
  
  c->Clear();
  c->Print("mipfit.pdf", "Title:End");
  
  c->Print("mipfit.pdf]");
  delete c;
}

void lin()
{
  //gStyle->SetOptStat("emruo");
  gStyle->SetOptStat("");
  //gStyle->SetOptFit(111);
  gStyle->SetLineScalePS(1);
  gErrorIgnoreLevel = kWarning;
  
  TCanvas* c = new TCanvas;
  //c->SetLogx();
  c->SetLogy();
  c->SetGrid();
  
  // mip calc
  //run3226,5-19-2,569.882
  //run3226,5-19-3,492.153
  //run3226,5-19-4,550.321
  //run3226,5-19-5,874.605
  
  const TString ch = "5-19-5";
  const double MIP = 874.605;
  const bool hasNDF = false;
  const TString run0 = "3353";
  const TString runNDF = "3429";
  const double ndfMIP = hasNDF ? (MIP / 10.) : MIP;
  
  TH1D* h0 = 0;
  TH1D* hNDF = 0;
  
  // normal run
  {
  TFile* f = new TFile("histo-dump-00" + run0 + ".root");
  TH2I* h = (TH2I*) f->Get("2signal")->Clone("sig" + run0);
  h->SetTitle(run0);
  h->RebinY(10);
  
  int idx = getbinnum(h->GetXaxis(), ch);
  TH1D* pr = h->ProjectionY(run0 + " " + ch, idx, idx, "e");
  pr->SetDirectory(0);
  pr->SetTitle(run0 + " " + ch);
  cout << pr->GetTitle() << " nevents = " << pr->GetEntries() << " integral = " << pr->Integral() << endl;
  
  pr->RebinX(50);
  pr->Scale(1/pr->Integral());
  TAxis* xa = pr->GetXaxis();
  xa->Set(xa->GetNbins(), xa->GetXmin(), xa->GetXmax() / MIP);
  xa->SetTitle("MIP");
  //xa->SetRangeUser(0., 500.);
  pr->Draw();
  h0 = pr;
  }
  
  // NDF run
  {
  TFile* f = new TFile("histo-dump-00" + runNDF + ".root");
  TH2I* h = (TH2I*) f->Get("2signal")->Clone("sig" + runNDF);
  h->SetTitle(runNDF);
  if (!hasNDF) h->RebinY(10);
  
  int idx = getbinnum(h->GetXaxis(), ch);
  TH1D* pr = h->ProjectionY(runNDF + " " + ch + " NDF", idx, idx, "e");
  pr->SetDirectory(0);
  pr->SetTitle(runNDF + " " + ch + " NDF");
  pr->SetLineColor(kRed);
  cout << pr->GetTitle() << " nevents = " << pr->GetEntries() << " integral = " << pr->Integral() << endl;
  
  pr->RebinX(50);
  pr->Scale(1/pr->Integral());
  TAxis* xa = pr->GetXaxis();
  xa->Set(xa->GetNbins(), xa->GetXmin(), xa->GetXmax() / ndfMIP);
  pr->Draw("SAME");
  hNDF = pr;
  }
  
  c->BuildLegend();
  
  // ration plot
  TCanvas* c1 = new TCanvas;
  c1->SetGrid();
  
  cout << "Ratio plot:\n"
       << "normal " << h0->GetXaxis()->GetNbins() << " " << h0->GetXaxis()->GetXmin() << " " << h0->GetXaxis()->GetXmax() << "\n"
       << "ndf " << hNDF->GetXaxis()->GetNbins() << " " << hNDF->GetXaxis()->GetXmin() << " " << hNDF->GetXaxis()->GetXmax()
       << endl;
  
  TH1D* hR = (TH1D*) h0->Clone("ratio");
  hR->SetTitle(TString("Ratio ") + h0->GetTitle() + " / " + hNDF->GetTitle() + ";MIP;ratio");
  hR->Divide(hNDF);
  hR->Draw();
}
