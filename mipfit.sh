#!/bin/bash

miplist="$@"

for i in $miplist ; do
  f=histo-dump-00$i.root
  if [[ ! -e $f ]] ; then
    echo dump-00$i.root
  fi
done | \
while read f ; do
  echo $f
  root -b -l <<EOF
.L ../lab28.cxx
read_data_tb2017("$f")
.q
EOF
done
