//#include <vector>
//#include <string>

#define NUMCHS 10000
#define NUMTS 10

struct TCalibLedInfo
{
    int numChs;
    int iphi[NUMCHS];
    int ieta[NUMCHS];
    int cBoxChannel[NUMCHS];
    int nTS[NUMCHS];
    double pulse[NUMCHS][NUMTS];
};

struct TQIE8Info
{
    int numChs;
    int numTS;
    int iphi[NUMCHS];
    int ieta[NUMCHS];
    int depth[NUMCHS];
    float pulse[NUMCHS][NUMTS];
    unsigned char pulse_adc[NUMCHS][NUMTS];
    bool valid[NUMCHS];
    
    
    // QIE8 event corruption check
    // http://cmsonline.cern.ch/cms-elog/968085
    bool isIdleStream() const {
      for (int j = 0; j < numChs; j++) {
        int n40 = 0;
        int n94 = 0;
        
        for (int k = 0; k < 10; k++) {
          const unsigned char adc = pulse_adc[j][k];
          if (adc == 40) n40++;
          if (adc == 94) n94++;
        }
        
        const bool isIdle = (n40 > 3) || (n94 > 3);
        
        if (isIdle) {
          std::cout << "INFO: QIE8 corrupted event | adc = ";
          for (int k = 0; k < 10; k++) std::cout << (int)pulse_adc[j][k] << " ";
          std::cout << std::endl;
          return true;
        }
      }
      
      return false;
    }
};

struct TQIE11Info
{
    int numChs;
    int numTS;
    int iphi[NUMCHS];
    int ieta[NUMCHS];
    int depth[NUMCHS];
    float pulse[NUMCHS][NUMTS];
    unsigned char pulse_adc[NUMCHS][NUMTS];
    bool capid_error[NUMCHS];
    bool link_error[NUMCHS];
    bool soi[NUMCHS][NUMTS];
    
    
    // QIE11 event corruption check
    // http://cmsonline.cern.ch/cms-elog/999277
    bool isBad() const {
      int nBad = 0;
      
      for (int j = 0; j < numChs; j++) {
        int n124 = 0;
        int n188 = 0;
        
        for (int k = 0; k < 10; k++) {
          const unsigned char adc = pulse_adc[j][k];
          if (adc == 124) n124++;
          if (adc == 188) n188++;
        }
        
        const bool isBad11 = (n124 > 3) || (n188 > 3);
        
        if ((n124 > 0) || (n188 > 0)) nBad++;
        
        if (isBad11) {
          std::cout << "INFO: QIE11 corrupted event | adc = ";
          for (int k = 0; k < 10; k++) std::cout << (int)pulse_adc[j][k] << " ";
          std::cout << std::endl;
          return true;
        }
      }
      
      // skip corrupted event
      if (nBad == numChs) {
        std::cout << "INFO: QIE11 corrupted event | nBad == numChs" << std::endl;
        return true;
      }
      
      return false;
    }
    
    void print(int j) const
    {
      std::cout << "ch#" << j
           << " iphi=" << iphi[j]
           << " ieta=" << ieta[j]
           << " depth=" << depth[j]
           << " | ADC: ";
      
      for (int k = 0; k < 10; k++) std::cout << (int)pulse_adc[j][k] << " ";
      
      std::cout << " | pulse/shunt (fC): ";
      
      for (int k = 0; k < 10; k++) std::cout << pulse[j][k] << " ";
      
      std::cout << std::endl;
    }
};

struct H2Triggers
{
    //  Standard Triggers
    int ped;
    int calib;
    int led;
    int laser;
    int beam;

    //  Added for completeness
    int fakeTrg;
    int inSpillTrg;
};

struct H2BeamCounters
{
    double cer1adc;
    double cer2adc;
    double cer3adc;
    double s1adc;
    double s2adc;
    double s3adc;
    double s4adc;
    double QADC5;
};

struct H2Timing
{
    int s1Count;
    int s2Count;
    int s3Count;
    int s4Count;

    double triggerTime;
    double ttcL1Atime;
};
