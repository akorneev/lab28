#include "TProfile2D.h"
#include "TH2.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TFitResult.h"
#include "TChain.h"
#include <iostream> 

void zoom_to_xcontents(TH1* h)
{
  const int f = h->FindFirstBinAbove(0);
  const int l = h->FindLastBinAbove(0);
  TAxis* a = h->GetXaxis();
  a->SetRange(f-10, l+10);
}

void read_dark_data(const vector<TString> fname, const int nTS)
{
  gStyle->SetOptStat("nemruo");
  TH1::AddDirectory(kFALSE);
  TH1::SetDefaultSumw2(kTRUE);
  
  const int fibermin = 2;
  const int fibermax = 9;
  //cout << "fiber range: " << fibermin << " - " << fibermax << endl;
  
  // book histograms
  TString label;
  TH2F* spectr[2];
  
  label.Form("spectra VS fiber led=%d", 0);
  spectr[0] = new TH2F(label, label + ";10*fiber+channel;signal", 10*(fibermax-fibermin+1),fibermin*10,(fibermax+1)*10, 10000,0,35000);
  
  // open data files
  TChain* Events = new TChain("QIE11Data/Events");
  TChain* treeTriggers = new TChain("Triggers/Events");
  TChain* treeBC = new TChain("BeamCounters/Events");
  
  cout << "===> Reading " << fname.size() << " files" << endl;
  cout << "  nTS = " << nTS << endl;
  
  for (int i = 0; i < fname.size(); ++i) {
    cout << "  " << fname[i] << endl;
    Events->Add(fname[i]);
    treeTriggers->Add(fname[i]);
    treeBC->Add(fname[i]);
  }
  
  float pulse[48][10];
  int depth[48];
  int ieta[48];
  int numChs;
  int led_trigger;
  double QADC5;
  
  Events->SetBranchAddress("numChs", &numChs);
  Events->SetBranchAddress("pulse",&pulse);
  Events->SetBranchAddress("ieta",&ieta);
  Events->SetBranchAddress("depth",&depth);
 
  treeTriggers->SetBranchAddress("led", &led_trigger);
 
  treeBC->SetBranchAddress("QADC5", &QADC5);
  
  // read events
  Int_t nentries = (Int_t)Events->GetEntries();
  cout << "Total events " << nentries << " " << flush;
  
  for (Int_t i = 0; i < nentries; i++) {
    // print progress
    if ( (i + 1) % 1000 == 0) {
      //cout << "processing event " << (i+1) << endl;
      cout << "." << flush;
    }
    
    Events->GetEntry(i);
    treeTriggers->GetEntry(i);
    treeBC->GetEntry(i);
    
    // process only "dark" events
    if (led_trigger != 0) continue;
    
    for (int j=0; j < numChs; j++) {
      
      // loop through sub-samples of size nTS
      for (int a = 0 ; a < 10/nTS; ++a) {
        
        float sumall = 0;
        
        for (int k=a*nTS; k < (a+1)*nTS; k++) {
          const double p = pulse[j][k];
          sumall += p;
        }
        
        spectr[led_trigger]->Fill(depth[j]*10+ieta[j], sumall);
      }
    }
  }
  
  cout << endl;
 
  // draw spectr vs channel plot
  TCanvas* c1 = new TCanvas("SiPM spectr vs channel", "SiPM spectr vs channel");
  //c1->Divide(1,2);
  //c1->cd(1);
  //c1->GetPad(1)->SetLogz();
  //spectr[1]->Draw("colz");
  
  //c1->cd(2);
  //c1->GetPad(2)->SetLogz();
  c1->SetLogz();
  spectr[0]->Draw("colz");
  
  // histograms of LED signal distributions
  /*
  TCanvas* c4 = new TCanvas("SiPM LED histograms", "SiPM LED histograms");
  c4->Divide(2, 2);
  for (int i = 0; i < N_channels; ++i) {
    TString name = selchan[i].name();
    const int bin = 1 + (selchan[i].fiber-fibermin)*10 + selchan[i].chan;
    TH1D* p = spectr[1]->ProjectionY(name, bin, bin);
    p->SetTitle(name + " led=1;signal, fC;#events");
    c4->cd(i+1);
    p->Draw("HIST");
    zoom_to_xcontents(p);
  }
  */
  
  
  //TFitResultPtr apdfit = APD->Fit("gaus", "QMS");
}


// 2016-Aug

// 1 fiber illumination
const vector<int> runs_lin_1fib = {
  5463,  5464,  5465,  5466,  5467,  5468,  5469,  5470,  5471,  5472,  5473,  5474,  5475
};


// 4 fiber illumination
const vector<int> runs_lin_4fib = {
  5494,  5495,  5496,  5497,  5498,  5499,  5500,  5501,  5502,  5503,  5504,  5505,  5506,  5507,  5508,  5511
};

/*
// 1 fiber with 10x filter, gain is approximate
const RunsSet runs_lin_1fib_filt10x = {
  5587,
  5586,
  5585,
  5584,
  5583,
  5582,
  5581,
  5580,
  5579,
  5578,
  5577,
  5576,
  5575,
  5574,
  5573,
  5572,
  5571,
  5570,
  5569,
  5568,
};

// 1 fiber with 10x, 100x and no filter, gain is approximate
const RunsSet runs_lin_1fib_filtNx = {
    {5591, 1.},
    {5592, 1.},
    {5593, 1.},
    {5594, 1.},
    {5595, 1.},
    {5596, 1.},
    {5597, 1.},
    {5598, 1.},
    {5599, 1.},
    {5600, 1.},
    {5601, 1.},
    {5602, 1.},
    {5603, 1.},
    {5604, 1.},
    {5605, 1.},
    {5606, 1.},
    {5607, 1.},
    {5608, 1.},
    {5609, 1.},
    {5610, 1.},
    {5611, 1.},
    {5612, 1.},
    {5613, 1.},
    {5614, 1.},
    {5615, 1.},
    {5616, 1.},
    {5617, 1.},
    {5618, 1.},
    {5619, 1.}
};
*/

void check_qie11_dark_data(int n)
{
  vector<TString> f;
  
  for (const int r : runs_lin_1fib) {
    f.push_back(TString::Format("dump-%06d.root", r));
  } 
  
  read_dark_data(f, n);
}
