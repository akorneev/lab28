#include "src/ADC_Conversion.h"
#include "src/ADC_Conversion2.h"

void show_qie11_lut()
{
  const int N = 256;
  double x[N];
  double y0[N];
  double y1[N];
  double y1m0[N];
  double y1r0[N];
  
  const Converter cnv;
  
  for (int i = 0; i < N; ++i) {
    x[i] = i;
    y0[i] = cnv.linearize(i);
    y1[i] = ADCtofC[i];
    y1m0[i] = y1[i] - y0[i];
    y1r0[i] = fabs(y1m0[i] / y0[i]);
  }
  
  // plot old and new ADC to fC
  TGraph* g0 = new TGraph(N, x, y0);
  g0->SetTitle("OLD");
  g0->SetMarkerStyle(kFullDotMedium);
  g0->SetFillColor(0);
  g0->SetMarkerColor(kBlack);
  g0->SetLineColor(kBlack);
  
  TGraph* g1 = new TGraph(N, x, y1);
  g1->SetTitle("NEW");
  g1->SetMarkerStyle(kFullDotMedium);
  g1->SetFillColor(0);
  g1->SetMarkerColor(kBlue);
  g1->SetLineColor(kBlue);
  
  TMultiGraph* mg = new TMultiGraph;
  mg->SetTitle("QIE11 ADC-to-fC OLD vs NEW;ADC counts;Charge, fC");
  mg->Add(g0);
  mg->Add(g1);
  mg->SetMinimum(0.1);
  
  TCanvas* c1 = new TCanvas("qie11-1-adctofc-old-and-new", "old-and-new");
  c1->SetGrid();
  c1->SetLogy();
  mg->Draw("APL");
  c1->BuildLegend(0.15, 0.88 - 0.05*2, 0.27, 0.88);
  c1->Print(".png");
  
  // plot new-old
  TGraph* g1m0 = new TGraph(N, x, y1m0);
  g1m0->SetTitle("NEW-OLD;ADC counts;Charge, fC");
  g1m0->SetMarkerStyle(kFullDotMedium);
  g1m0->SetFillColor(0);
  g1m0->SetMarkerColor(kBlack);
  g1m0->SetLineColor(kBlack);
  
  TCanvas* c2 = new TCanvas("qie11-2-adctofc-new-minus-old", "new-minus-old");
  c2->SetGrid();
  c2->SetLogy();
  g1m0->Draw("APL");
  c2->Print(".png");
  
  TGraph* g1r0 = new TGraph(N, x, y1r0);
  g1r0->SetTitle("(NEW-OLD)/OLD;ADC counts");
  g1r0->SetMarkerStyle(kFullDotMedium);
  g1r0->SetFillColor(0);
  g1r0->SetMarkerColor(kBlack);
  g1r0->SetLineColor(kBlack);
  g1r0->SetMinimum(0.01);
  
  TCanvas* c3 = new TCanvas("qie11-3-adctofc-new-vs-old", "new-vs-old");
  c3->SetGrid();
  c3->SetLogy();
  g1r0->Draw("APL");
  c3->Print(".png");
}
