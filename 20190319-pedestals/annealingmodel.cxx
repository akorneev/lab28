// .L annealingmodel.cxx

// meeting 20191210:
//   - 3.3 mm fit:
//   annealingmodel(0,250, 0.01368,  -1,1.,  0.02232,28.7,  -1,60.,  true)
//   annealingmodel(0,250, 0.0356001,  -1,1.,  -1,30.9379,  -1,60.,  false)    - no annealing
//   annealingmodel(0,8000, 0.01368,  -1,1.,  0.02232,28.7,  -1,60.,  true)    - 20 years prediction

//   -2.8 mm:
//     annealingmodel(0,250,  0.036,  0.38,  -1,1.,  0.62,28.7,  -1,60.,  true, "he2018-sipm28mm.csv")
//     annealingmodel(0,8000, 0.036,  0.38,  -1,1.,  0.62,28.7,  -1,60.,  true, "he2018-sipm28mm.csv")


// modelirowanie signala konstantoj s wesom w0 i tremq eksponentami taui s wesami wi i = 1,2,3
// toki dlq kazhdoj prorisowywayutsq otdel'no
// na polnom materiale - 75 tochek
// optimal'nye parametry nahodqtsq fitirowaniem


// dobawlena swetimost' - po 238 tochkam

// fitting - w predelah lim1-lim2 [0-230 dnej]

// paramenty fittinga - na whode

// tok w mikroamperah  QfC/200nS = Q/200 uA


#include "TString.h"
#include "TFile.h"
#include "TProfile.h"
#include "TMultiGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TFitResult.h"
#include "TLegend.h"
#include "TPRegexp.h"

#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <map>
using namespace std;


struct CsvFile {
  
  vector<vector<double> > data;
  //vector<string> head;
  
  
  void line_trim(string& s) const
  {
    //cout << "line_trim() in=" << s << endl;
    
    // remove leading '"'
    while (!s.empty() && (s[0] == '"')) s.erase(0, 1);
    
    // remove trailing ','
    while (!s.empty() && (s[s.size()-1] == '\r')) s.erase(s.size()-1, 1);
    while (!s.empty() && (s[s.size()-1] == '\n')) s.erase(s.size()-1, 1);
    while (!s.empty() && (s[s.size()-1] == ',')) s.erase(s.size()-1, 1);
    
    //cout << "line_trim() out=" << s << endl;
  }
  
  
  void read(const char* fname, const int ncols)
  {
    cout << "CsvFile::read() fname = " << fname << endl;
    
    data.clear();
    //head.clear();
    
    ifstream f(fname);
    
    string line;
    
    while (getline(f, line)) {
      // compatibility with Excel csv edits
      line_trim(line);
      
      // skip empty lines
      if (line.empty()) continue;
      
      // skip comments
      if (line[0] == '#') continue;
      
      //cout << "DEBUG: line=" << line << endl;
      
      vector<double> row;
      double x;
      istringstream iss(line);
      
      while (iss >> x) {
        row.push_back(x);
        iss.ignore(100, ',');
      }
      
      if (row.size() != ncols) {
        cout << "ERROR: incorrect input line: n_parameters=" << row.size() << " n_expected=" << ncols << "\n"
             << line
             << endl;
        exit(1);
      }
      
      data.push_back(row);
    }
    
    cout << "CsvFile::read() total entries = " << data.size() << endl;
  }
  
  void write(const char* fname) const
  {
    cout << "CsvFile::write() fname = " << fname << ", nrows = " << data.size() << endl;
    
    ofstream f(fname);
    f.precision(15);
    
    for (int i = 0; i < data.size(); ++i) {
      for (int j = 0; j < data[i].size(); ++j) {
        if (j != 0) f << ", ";
        f << data[i][j];
      }
      
      f << endl;
    }
  }
  
  int size() const { return data.size(); }
  
  double get(int irow, int icol) const { return data[irow][icol]; }
  
};

// delivered lumi data
struct LumiSection {
  double start;  // start timestamp, days
  double stop;   // stop timestamp, days
  double lumi;   // delivered lumi, nb^-1
  
  double duration() const { return (stop-start); }
  
  // divide lumi interval:
  //   x - midpoint
  //   index - 0: subrange before midpoint, 1: subrange after midpoint
  void set_subrange(double x, const int index)
  {
    // check and correct midpoint position
    if (x < start) x = start;
    if (x > stop) x = stop;
    
    const double F = (x - start) / duration();
    
    if (index == 0) {
      lumi *= F;
      stop = x;
    }
    else {
      lumi *= (1. - F);
      start = x;
    }
  }
  
  // split the [start,stop] range in "n" intervals
  void set_subrange_by_index(const int n, const int index)
  {
    // sanity check
    if (n < 1) return;
    if ((index < 0) || (index >= n)) return;
    
    const double newdur = duration() / n;
    
    stop = start + newdur * (index+1);
    start = start + newdur * index;
    lumi /= n;
  }
};

bool byStartLumi(const LumiSection& l, const LumiSection& r) {
  if (l.start == r.start)
    return (l.stop < r.stop);
  
  return (l.start < r.start);
};

vector<LumiSection> lumidata;

// integrated lumi data (computed from delivered lumi data)
int nl2 = 0;
const int MAXNL2 = 100000;
double Pss[MAXNL2];   // tochki start-stop posledowatel'no
double Lum2[MAXNL2];  // integral'naq dostawlennaq swetimost'

// dark current data
int nk = 0;
const int MAXNL = 10000;
double T[MAXNL];
double dc[MAXNL];

struct ModelParams {
  double lim1;
  double lim2;
  double scale;
  vector<double> w;
  vector<double> tau;
  
  // check t is inside [lim1, lim2)
  bool inRange(const double t) const { return (lim1 <= t) && (t < lim2); }
};

vector<ModelParams> model;

int getparamsbytime(double t)
{
  //cout << "getparamsbytime() t=" << t << endl;
  
  if (t < model[0].lim1) {
    //cout << "low idx=" << 0 << endl;
    return 0;
  }
  
  for (int i = 0; i < model.size(); i++)
    if (model[i].inRange(t)) {
      //cout << "idx=" << i << endl;
      return i;
    }
  
  //cout << "idx=" << (model.size()-1) << endl;
  return (model.size()-1);
}


Double_t frad(double tau, double t,
double I0, double A0, double T0)
{
// tip 0
  if (tau>0.) {
    return A0*(t-T0) + I0*exp(-(t-T0)/tau);
  } else {
    return A0*(t-T0) + I0;  // eto dlq tau = infinity
  }

/*
// tip 1 - formula Cavallari iz CMS Note
// http://cds.cern.ch/record/687554
  if (tau>0.) {
    return (I0-A0*tau)*exp(-(t-T0)/tau) + A0*tau;
  } else {
    //return I0;
    return I0 + A0*(t-T0);  // <-- Y.Andreev edit @2019Dec10
  }
*/


/*
// tip 2
  if (tau>0.) {
    return A0*t + (I0-A0*T0)*exp(-(t-T0)/tau);
  } else {
    return A0*(t-T0) + I0;  // eto dlq tau = infinity
  }
*/

/*
// tip 4a - formula 4.8-4.9 iz ECAL TDR
  if (tau>0.) {
    return A0*t + (I0-A0*t)*exp(-(t-T0)/tau);
  } else {
    return I0;
  }
*/
}

Double_t fun0(double tau, double t)
{
  //cout << "fun0() tau=" << tau << " t=" << t << endl;
  
  double I0 = 0.;
  double T0 = Pss[0];
  
  if (t < T0) return I0;

  for (int i = 1; i < nl2; ++i ) {
    // average instantaneous luminosity
    const double A0 = (Lum2[i] - Lum2[i-1]) / (Pss[i] - Pss[i-1]);
    
//printf("%3d %5.1f %5.2f %7.2f   ",i,I0,T0,A0);

    if (t<=Pss[i]) {
      return frad(tau,t, I0, A0, T0);
    }
    I0 = frad(tau,Pss[i], I0, A0, T0);  // tok
    T0 = Pss[i];
//printf("%5.1f %5.2f %7.2f\n",I0,T0,A0);
  }

  //A0 = 0.;
  return frad(tau,t, I0, 0., T0);
}

Double_t fitf(Double_t *x, Double_t *par)
{
  const double t = x[0];
  double val = par[0]*fun0(par[1], t) + par[2]*fun0(par[3], t) + par[4]*fun0(par[5], t) + par[6]*fun0(par[7], t);
  val *= par[8]; // scale
  return val;
}

Double_t fun0new(double, double t, int idx)
{
  //cout << "fun0new() idx=" << idx << " t=" << t << endl;
  
  double I0 = 0.;
  double T0 = Pss[0];
  
  if (t < T0) return I0;
  
  for (int i = 1; i < nl2; ++i ) {
    // average instantaneous luminosity
    const double A0 = (Pss[i] != Pss[i-1]) ? (Lum2[i] - Lum2[i-1]) / (Pss[i] - Pss[i-1]) : 0.;
    
    const double tau = model[getparamsbytime(Pss[i-1])].tau[idx];
    
    /*
    cout << "interval=" << Pss[i-1] << " " << Pss[i]
         << " tau=" << tau
         << " A0=" << A0
         << " T0=" << T0
         << " I0=" << I0;
    */
    
    const double t_next = min(t, Pss[i]);
    
    I0 = frad(tau,t_next, I0, A0, T0);  // tok
    T0 = Pss[i];
    
    /*
    cout << "| A0=" << A0
         << " T0=" << T0
         << " I0=" << I0
         << endl;
    */
    
    if (t<=Pss[i]) return I0;
  }
  
  
  const double tau = model[getparamsbytime(t)].tau[idx];

  //A0 = 0.;
  return frad(tau,t, I0, 0., T0);
}

Double_t fitfnew(Double_t *x, Double_t *par)
{
  const double t = x[0];
  //double val = par[0]*fun0new(par[1], t, 0) + par[2]*fun0new(par[3], t, 1) + par[4]*fun0new(par[5], t, 2) + par[6]*fun0new(par[7], t, 3);
  
  double val = 0.;
  if (par[0] > 0.) val += par[0]*fun0new(par[1], t, 0);
  if (par[2] > 0.) val += par[2]*fun0new(par[3], t, 1);
  if (par[4] > 0.) val += par[4]*fun0new(par[5], t, 2);
  if (par[6] > 0.) val += par[6]*fun0new(par[7], t, 3);
  
  const double scale = model[getparamsbytime(t)].scale;
  val *= scale;
  
  return val;
}



//________________________________________________________________________________________________________________________________

// LHC schedule
//   https://lhc-commissioning.web.cern.ch/lhc-commissioning/schedule/LHC-long-term.htm
//     https://lhc-commissioning.web.cern.ch/lhc-commissioning/schedule/LHC%20schedule%20beyond%20LS1%20MTP%202015_Freddy_June2015.pdf
//     https://lhc-commissioning.web.cern.ch/lhc-commissioning/schedule/HL-LHC-plots.htm
//   https://project-hl-lhc-industry.web.cern.ch/content/project-schedule


void add_year(
  double timeshift = 0.,
  double lumiscale = 1.
)
{
  // konstruirowanie tochnoj integral'noj swetimosti

  double intLum = nl2 ? Lum2[nl2-1] : 0.;
  for (const auto i : lumidata) {
    Pss[nl2] = i.start + timeshift;
    Pss[nl2+1] = i.stop + timeshift;
    
    const double L = i.lumi * lumiscale;
    Lum2[nl2] = intLum;
    intLum += L;
    Lum2[nl2+1] = intLum;
    
    nl2+=2;
    
    if (nl2 >= MAXNL2) {
      cout << "ERROR: insufficient buffer for Lum2[]" << endl;
      exit(1);
    }
  }
}

void graph_scale_Y(TGraph* g, const double f)
{
    const int n = g->GetN();
    double* y = g->GetY();
    
    for (int j = 0; j < n; ++j) {
      y[j] *= f;
    }
}


void annealingmodel(
  const char* modelfilename = "model.csv",
  const char* lumifilename = "lumi.csv",
  const char* dcfilename = "darkcurrent.csv",
  const bool runfit = false,
  const int ngrid = 10000
)
{
  CsvFile modelfile;
  modelfile.read(modelfilename, 10);
  
  model.clear();
  for (int i = 0; i < modelfile.size(); ++i) {
    ModelParams m;
    m.lim1 = modelfile.get(i, 0);
    m.lim2 = modelfile.get(i, 1);
    m.scale = modelfile.get(i, 2);
    m.w.assign(4,0);
    m.tau.assign(4,0);
    m.w[0] = modelfile.get(i, 3);
    m.tau[0] = 0.;
    m.w[1] = modelfile.get(i, 4);
    m.tau[1] = modelfile.get(i, 5);
    m.w[2] = modelfile.get(i, 6);
    m.tau[2] = modelfile.get(i, 7);
    m.w[3] = modelfile.get(i, 8);
    m.tau[3] = modelfile.get(i, 9);
    model.push_back(m);
  }
  
  double lim1 = model[0].lim1;
  double lim2 = model[model.size()-1].lim2;
  double scale = model[0].scale;
  double wes0 = model[0].w[0];
  double wes1 = model[0].w[1];
  double tau1 = model[0].tau[1];
  double wes2 = model[0].w[2];
  double tau2 = model[0].tau[2];
  double wes3 = model[0].w[3];
  double tau3 = model[0].tau[3];
  
  // load lumi data
  CsvFile lumifile;
  lumifile.read(lumifilename, 3);
  lumidata.clear();
  
  for (int i = 0; i < lumifile.size(); ++i) {
    const int nsplit = (lumifile.get(i, 2) != 0.) ? 100 : 1;
    
    for (int j = 0; j < nsplit; ++j) {
      LumiSection s = {lumifile.get(i, 0), lumifile.get(i, 1), lumifile.get(i, 2)};
      s.set_subrange_by_index(nsplit, j);
      lumidata.push_back(s);
    }
  }
  
  vector<double> edges;
  for (const auto i : model) {
    edges.push_back(i.lim1);
    edges.push_back(i.lim2);
  }
  
  for (const auto e : edges) {
    // search for split candidate
    for (int j = 0; j < lumidata.size(); ++j) {
      const bool isInside = (lumidata[j].start < e) && (e < lumidata[j].stop);
      // split lumi section
      if (isInside) {
        auto nth = lumidata.begin() + j;
        lumidata.insert(nth, lumidata[j]);
        
        lumidata[j].set_subrange(e, 0);
        lumidata[j+1].set_subrange(e, 1);
        
        break;
      }
    }
    
    // search for edge
    bool isEdgePresent = false;
    for (const auto j : lumidata)
      if ((e == j.start) || (e == j.stop)) {
        isEdgePresent = true;
        break;
      }
    
    if (!isEdgePresent)
      lumidata.push_back({e,e,0.});
  }
  
  sort(lumidata.begin(), lumidata.end(), byStartLumi);
  
  //cout << "INFO: lumidata" << endl;
  //for (auto i : lumidata)
  //  cout << i.start << " " << i.stop << " " << i.lumi << endl;
  
  // load dark current data
  CsvFile dcfile;
  dcfile.read(dcfilename, 2);
  nk = 0;
  
  for (int i = 0; i < dcfile.size(); ++i) {
    T[nk] = dcfile.get(i, 0);
    dc[nk] = dcfile.get(i, 1);
    nk++;
  }
  
  // https://lhc-commissioning.web.cern.ch/lhc-commissioning/schedule/HL-LHC-plots.htm
  // "Nominal HL-LHC luminosity - ions stopped after LS4"
  // scale factor 1.6 is to bring total lumi to 2900 from 1830
  nl2 = 0;
  add_year(    0.,     1. ); // 2018, scale 1. == 2E34 lumi
  
  /*
  // LS2
  add_year( 3*365, 1.6*0.95); // 2021
  add_year( 4*365, 1.6*1. );
  add_year( 5*365, 1.6*1. );
  // LS3
  add_year( 8*365, 1.6*1.5); // 2026
  add_year( 9*365, 1.6*2. );
  add_year(10*365, 1.6*2.5);
  add_year(11*365, 1.6*2.5);
  // LS4
  add_year(13*365, 1.6*2.5);
  add_year(14*365, 1.6*2.5);
  add_year(15*365, 1.6*2.5);
  // LS5
  add_year(17*365, 1.6*2.5);
  add_year(18*365, 1.6*2.5);
  add_year(19*365, 1.6*2.5);
  */
  
  //cout << "nl2=" << nl2 << endl;
  
  // subtract pedestal
  const double ped = nk ? dc[0] : 0.;
  for (int i = 0; i < nk; ++i ) {
    dc[i] -= ped;
  }
  
  // style
  gStyle->SetLineScalePS(1);
  //gStyle->SetOptStat(0);
  gStyle->SetTextFont(42);
  
  // integrated lumi
  TCanvas* c3 = new TCanvas("lumi", "lumi");
  c3->SetGrid();
  
  TGraph* glum = new TGraph(nl2, Pss, Lum2);
  glum->SetTitle("Integrated Luminosity;time, days;integrated luminosity, fb^{-1}");
  glum->SetMarkerStyle(20);
  glum->SetMarkerSize(0.2);
  glum->SetFillColor(0);
  glum->SetMarkerColor(kBlue);
  glum->SetLineColor(kBlue);
  glum->GetXaxis()->SetLimits(0., lim2);
  glum->GetYaxis()->SetRangeUser(0., 1.1*glum->Eval(lim2));
  glum->Draw("APL");
  
  // dark current
  TCanvas* c2 = new TCanvas("darkcurrent", "darkcurrent");
  c2->SetGrid();
  
  TGraph* g = new TGraph(nk, T, dc);
  g->SetTitle("Dark Current;time, days;dark current, #muA");
  //g->SetTitle(dcfilename);
  g->SetMarkerStyle(21);
  g->SetMarkerSize(0.5);
  g->SetFillColor(0);
  g->SetMarkerColor(kGreen-2);
  g->SetLineColor(kGreen-2);
  g->Draw("APL");


  // model
  //TF1 *ff = new TF1("fit",fitf,lim1,lim2,9);
  TF1 *ff = new TF1("fit",fitfnew,lim1,lim2,9);
  ff->SetTitle("Model;time, days;dark current, #muA");
  ff->SetLineColor(kRed);
  ff->SetNpx(10000);
  ff->SetParameters(wes0,0, wes1,tau1, wes2,tau2, wes3,tau3, scale);
  ff->SetParNames("W0","Tau0","W1","Tau1","W2","Tau2","W3","Tau3", "scale");
  
  ff->SetParLimits(0,0.,1e9);  // w0
  ff->SetParLimits(2,0.,1e9);  // w1
  ff->SetParLimits(4,0.,1e9);  // w2
  ff->SetParLimits(6,0.,1e9);  // w3
  
  ff->FixParameter(1, 0.);     // tau0
  ff->SetParLimits(3,1.,1e9);  // tau1
  ff->SetParLimits(5,1.,1e9);  // tau2
  ff->SetParLimits(7,1.,1e9);  // tau3
  
  ff->FixParameter(8, scale);     // scale
  
  if (wes1 < 0.) {
    ff->FixParameter(2, 0.);
    ff->FixParameter(3, 1.);
  }

  if (wes2 < 0.) {
    ff->FixParameter(4, 0.);
    ff->FixParameter(5, 1.);
  }

  if (wes3 < 0.) {
    ff->FixParameter(6, 0.);
    ff->FixParameter(7, 1.);
  }

  if (runfit) {
    g->Fit(ff, "RN");
    double chi = g->Chisquare(ff);
    chi /= g->GetN();
    cout << "fit limits " << lim1 << " " << lim2 << "\n"
         << "fit chi=" << chi << "\n"
         << "chi/ndof=" << chi << endl;
  }
  
  double w0, w1, w2, w3;
  w0 = ff->GetParameter(0);
  w1 = ff->GetParameter(2);
  w2 = ff->GetParameter(4);
  w3 = ff->GetParameter(6);
  tau1 = ff->GetParameter(3);
  tau2 = ff->GetParameter(5);
  tau3 = ff->GetParameter(7);
  const double wsum = w0 + w1 + w2 + w3;  // naklon
  
  // errors
  const double ew0 = 100*ff->GetParError(0)/ff->GetParameter(0);
  const double ew1 = 100*ff->GetParError(2)/ff->GetParameter(2);
  const double ew2 = 100*ff->GetParError(4)/ff->GetParameter(4);
  const double ew3 = 100*ff->GetParError(6)/ff->GetParameter(6);
  const double etau1 = 100*ff->GetParError(3)/ff->GetParameter(3);
  const double etau2 = 100*ff->GetParError(5)/ff->GetParameter(5);
  const double etau3 = 100*ff->GetParError(7)/ff->GetParameter(7);
  
  if (runfit) {
  cout << std::left
       << "  scale   " << setw(8) << wsum << "\n"
       << "  #0:   w=" << setw(8) << w0/wsum << " t=." << "\n"
       << "  #1:   w=" << setw(8) << w1/wsum << " t=" << ff->GetParameter(3) << "\n"
       << "  #2:   w=" << setw(8) << w2/wsum << " t=" << ff->GetParameter(5) << "\n"
       << "  #3:   w=" << setw(8) << w3/wsum << " t=" << ff->GetParameter(7) << "\n"
       << endl;
  }
  
  // draw model
  ff->SetRange(0,lim2);
  ff->DrawClone(g->GetN() ? "same" : "");
  
  const double ifinal = ff->Eval(lim2);
  cout << "I_final(t=" << lim2 << ") = " << ifinal << "\n"
       << "I_max(t=" << ff->GetMaximumX() << ") = " << ff->GetMaximum() << "\n"
       << endl;
  
  // save model
  CsvFile modelout;
  //for (int i = 0; i < glum->GetN(); ++i) {
  //  const double x = glum->GetX()[i];
  //  modelout.data.push_back({x, ff->Eval(x)});
  // }
  
  for (int i = 0; i < ngrid+1; ++i) {
    const double x = lim1 + i*(lim2-lim1)/ngrid;
    modelout.data.push_back({x, ff->Eval(x)});
  }
  
  modelout.write("output-darkcurrent.csv");
  
  
  // text box with the list of time constants
  TPaveText pt33(0.66,0.13,0.88,0.31,"NDC");
  pt33.SetBorderSize(1);
  
  TString buf;
  if (wes0>=0) {
    buf.Form("w_{0} = %.2f (err = %.1f%%)", w0/wsum, ew0);
    pt33.AddText(buf);
    buf.Form("#tau_{0} = #infty");
    pt33.AddText(buf);
  }
  
  if (wes1>=0) {
    buf.Form("w_{1} = %.2f (err = %.1f%%)", w1/wsum, ew1);
    pt33.AddText(buf);
    buf.Form("#tau_{1} = %.2f (err = %.1f%%)", tau1, etau1);
    pt33.AddText(buf);
  }
  
  if (wes2>=0) {
    buf.Form("w_{2} = %.2f (err = %.1f%%)", w2/wsum, ew2);
    pt33.AddText(buf);
    buf.Form("#tau_{2} = %.2f (err = %.1f%%)", tau2, etau2);
    pt33.AddText(buf);
  }
  
  if (wes3>=0) {
    buf.Form("w_{3} = %.2f (err = %.1f%%)", w3/wsum, ew3);
    pt33.AddText(buf);
    buf.Form("#tau_{3} = %.2f (err = %.1f%%)", tau3, etau3);
    pt33.AddText(buf);
  }
  
  for (int i = 0; i < pt33.GetSize(); ++i)
    pt33.GetLine(i)->SetTextAlign(12);
  
  
  if ((modelfile.size() == 1) || runfit)
    pt33.DrawClone();
  
  // legend
  TLegend* tagleg = new TLegend(0.66, 0.35, 0.88, 0.45);
  //tagleg->AddEntry(glum, " lumi", "PL");
  if (g->GetN()) tagleg->AddEntry(g, dcfilename, "PL");
  tagleg->AddEntry(ff, " model", "PL");
  
  if (g->GetN())
    tagleg->DrawClone();

  // parameters regions
  for (auto i : model) {
    TArrow* lin1 = new TArrow(i.lim1,0,i.lim1,ifinal/20, 0.01, "|>");
    lin1->SetLineColor(kYellow-2);
    lin1->SetFillColor(kYellow-2);
    lin1->Draw();
  }
  
  //c3->Print(".png");
}

int mgsize(TMultiGraph* mg)
{
  if (!mg) return 0;
  TList* l = mg->GetListOfGraphs();
  if (!l) return 0;
  return l->GetEntries();
}

void multiplot(vector<string> fnames)
{
  TMultiGraph* mg = new TMultiGraph;
  
  // predefined pallete
  const int ncolors = 6;
  const int colors[ncolors] = {kBlue+1, kGreen+1, kRed+1, kMagenta+1, kCyan+2, kYellow+2};
  
  for (auto i : fnames) {
    CsvFile csv;
    csv.read(i.c_str(), 2);
    const int N = csv.size();
    
    double* x = new double[N];
    double* y = new double[N];
    
    for (int j = 0; j < N; ++j) {
      x[j] = csv.get(j, 0);
      y[j] = csv.get(j, 1);
    }
    
    TGraph* g = new TGraph(N, x, y);
    g->SetTitle(i.c_str());
    //g->SetMarkerStyle(21);
    //g->SetMarkerSize(0.5);
    g->SetMarkerStyle(20);
    g->SetMarkerSize(0.2);
    g->SetFillColor(0);
    
    const int idx = mgsize(mg);
    const int col = colors[idx % ncolors];
    g->SetMarkerColor(col);
    g->SetLineColor(col);
    
    //g->Draw("APL");
    
    mg->Add(g);
  }
  
  TCanvas* c = new TCanvas("multiplot", "multiplot");
  c->SetGrid();
  // use automatic pallete if predefined is not enoght
  TString opt = (mgsize(mg) <= ncolors) ? "APL" : "APL  pmc plc";
  mg->Draw(opt);
  c->BuildLegend();
}
