#!/bin/bash -e

subdir="annealingmodel$(date +%Y%m%d)"
distzip="$subdir.zip"

if [[ -e "$distzip" ]] ; then
  echo "ERROR: output file already exists: $distzip"
  exit 1
fi

mkdir "$subdir"
cp annealingmodel.cxx usage.txt *.csv root622.lnk "$subdir/"
rm -f "$subdir/output-darkcurrent.csv"

zip -q -m -r "$distzip" "$subdir/"

echo "INFO: release output = $distzip"
