
/*
// initial test runs
// gain from run 2517:
//    (734.6 - 312.7) / 8 = 52.7
//    (745.9 - 341.6) / 8 = 50.5
//    (691.9 - 282.7) / 8 = 51.2
//    (772   - 340.2) / 8 = 54.0
const RunsSet runs_initial_test = {
  "initial-test",
  {52.7, 50.5, 51.2, 54.0},
  {
    {2517, 1.},
    {2518, 1.},
    {2519, 1.},
    {2520, 1.},
    {2521, 1.},
    {2522, 1.},
    {2523, 1.},
    {2524, 1.},
    {2525, 1.},
    {2526, 1.},
    {2527, 1.},
    {2528, 1.},
    {2529, 1.},
    {2530, 1.}
  }
};

// 1 fiber illumination (no light mixer)
// 2551 has less light than 2550
// ADP-QADC is saturated in run 2566 and 2567
// gain from run 2551:
//    (524.4 - 152.2 ) / 7  = 53.2
//    (642.2 - 188.2 ) / 9  = 50.4
//    (740.4 - 229.4 ) / 10 = 51.1
//    (825.6 - 284.9 ) / 10 = 54.1
const RunsSet runs_1fib_nomix = {
  "1fib_nomix",
  {53.2, 50.4, 51.1, 54.1},
  {
    {2551, 1.},
    {2550, 1.},
    {2552, 1.},
    {2553, 1.},
    {2554, 1.},
    {2555, 1.},
    {2556, 1.},
    {2557, 1.},
    {2558, 1.},
    {2559, 1.},
    {2560, 1.},
    {2561, shunt11_5},
    {2562, shunt11_5},
    {2563, shunt11_5},
    {2564, shunt11_5},
    {2565, shunt11_5},
//    {2566, shunt11_5},
//    {2567, shunt11_5},
    {2568, shunt11_5}
  }
};

// 4 fiber illumination (no light mixer)
// gain from run 2570:
//   ( 944.9 - 417.8 ) / 10 = 52.7
//   ( 945.9 - 591.5 ) /  7 = 50.6
//   ( 1096 -   688.7) / 8 = 50.9
//   ( 1087 -   606.1) / 9 = 53.4
const RunsSet runs_4fib_nomix = {
  "4fib_nomix",
  {52.7, 50.6, 50.9, 53.4},
  {
    {2570, 1.},
    {2571, 1.},
    {2572, 1.},
    {2573, 1.},
    {2574, 1.},
    {2575, 1.},
    {2576, 1.},
    {2577, 1.},
    {2578, shunt11_5},
    {2579, shunt11_5},
    {2580, shunt11_5},
    {2581, shunt11_5},
    {2582, shunt11_5},
    {2583, shunt11_5}
  }
};

// 1 fiber illumination, 1 mm thick mixer
// 2620 and 2638 are bad data , DAQ got stuck and QDC saturation
// gain from run 2612:
//   (633.7 - 216)   / 8  = 52.2
//   (697.2 - 243.8) / 9  = 50.4
//   (796.2 - 286.7) / 10 = 51.0
//   (935.3 - 401.8) / 10 = 53.4
const RunsSet runs_1fib_1mm_mix = {
  "1fib_1mm_mix",
  {52.2, 50.4, 51.0, 53.4},
  {
    {2612, 1.},
    {2613, 1.},
    {2614, 1.},
    {2615, 1.},
    {2616, 1.},
    {2617, 1.},
    {2618, 1.},
    {2619, 1.},
//    {2620, 1.},
    {2621, 1.},
    {2622, 1.},
    {2623, 1.},
    {2624, 1.},
    {2625, 1.},
    {2626, 1.},
    {2627, 1.},
    {2628, 1.},
    {2629, 1.},
    {2630, shunt11_5},
    {2631, shunt11_5},
    {2632, shunt11_5},
    {2633, shunt11_5},
    {2634, shunt11_5},
    {2635, shunt11_5},
    {2636, shunt11_5},
    {2637, shunt11_5},
//    {2638, shunt11_5},
    {2639, shunt11_5}
  }
};


// 4 fiber illumination, 1 mm thick mixer
// 2596 is bad data DAQ got stuck
// gain per channel, from run 2590:
//  (679.6 - 202.6) / 9  = 53.0
//  (841.7 - 339 )  / 10 = 50.3
//  (892.1 - 381.9) / 10 = 51.0
//  (925.9 - 389)   / 10 = 53.7
const RunsSet runs_4fib_1mm_mix = {
  "4fib_1mm_mix",
  {53.0, 50.3, 51.0, 53.7},
  {
    {2590, 1.},
    {2591, 1.},
    {2592, 1.},
    {2593, 1.},
    {2594, 1.},
    {2595, 1.},
//    {2596, 1.},
    {2597, 1.},
    {2598, 1.},
    {2599, 1.},
    {2600, shunt11_5},
    {2601, shunt11_5},
    {2602, shunt11_5},
    {2603, shunt11_5},
    {2604, shunt11_5},
    {2605, shunt11_5},
    {2606, shunt11_5},
    {2607, shunt11_5},
    {2608, shunt11_5},
    {2609, shunt11_5},
    {2610, shunt11_5}
  }
};

// 1 fiber illumination, 1.6 mm mixer
// 2659  DAQ got stuck
// gain from 2642:
//    (681   - 150.8) / 10 = 53.0
//    (743.6 - 238.4) / 10 = 50.5
//    (893.6 - 384.8) / 10 = 50.9
//    (931.1 - 391.9) / 10 = 53.9
const RunsSet runs_1fib_16mm_mix = {
  "1fib_1.6mm_mix",
  {53.0, 50.5, 50.9, 53.9},
  {
    {2642, 1.},
    {2643, 1.},
    {2644, 1.},
    {2645, 1.},
    {2646, 1.},
    {2647, 1.},
    {2648, 1.},
    {2649, 1.},
    {2650, 1.},
    {2651, 1.},
    {2652, 1.},
    {2653, 1.},
    {2654, 1.},
    {2655, 1.},
    {2656, 1.},
    {2657, 1.},
    {2658, 1.},
//    {2659, shunt11_5},
    {2660, shunt11_5},
    {2661, shunt11_5},
    {2662, shunt11_5},
    {2663, shunt11_5},
    {2664, shunt11_5},
    {2665, shunt11_5},
    {2666, shunt11_5},
    {2667, shunt11_5},
    {2668, shunt11_5},
    {2669, shunt11_5}
  }
};

// 4 fiber illumination, 1.6 mm mixer
// 2672 is the lowest light and best for gain calibration
// 2678 is bad data DAQ got stuck
// gain from 2672:
//   (  785.7 - 203.8) / 11 = 52.9
//   (  893.8 - 341.1) / 11 = 50.2
//   (  945   - 383.1) / 11 = 51.1
//   (  928.5 - 390.8) / 10 = 53.8
const RunsSet runs_4fib_16mm_mix = {
  "4fib_1.6mm_mix",
  {52.9, 50.2, 51.1, 53.8},
  {
    {2672, 1.},
    {2671, 1.},
    {2673, 1.},
    {2674, 1.},
    {2675, 1.},
    {2676, 1.},
    {2677, 1.},
//    {2678, 1.},
    {2679, 1.},
    {2680, 1.},
    {2681, 1.},
    {2682, 1.},
    {2683, 1.},
    {2684, shunt11_5},
    {2685, shunt11_5},
    {2686, shunt11_5},
    {2687, shunt11_5},
    {2688, shunt11_5},
    {2689, shunt11_5},
    {2690, shunt11_5},
    {2691, shunt11_5},
    {2692, shunt11_5},
    {2693, shunt11_5},
    {2694, shunt11_5}
  }
};

// 2016-Aug

// 1 fiber illumination
const RunsSet runs_lin_1fib = {
  "1fib",
//  {47.3, 46.0, 46.3, 47.6},
  {47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0},
  {
    {5463, 1.},
    {5464, 1.},
    {5465, 1.},
    {5466, 1.},
    {5467, 1.},
    {5468, 1.},
    {5469, 1.},
    {5470, 1.},
    {5471, 1.},
    {5472, 1.},
    {5473, 1.},
    {5474, 1.},
    {5475, 1.},
    {5477, shunt11_5},
    {5478, shunt11_5},
    {5479, shunt11_5},
    {5480, shunt11_5},
    {5481, shunt11_5},
    {5482, shunt11_5},
    {5483, shunt11_5},  // APD = 330V
    {5484, shunt11_5},
    {5485, shunt11_5}
  }
};


// 4 fiber illumination
const RunsSet runs_lin_4fib = {
  "4fib",
  {47.6, 46.6, 46.0, 46.6},
  {
    {5494, 1.},
    {5495, 1.},
    {5496, 1.},
    {5497, 1.},
    {5498, 1.},
    {5499, 1.},
    {5500, 1.},
    {5501, 1.},
    {5502, 1.},
    {5503, 1.},
    {5504, 1.},
    {5505, 1.},
    {5506, 1.},
    {5507, 1.},
    {5508, 1.},
    {5511, 1.},
    {5512, shunt11_5},
    {5513, shunt11_5},
    {5514, shunt11_5},
    {5515, shunt11_5},
    {5516, shunt11_5},
    {5517, shunt11_5},
    {5518, shunt11_5},
    {5519, shunt11_5},
    {5520, shunt11_5}  // APD = 330V
  }
};

// 2016-Sep

// 1 fiber with 10x filter, gain is approximate
const RunsSet runs_lin_1fib_filt10x = {
  "1fib_10x",
  {47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0},
  {
    {5587, 1.},
    {5586, 1.},
    {5585, 1.},
    {5584, 1.},
    {5583, 1.},
    {5582, 1.},
    {5581, 1.},
    {5580, 1.},
    {5579, 1.},
    {5578, 1.},
    {5577, 1.},
    {5576, 1.},
    {5575, 1.},
    {5574, 1.},
    {5573, 1.},
    {5572, 1.},
    {5571, 1.},
    {5570, 1.},
    {5569, 1.},
    {5568, 1.}
  }
};

// 1 fiber with 10x, 100x and no filter, gain is approximate
const RunsSet runs_lin_1fib_filtNx = {
  "1fib_Nx",
  {47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0, 47.0},
  {
    {5591, 1.},
    {5592, 1.},
    {5593, 1.},
    {5594, 1.},
    {5595, 1.},
    {5596, 1.},
    {5597, 1.},
    {5598, 1.},
    {5599, 1.},
    {5600, 1.},
    {5601, 1.},
    {5602, 1.},
    {5603, 1.},
    {5604, 1.},
    {5605, 1.},
    {5606, 1.},
    {5607, 1.},
    {5608, 1.},
    {5609, 1.},
    {5610, 1.},
    {5611, 1.},
    {5612, 1.},
    {5613, 1.},
    {5614, 1.},
    {5615, 1.},
    {5616, 1.},
    {5617, 1.},
    {5618, 1.},
    {5619, 1.}
  }
};

*/

// 2016-Oct

// SiPM+APD+HPD readout
const RunsSet runs20161012_sipm_hpd_apd = {
  "sipm_hpd_apd",
  {
    {"APD", 0, ""},
    {"SiPM-rm3-ch41", 47.0, ""},
    {"SiPM-rm3-ch63", 47.0, ""},
    {"SiPM-rm3-ch66", 47.0, ""},
    {"SiPM-rm3-ch71", 47.0, ""},
    {"SiPM-rm3-ch82", 47.0, ""},
    {"SiPM-rm3-ch83", 47.0, ""},
    {"SiPM-rm3-ch84", 47.0, ""},
    {"SiPM-rm3-ch86", 47.0, ""},
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""},
    {"HPD-rm1-ch11", 0, ""},
    {"HPD-rm1-ch17", 0, ""}
  },
  {
    {5694, shunt11_5},
    {5695, shunt11_5},
    {5696, shunt11_5},
    {5697, shunt11_5},
    {5698, shunt11_5},
    {5699, shunt11_5},
    {5700, shunt11_5},
    {5701, shunt11_5},
    {5702, shunt11_5},
    {5703, shunt11_5},
    {5704, 1.},
    {5705, 1.},
    {5706, 1.},
    {5707, 1.},
    {5708, 1.},
    {5709, 1.},
    {5710, 1.},
    {5711, 1.},
    {5712, 1.},
    {5713, 1.},
    {5714, 1.},
    {5715, 1.},
    {5716, 1.},
    {5717, 1.},
    {5718, 1.}
  }
};

// SiPM voltage scan
const vector<double> runs20161026_voltages = {
  62, 62.2, 62.4, 62.6, 62.8,
  63, 63.2, 63.4, 63.6, 63.8,
  64, 64.2, 64.4, 64.6, 64.8,
  65, 65.2, 65.4, 65.6, 65.8,
  66, 66.5,
  67,
  68, 68.5,
  69, 69.5,
  70, 70.5,
  71
  //68
};

const RunsSet runs20161026_bvscan = {
  "sipm_bvscan",
  {
    /*
    {"SiPM-rm3-ch13", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    
    {"SiPM-rm3-ch22", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch23", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch24", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch41", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch45", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch46", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch51", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch53", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch54", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch56", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch61", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch63", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch66", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch71", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch75", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch76", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch81", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch82", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch83", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch85", 40.0, "1fib-2.8mm"},
    
    // HPD reference
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""}
    */
  },
  {
    {5801, 1.},
    {5802, 1.},
    {5803, 1.},
    {5804, 1.},
    {5805, 1.},
    {5806, 1.},
    {5807, 1.},
    {5808, 1.},
    {5809, 1.},
    {5810, 1.},
    {5811, 1.},
    {5812, 1.},
    {5813, 1.},
    {5814, 1.},
    {5815, 1.},
    {5816, 1.},
    {5817, 1.},
    {5818, 1.},
    {5819, 1.},
    {5820, 1.},
    {5821, 1.},
    {5822, 1.},
    {5823, 1.},
    {5825, 1.},
    {5826, 1.},
    {5827, 1.},
    {5828, 1.},
    {5829, 1.},
    {5830, 1.},
    {5831, 1.}
    // {5832, 1.}
  }
};

// 2016-Nov
// SiPM+HPD readout, 1 and 4 fibers
const RunsSet runs20161128_sipm_hpd = {
  "sipm_hpd",
  {
    // 1-fiber channels
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    //{"SiPM-rm3-ch22", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch23", 40.0, "1fib-2.8mm"},
    //{"SiPM-rm3-ch24", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch45", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch46", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch51", 40.0, "1fib-2.8mm"},
    
    // 4-fiber channels
    //{"SiPM-rm3-ch53", 40.0, "4fib-3.3mm"},
    {"SiPM-rm3-ch54", 40.0, "4fib-3.3mm"},
    //{"SiPM-rm3-ch61", 40.0, "4fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "4fib-2.8mm"},
    
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""}
  },
  {
    {5857, shunt11_5},
    {5858, shunt11_5},
    {5859, shunt11_5},
    {5860, shunt11_5},
    {5861, shunt11_5},
    {5862, shunt11_5},
    {5863, shunt11_5},
    {5864, shunt11_5},
    {5865, shunt11_5},
    {5866, shunt11_5},
    {5867, shunt11_5},
    {5868, shunt11_5},
    {5869, shunt11_5},
    {5870, shunt11_5},
    {5871, shunt11_5},
    {5872, 1.},
    {5873, 1.},
    {5874, 1.},
    {5875, 1.},
    {5876, 1.},
    {5877, 1.},
    {5878, 1.},
    {5879, 1.},
    {5880, 1.},
    {5881, 1.},
    {5882, 1.},
    {5883, 1.},
    {5885, 1.},
    {5887, 1.},
    {5888, 1.},
    {5889, 1.}
  }
};

// SiPM+HPD readout, 1 fiber
const RunsSet runs20161129_sipm1_hpd = {
  "sipm1_hpd",
  {
    // 1-fiber channels
    //{"SiPM-rm3-ch13", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    
    //{"SiPM-rm3-ch22", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch23", 40.0, "1fib-2.8mm"},
    //{"SiPM-rm3-ch24", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch41", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch45", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch46", 40.0, "1fib-2.8mm"},
    
    //{"SiPM-rm3-ch51", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch53", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch54", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch56", 40.0, "1fib-2.8mm"},
    
    //{"SiPM-rm3-ch61", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch63", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch66", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch71", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch75", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch76", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch81", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch82", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch83", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch85", 40.0, "1fib-2.8mm"},
    
    // HPD reference
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""}
  },
  {
    {5900, shunt11_5},
    {5901, shunt11_5},
    {5902, shunt11_5},
    {5903, shunt11_5},
    {5904, shunt11_5},
    {5906, shunt11_5},
    {5907, shunt11_5},
    {5908, shunt11_5},
    {5909, shunt11_5},
    {5910, shunt11_5},
    {5911, shunt11_5},
    {5912, shunt11_5},
    {5913, shunt11_5},
    {5914, shunt11_5},
    {5915, shunt11_5},
    {5916, 1.},
    {5917, 1.},
    {5918, 1.},
    {5919, 1.},
    {5920, 1.},
    {5921, 1.},
    {5922, 1.},
    {5923, 1.},
    {5925, 1.},
    {5926, 1.},
    {5927, 1.},
    {5928, 1.},
    {5929, 1.},
    {5930, 1.},
    {5931, 1.}
  }
};

// 2016-Dec
// SiPM+HPD readout, for QIE11 check
const RunsSet runs20161207_shunt1 = {
  "shunt1",
  {
    // 1-fiber channels
    {"SiPM-rm3-ch13", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    
    {"SiPM-rm3-ch22", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch23", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch24", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch41", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch45", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch46", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch51", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch53", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch54", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch56", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch61", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch63", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch66", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch71", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch75", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch76", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch81", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch82", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch83", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch85", 40.0, "1fib-2.8mm"},
    
    // HPD reference
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""}
  },
  {
    
    {6000, 1.},
    {6003, 1.},
    {6006, 1.},
    {6009, 1.},
    {6012, 1.},
    // {6060, 1.},
    // {6063, 1.},
    
    {6015, 1.},
    // {6066, 1.},
    
    {6018, 1.},
    // {6069, 1.},
    // {6072, 1.},
    
    {6021, 1.},
    {6024, 1.},
    {6027, 1.},
    {6030, 1.},
    {6033, 1.},
    {6036, 1.},
    {6039, 1.},
    {6042, 1.},
    {6045, 1.}
  }
};

const RunsSet runs20161207_shunt5 = {
  "shunt5",
  {
    // 1-fiber channels
    {"SiPM-rm3-ch13", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    
    {"SiPM-rm3-ch22", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch23", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch24", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch41", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch45", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch46", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch51", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch53", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch54", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch56", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch61", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch63", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch66", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch71", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch75", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch76", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch81", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch82", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch83", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch85", 40.0, "1fib-2.8mm"},
    
    // HPD reference
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""}
  },
  {
    {6001, 5.},
    {6004, 5.},
    {6007, 5.},
    {6010, 5.},
    {6013, 5.},
    {6016, 5.},
    {6019, 5.},
    {6022, 5.},
    {6025, 5.},
    {6028, 5.},
    {6031, 5.},
    {6034, 5.},
    {6037, 5.},
    {6040, 5.},
    {6043, 5.},
    {6046, 5.}
    
    // {6061, 5.},
    // {6064, 5.},
    // {6067, 5.},
    // {6070, 5.},
    // {6073, 5.}
  }
};

const RunsSet runs20161207_shunt11 = {
  "shunt11",
  {
    // 1-fiber channels
    {"SiPM-rm3-ch13", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    
    {"SiPM-rm3-ch22", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch23", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch24", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch41", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch45", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch46", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch51", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch53", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch54", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch56", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch61", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch63", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch66", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch71", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch75", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch76", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch81", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch82", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch83", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch85", 40.0, "1fib-2.8mm"},
    
    // HPD reference
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch6", 0, ""}
  },
  {
    {6002, 11.5},
    {6005, 11.5},
    {6008, 11.5},
    {6011, 11.5},
    {6014, 11.5},
    {6017, 11.5},
    {6020, 11.5},
    {6023, 11.5},
    {6026, 11.5},
    {6029, 11.5},
    {6032, 11.5},
    {6035, 11.5},
    {6038, 11.5},
    {6041, 11.5},
    {6044, 11.5},
    {6047, 11.5}
    
    // {6062, 11.5},
    // {6065, 11.5},
    // {6068, 11.5},
    // {6071, 11.5},
    // {6074, 11.5}
  }
};

// 2017-Feb
// P5 SiPM+HPD readout, laser HEP_Megatile at different laser amplitudes
// http://cmsonline.cern.ch/cms-elog/967448
// http://cmsonline.cern.ch/cms-elog/968186
const RunsSet runs20170220_p5laser = {
  "p5laser",
  {
    // rm1
    {"SiPM-rm1-ch11", 40.0, "2.8mm"},
    {"SiPM-rm1-ch15", 40.0, "2.8mm"},
    {"SiPM-rm1-ch16", 40.0, "2.8mm"},
    
    {"SiPM-rm1-ch23", 40.0, "2.8mm"},
    //{"SiPM-rm1-ch24", 40.0, "3.3mm"},   - negative (led-ped)
    {"SiPM-rm1-ch25", 40.0, "3.3mm"},
    {"SiPM-rm1-ch26", 40.0, "2.8mm"},
    
    {"SiPM-rm1-ch33", 40.0, "2.8mm"},
    
    {"SiPM-rm1-ch41", 40.0, "2.8mm"},
    {"SiPM-rm1-ch42", 40.0, "2.8mm"},
    
    {"SiPM-rm1-ch63", 40.0, "3.3mm"},
    {"SiPM-rm1-ch66", 40.0, "2.8mm"},
    
    {"SiPM-rm1-ch75", 40.0, "2.8mm"},
    
    {"SiPM-rm1-ch82", 40.0, "2.8mm"},
    {"SiPM-rm1-ch83", 40.0, "2.8mm"},
    {"SiPM-rm1-ch84", 40.0, "2.8mm"},
    {"SiPM-rm1-ch85", 40.0, "2.8mm"},
    {"SiPM-rm1-ch86", 40.0, "2.8mm"},
    
    // rm2
    {"SiPM-rm2-ch13", 40.0, "2.8mm"},
    {"SiPM-rm2-ch14", 40.0, "2.8mm"},
    {"SiPM-rm2-ch15", 40.0, "2.8mm"},
    {"SiPM-rm2-ch16", 40.0, "2.8mm"},
    
    {"SiPM-rm2-ch25", 40.0, "2.8mm"},
    
    {"SiPM-rm2-ch32", 40.0, "2.8mm"},
    {"SiPM-rm2-ch35", 40.0, "2.8mm"},
    
    {"SiPM-rm2-ch42", 40.0, "2.8mm"},
    {"SiPM-rm2-ch44", 40.0, "3.3mm"},
    
    {"SiPM-rm2-ch53", 40.0, "2.8mm"},
    
    {"SiPM-rm2-ch73", 40.0, "2.8mm"},
    //{"SiPM-rm2-ch74", 40.0, "3.3mm"},  // max light = 100 pixels
    {"SiPM-rm2-ch75", 40.0, "3.3mm"},
    {"SiPM-rm2-ch76", 40.0, "2.8mm"},
    
    {"SiPM-rm2-ch81", 40.0, "2.8mm"},
    {"SiPM-rm2-ch85", 40.0, "2.8mm"},
    {"SiPM-rm2-ch86", 40.0, "2.8mm"},
    
    // rm3
    {"SiPM-rm3-ch11", 40.0, "2.8mm"},
    {"SiPM-rm3-ch15", 40.0, "2.8mm"},
    {"SiPM-rm3-ch16", 40.0, "2.8mm"},
    
    {"SiPM-rm3-ch23", 40.0, "2.8mm"},
    {"SiPM-rm3-ch24", 40.0, "3.3mm"},
    {"SiPM-rm3-ch25", 40.0, "3.3mm"},
    {"SiPM-rm3-ch26", 40.0, "2.8mm"},
    
    {"SiPM-rm3-ch42", 40.0, "2.8mm"},
    
    {"SiPM-rm3-ch52", 40.0, "2.8mm"},
    
    //{"SiPM-rm3-ch63", 40.0, "3.3mm"},  // max light = 300 pixels
    {"SiPM-rm3-ch66", 40.0, "2.8mm"},
    
    {"SiPM-rm3-ch75", 40.0, "2.8mm"},
    
    {"SiPM-rm3-ch82", 40.0, "2.8mm"},
    {"SiPM-rm3-ch83", 40.0, "2.8mm"},
    {"SiPM-rm3-ch84", 40.0, "2.8mm"},
    {"SiPM-rm3-ch85", 40.0, "2.8mm"},
    {"SiPM-rm3-ch86", 40.0, "2.8mm"},
    
    // rm4
    {"SiPM-rm4-ch13", 40.0, "2.8mm"},
    {"SiPM-rm4-ch14", 40.0, "2.8mm"},
    {"SiPM-rm4-ch15", 40.0, "2.8mm"},
    {"SiPM-rm4-ch16", 40.0, "2.8mm"},
    
    {"SiPM-rm4-ch25", 40.0, "2.8mm"},
    
    {"SiPM-rm4-ch35", 40.0, "2.8mm"},
    
    {"SiPM-rm4-ch42", 40.0, "2.8mm"},
    //{"SiPM-rm4-ch44", 40.0, "3.3mm"},  // max light = 300 pixels
    
    {"SiPM-rm4-ch53", 40.0, "2.8mm"},
    {"SiPM-rm4-ch56", 40.0, "2.8mm"},
    
    {"SiPM-rm4-ch64", 40.0, "2.8mm"},
    
    {"SiPM-rm4-ch73", 40.0, "2.8mm"},
    {"SiPM-rm4-ch74", 40.0, "3.3mm"},
    {"SiPM-rm4-ch75", 40.0, "3.3mm"},
    {"SiPM-rm4-ch76", 40.0, "2.8mm"},
    
    {"SiPM-rm4-ch81", 40.0, "2.8mm"},
    {"SiPM-rm4-ch85", 40.0, "2.8mm"},
    {"SiPM-rm4-ch86", 40.0, "2.8mm"},
    
    // HPD reference
    {"HPD-rm1-ch0", 0, ""},
    {"HPD-rm1-ch1", 0, ""},
    {"HPD-rm1-ch2", 0, ""},
    {"HPD-rm1-ch4", 0, ""},
    {"HPD-rm1-ch5", 0, ""},
    {"HPD-rm1-ch6", 0, ""},
    {"HPD-rm1-ch7", 0, ""},
    {"HPD-rm1-ch8", 0, ""},
    {"HPD-rm1-ch10", 0, ""},
    {"HPD-rm1-ch11", 0, ""},
    {"HPD-rm1-ch12", 0, ""},
    {"HPD-rm1-ch13", 0, ""},
    {"HPD-rm1-ch14", 0, ""},
    {"HPD-rm1-ch15", 0, ""},
    {"HPD-rm1-ch16", 0, ""},
    {"HPD-rm1-ch17", 0, ""},
    
    {"HPD-rm2-ch0", 0, ""},
    {"HPD-rm2-ch1", 0, ""},
    {"HPD-rm2-ch3", 0, ""},
    {"HPD-rm2-ch4", 0, ""},
    {"HPD-rm2-ch6", 0, ""},
    {"HPD-rm2-ch8", 0, ""},
    {"HPD-rm2-ch9", 0, ""},
    {"HPD-rm2-ch10", 0, ""},
    {"HPD-rm2-ch11", 0, ""},
    {"HPD-rm2-ch12", 0, ""},
    {"HPD-rm2-ch15", 0, ""},
    {"HPD-rm2-ch16", 0, ""},
    {"HPD-rm2-ch17", 0, ""}
    
  },
  {
    {287866, 1.},
    {287867, 1.},
    {287868, 1.},
    {287869, 1.},
    {287870, 1.},
    
    {288181, 1.},
    {288180, 1.},
    {288183, 1.},
    {288184, 1.},
    {288185, 1.},
    {288186, 1.},
    {288187, 1.},
    {288188, 1.},
    {288189, 1.},
    {288190, 1.},
    {288191, 1.},
    
    // pedestal run
    {288086, 1.}
    
  }
};

const RunsSet runs20170602_noy11 = {
  "noy11",
  {
    // ??? 1-fiber channels
    {"SiPM-rm3-ch13", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch14", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch15", 40.0, "1fib-3.3mm"},
    
    {"SiPM-rm3-ch21", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch25", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch26", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch31", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch32", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch34", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch41", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch43", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch53", 40.0, "1fib-3.3mm"},
    {"SiPM-rm3-ch56", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch63", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch66", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch71", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch72", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch73", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch74", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch75", 40.0, "1fib-2.8mm"},
    {"SiPM-rm3-ch76", 40.0, "1fib-2.8mm"},
    
    {"SiPM-rm3-ch85", 40.0, "1fib-2.8mm"},
    
    // no HPD reference
    // {"HPD-rm1-ch0", 0, ""},
  },
  {
    // shunt=1
    {6258, 1.},
    {6259, 1.},
    {6260, 1.},
    {6261, 1.},
    {6262, 1.},
    {6263, 1.},
    {6264, 1.},
    
    // shunt=11.5
    {6265, 11.5},
    {6266, 11.5},
    {6267, 11.5},
    {6268, 11.5},
    {6269, 11.5}
  }
};

// 4 fibers per channel
const RunsSet runs20170627 = {
  "bright",
  {
    {"SiPM-rm3-ch42", 40.0, "4fib-2.8mm"},
    {"SiPM-rm3-ch44", 40.0, "4fib-2.8mm"},
    
    {"SiPM-rm3-ch54", 40.0, "4fib-3.3mm"},
    
    {"SiPM-rm3-ch61", 40.0, "4fib-2.8mm"},
    {"SiPM-rm3-ch64", 40.0, "4fib-2.8mm"},
    {"SiPM-rm3-ch65", 40.0, "4fib-2.8mm"},
    
    // HPD
    {"HPD-rm1-ch14", 0, ""},
    {"HPD-rm1-ch16", 0, ""}
  },
  {
    {6290, 7.},
    {6291, 7.},
    {6292, 7.},
    {6293, 7.},
    {6294, 7.},
    {6295, 7.},
    {6296, 7.},
    {6297, 7.},
    {6298, 7.},
    {6299, 7.},
    {6300, 7.},
    {6302, 7.},
    {6303, 7.},
    {6304, 7.},
    {6305, 7.},
    {6306, 7.},
    {6307, 7.},
    {6308, 7.},
    {6309, 7.},
    {6310, 7.},
    {6311, 7.}
  }
};
